//
//  FavouriteVC.h
//  Ajo
//
//  Created by Samreen on 29/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouriteBaseController.h"

@interface FavouriteVC : FavouriteBaseController
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIView *navView;

@end
