//
//  Category.h
//  Ajo
//
//  Created by Samreen on 03/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject
@property (nonatomic, strong) NSString * categoryName;
@property (nonatomic, strong) NSString * categoryImg;

@property (nonatomic, strong) NSMutableArray * subCategory;



- (id)initWithDictionary:(NSDictionary *) responseData;
+ (NSArray *)mapCategoryFromArray:(NSArray *)arrlist ;
@end
