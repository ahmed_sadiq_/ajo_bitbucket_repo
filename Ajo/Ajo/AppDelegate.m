//
//  AppDelegate.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AppDelegate.h"
#import "User.h"
#import "DataManager.h"
#import "Constant.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "WalkThroughVC.h"
#import "FavouriteVC.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize viewController,navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    

    
    [Fabric with:@[[Crashlytics class]]];
    // Override point for customization after application launch.
    
    BOOL isLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"is_logged_in"];
    if(isLoggedIn) {
        [self setupLoginUser];
        [self createTabBarAndControl];
    }
    else{
        NSUserDefaults *defaults= [NSUserDefaults standardUserDefaults];

        if(![[[defaults dictionaryRepresentation] allKeys] containsObject:@"isFirstLogin"]){
            
           [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstLogin"];
            NSLog(@"mykey found");
            self.walkThroughVC = [[WalkThroughVC alloc] initWithNibName:@"WalkThroughVC" bundle:nil];
            
            self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.walkThroughVC];
            _window.rootViewController = self.navigationController;
            [self.navigationController setNavigationBarHidden:YES];
        }
        else{
    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    _window.rootViewController = self.navigationController;
    [self.navigationController setNavigationBarHidden:YES];
        }
    }
    //return YES;
    return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];

}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void)createTabBarAndControl {
    
    self.homeViewController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.homeViewController];
  //  [self.navigationController.tabBarItem setSelectedImage:[[UIImage imageNamed:DISCOVER_SELECTED_iMG]
                                                    // imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
    
    //[self.navigationController.tabBarItem setImage:[[UIImage imageNamed:DISCOVER_iMG] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    //self.navigationController.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);

    UIImage *img = [UIImage imageNamed:@"ajonear"];

    [self.navigationController.tabBarItem initWithTitle:@"NEARBY" image:[img imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    self.navigationController.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);

    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *recommendationVC;
    recommendationVC = [[RecommendationVC alloc] initWithNibName:@"RecommendationVC" bundle:[NSBundle mainBundle]];
    
    
    UINavigationController *recommendationNavController = [[UINavigationController alloc] initWithRootViewController:recommendationVC];
    
    
//    [recommendationVC.tabBarItem setSelectedImage:[[UIImage imageNamed:RECOMMENDATION_SELECTED_iMG]
//                                                   imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
//    [recommendationVC.tabBarItem setImage:[[UIImage imageNamed:RECOMMENDATION_iMG] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    
//    
//    recommendationVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
//
    UIImage *search = [UIImage imageNamed:@"ajosearch"];
    
    [recommendationNavController.tabBarItem initWithTitle:@"SEARCH" image:[search imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    recommendationNavController.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);

    [recommendationVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *favouriteVC;
    favouriteVC = [[FavouriteVC alloc] initWithNibName:@"FavouriteVC" bundle:[NSBundle mainBundle]];
    
    
    UINavigationController *favouriteVCNavController = [[UINavigationController alloc] initWithRootViewController:favouriteVC];
    
    UIImage *favIcon = [UIImage imageNamed:@"ajoFavorite"];
    
    [favouriteVC.tabBarItem initWithTitle:@"FAVOURITE" image:[favIcon imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    favouriteVC.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [favouriteVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *profileVC;
    profileVC = [[ProfileVC alloc] initWithNibName:@"ProfileVC" bundle:[NSBundle mainBundle]];
    UINavigationController *profileNavController = [[UINavigationController alloc] initWithRootViewController:profileVC];
    
//    [profileVC.tabBarItem setSelectedImage:[[UIImage imageNamed:PROFILE_SELECTED_iMG]
//                                            imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
   // [profileVC.tabBarItem setImage:[[UIImage imageNamed:PROFILE_iMG] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    //profileVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    UIImage *profilicon = [UIImage imageNamed:@"ajoprofile"];
    
    [profileVC.tabBarItem initWithTitle:@"PROFILE" image:[profilicon imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    profileVC.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [profileVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *settingVC;
    settingVC = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:[NSBundle mainBundle]];
    UINavigationController *settingNavController = [[UINavigationController alloc] initWithRootViewController:settingVC];
    
//    [settingVC.tabBarItem setSelectedImage:[[UIImage imageNamed:SETTINGS_SELECTED_iMG]
//                                            imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
//    [settingVC.tabBarItem setImage:[[UIImage imageNamed:SETTINGS_iMG] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    
//    settingVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    UIImage *settingIcon = [UIImage imageNamed:@"ajosettings"];
    
    [settingVC.tabBarItem initWithTitle:@"SETTINGS" image:[settingIcon imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    settingVC.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [settingVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.tabBarController = [[UITabBarController alloc] init] ;
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:self.navigationController, recommendationNavController,favouriteVCNavController,profileNavController,settingNavController,nil];
    
    [self.tabBarController.tabBar setBackgroundColor:[UIColor whiteColor]];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:59.0/255.0 green:197.0/255.0 blue:186.0/255.0 alpha:1];
    self.viewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //self.viewController.navigationController.navigationBar
    [[[UIApplication sharedApplication]delegate] window].rootViewController = self.tabBarController;
}
-(void) setupLoginUser{

    
    User * user = [User new];
    user.name = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_name"];
    user.email = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_email"];
    user.userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_Id"];
    user.profilePicture = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_profile_pic"];
    user.proximity = [[NSUserDefaults standardUserDefaults] objectForKey:@"Proximity"];
    user.phone_number =[[NSUserDefaults standardUserDefaults] objectForKey:@"phone"];

    user.city =[[NSUserDefaults standardUserDefaults] objectForKey:@"City"];
    user.country = [[NSUserDefaults standardUserDefaults] objectForKey:@"Country"];
    user.address = [[NSUserDefaults standardUserDefaults] objectForKey:@"address"];

    user.latitude =@"";
    user.longitude = @"";
    user.location = [[NSUserDefaults standardUserDefaults] objectForKey:@"location"];
    
    
    

    [[DataManager sharedManager] setCurrentUser:user];


}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [FBSDKAppEvents activateApp];
}



@end
