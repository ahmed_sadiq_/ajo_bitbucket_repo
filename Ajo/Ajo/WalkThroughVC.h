//
//  WalkThroughVC.h
//  Ajo
//
//  Created by Samreen on 27/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface WalkThroughVC : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControll;
@property (weak, nonatomic) IBOutlet UIButton *btnGetStart;



@end
