//
//  WalkThroughVC.m
//  Ajo
//
//  Created by Samreen on 27/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "WalkThroughVC.h"

@interface WalkThroughVC ()<UIScrollViewDelegate>

@end

@implementation WalkThroughVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _btnGetStart.layer.cornerRadius = 5; // this value vary as per your desire
    _btnGetStart.clipsToBounds = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
   
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidLayoutSubviews{
    
    CGFloat scrollViewHeight = self.scrollView.frame.size.height;
    CGFloat scrollViewWidth = self.scrollView.frame.size.width;
    ///////////////Image 1////////////////

//    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,scrollViewWidth, scrollViewHeight)];
//    UIImageView *icon1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollViewWidth/2)-80, 150,160, 160)];
//    icon1.image = [UIImage imageNamed:@"Explore_icon.png"];
//    icon1.contentMode = UIViewContentModeScaleAspectFit;
//    UILabel *lblTitle1 = [[UILabel alloc] initWithFrame:CGRectMake((scrollViewWidth/2)-150, 400, 300, 50)];
//    lblTitle1.textAlignment = NSTextAlignmentCenter;
//    lblTitle1.textColor = [UIColor whiteColor];
//    [lblTitle1 setFont:[UIFont fontWithName:@"Roboto-Bold" size:20.0]];
//    lblTitle1.text = @"Explore";
//    UILabel *lblDetail1 = [[UILabel alloc] initWithFrame:CGRectMake((scrollViewWidth/2)-120, 450, 240, 50)];
//    lblDetail1.textAlignment = NSTextAlignmentCenter;
//    lblDetail1.textColor = [UIColor whiteColor];
//    [lblDetail1 setFont:[UIFont fontWithName:@"Roboto-Light" size:15.0]];
//    lblDetail1.text = @"Discover amazing places and points of interests in Africa";
//    lblDetail1.numberOfLines = 2;
//    ///////////////Image 2////////////////
//
//    UIImageView *image2 = [[UIImageView alloc] initWithFrame:CGRectMake(scrollViewWidth, 0,scrollViewWidth, scrollViewHeight)];
//    UIImageView *icon2 = [[UIImageView alloc] initWithFrame:CGRectMake(scrollViewWidth+(scrollViewWidth/2)-80, 150,160, 160)];
//    icon2.image = [UIImage imageNamed:@"Review _icon.png"];
//    icon2.contentMode = UIViewContentModeScaleAspectFit;
//    UILabel *lblTitle2 = [[UILabel alloc] initWithFrame:CGRectMake(scrollViewWidth+(scrollViewWidth/2)-150, 400, 300, 50)];
//    lblTitle2.textAlignment = NSTextAlignmentCenter;
//    lblTitle2.textColor = [UIColor whiteColor];
//    [lblTitle2 setFont:[UIFont fontWithName:@"Roboto-Bold" size:20.0]];
//    lblTitle2.text = @"Review Places";
//    UILabel *lblDetail2 = [[UILabel alloc] initWithFrame:CGRectMake(scrollViewWidth+(scrollViewWidth/2)-108, 450, 216, 50)];
//    lblDetail2.textAlignment = NSTextAlignmentCenter;
//    lblDetail2.textColor = [UIColor whiteColor];
//    [lblDetail2 setFont:[UIFont fontWithName:@"Roboto-Light" size:15.0]];
//    lblDetail2.text = @"Search and review places,upload pictures of places";
//    lblDetail2.numberOfLines = 2;
//    ///////////////Image 3 ////////////////
//
//    UIImageView *image3 = [[UIImageView alloc] initWithFrame:CGRectMake(scrollViewWidth*2, 0, scrollViewWidth, scrollViewHeight)];
//    
//    UIImageView *icon3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollViewWidth+scrollViewWidth+(scrollViewWidth/2))-80, 150,160, 160)];
//    icon3.image = [UIImage imageNamed:@"Enjoy_your_vacation_icon.png"];
//    icon3.contentMode = UIViewContentModeScaleAspectFit;
//    UILabel *lblTitle3 = [[UILabel alloc] initWithFrame:CGRectMake((scrollViewWidth+scrollViewWidth+(scrollViewWidth/2))-150, 400, 300, 50)];
//    lblTitle3.textAlignment = NSTextAlignmentCenter;
//    lblTitle3.textColor = [UIColor whiteColor];
//    [lblTitle3 setFont:[UIFont fontWithName:@"Roboto-Bold" size:20.0]];
//    lblTitle3.text = @"Enjoy your vacation";
//    UILabel *lblDetail3 = [[UILabel alloc] initWithFrame:CGRectMake(scrollViewWidth+scrollViewWidth+(scrollViewWidth/2)-120, 450, 240, 50)];
//    lblDetail3.textAlignment = NSTextAlignmentCenter;
//    lblDetail3.textColor = [UIColor whiteColor];
//    [lblDetail3 setFont:[UIFont fontWithName:@"Roboto-Light" size:15.0]];
//    lblDetail3.text = @"Have as much fun as you can.Don't forget to take photos";
//    lblDetail3.numberOfLines = 2;
//    image1.image = [UIImage imageNamed:@"Ajo mobile -bg.png"];
//    image2.image = [UIImage imageNamed:@"Ajo mobile -bg.png"];
//    image3.image = [UIImage imageNamed:@"Ajo mobile -bg.png"];
//    
//    [_scrollView addSubview:image1];
//    [_scrollView addSubview:image2];
//    [_scrollView addSubview:image3];
//    [_scrollView addSubview:icon1];
//    [_scrollView addSubview:icon2];
//    [_scrollView addSubview:icon3];
//    [_scrollView addSubview:lblTitle1];
//    [_scrollView addSubview:lblTitle2];
//    [_scrollView addSubview:lblTitle3];
//    [_scrollView addSubview:lblDetail1];
//    [_scrollView addSubview:lblDetail2];
//    [_scrollView addSubview:lblDetail3];
//
    
    self.scrollView.contentSize = CGSizeMake(scrollViewWidth * 3, scrollViewHeight);
    self.scrollView.delegate = self;
    self.pageControll.currentPage = 0;
    

}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
        int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    self.pageControll.currentPage = page;
    if (page == 2) {
        _btnGetStart.hidden = NO;
    }
    else{
        _btnGetStart.hidden = YES;

    }

}
- (IBAction)btnGetStart:(id)sender {

    ViewController *viewContr = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    [self.navigationController pushViewController:viewContr animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
