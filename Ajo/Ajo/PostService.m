//
//  PostService.m
//  Ajo
//
//  Created by Samreen Noor on 04/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "PostService.h"
#import "PostModel.h"
#import "SVProgressHUD.h"
@implementation PostService


+(void) addNewPostWithPlaceId:(NSString *)placeId
                     PostText:(NSString *)text
                     DateTime:(NSString *)dateTime
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure{
    
    User *user=[DataManager sharedManager].currentUser;

    NSDictionary * postParam = @{
                                    @"user_id":user.userID,
                                    @"place_id":placeId,
                                    @"post_text":text,
                                    @"date_time":dateTime
                                    
                                    };
    
    [NetworkManager postURI:URL_SAVE_POST
                 parameters:postParam
                    success:^(id data) {
                        
                            [SVProgressHUD dismiss];
                        BOOL status=data[@"status"];
                        
                        if (status) {
                            PostModel *post = [[PostModel alloc]initWithDictionaryPost:data[@"post"]] ;
                            
                            success (post);
                       
                        
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) addNewPostWithPlaceId:(NSString *)placeId
                       Rating:(NSString *)rating
                     PostText:(NSString *)text
                     DateTime:(NSString *)dateTime
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure{
    
    User *user=[DataManager sharedManager].currentUser;
    NSString *userId = user.userID;
    if (userId==nil) {
        userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_Id"];
        
    }
    NSDictionary * postParam = @{
                                 @"user_id":userId,
                                 @"place_id":placeId,
                                 @"review_text":text,
                                 @"date_time":dateTime,
                                 @"rating":rating
                                 };
    
    [NetworkManager postURI:URL_UPDATE_REVIEWS
                 parameters:postParam
                    success:^(id data) {
                            [SVProgressHUD dismiss];
                        
                        BOOL status=data[@"status"];
                        
                        if (status) {
                            PostModel *post = [[PostModel alloc]initWithDictionaryReviews:data[@"review"]] ;
                            
                            success (post);
                            
                            
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) addNewPhotosWithPlaceId:(NSString *)placeId
                       Pictures:(NSArray *)picsArray
                     PhotoText:(NSString *)text
                     DateTime:(NSString *)dateTime
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure{
    
    User *user=[DataManager sharedManager].currentUser;
    NSMutableArray *formDataArray = [[NSMutableArray alloc]init];
    NSDictionary * postParam = @{
                                 @"user_id":user.userID,
                                 @"place_id":placeId,
                                 @"picture_text":text,
                                 @"date_time":dateTime,
                                 };
    
    for (int i=0; i<[picsArray count]; i++) {
        NSData *imageData = UIImagePNGRepresentation(picsArray[i]);
        [formDataArray addObject:imageData];
    }
    

    [NetworkManager postURIWithFormDataArray:formDataArray parameters:postParam url:URL_UPLOAD_PICTURE success:^(id data) {
        
            [SVProgressHUD dismiss];
        
        BOOL status=data[@"status"];
        
        if (status) {
            PostModel *post = [[PostModel alloc]initWithDictionaryPics:data[@"post_picture"]] ;
            
            success (post);
            
            
        }

        
    } failure:^(NSError *error) {
        
        failure(error);

        
    }];
    
    
}

+(void) deletePostWithPostId:(NSString *)postId
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure{
    
    
    NSDictionary * postParam = @{
                                 @"post_id":postId,
                               
                                 
                                 };
    
    [NetworkManager postURI:URL_DELETE_POST
                 parameters:postParam
                    success:^(id data) {
                            [SVProgressHUD dismiss];
                        
                        BOOL status=data[@"status"];
                        
                        if (status) {
                            
                            success (data);
                            
                            
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) deletePostWithReviewId:(NSString *)reviewId
                     success:(serviceSuccess)success
                     failure:(serviceFailure)failure{
    
    
    NSDictionary * postParam = @{
                                 @"review_id":reviewId,
                                 
                                 
                                 };
    
    [NetworkManager postURI:URL_DELETE_REVIEWS
                 parameters:postParam
                    success:^(id data) {
                        
                            [SVProgressHUD dismiss];
                        BOOL status=data[@"status"];
                        
                        if (status) {
                            
                            success (data);
                            
                            
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) deletePostedPicturesWithPitureId:(NSString *)picId
                             andPictures:(NSArray *) pics
                       success:(serviceSuccess)success
                       failure:(serviceFailure)failure{
    
    
    NSMutableArray *formDataArray = [[NSMutableArray alloc]init];

    NSDictionary * postParam = @{
                                 @"post_picture_id":picId,
                                 @"pictures" : pics
                                 
                                 };
    
    
    [NetworkManager postURI:URL_DELETE_POSTED_PICTURE
                 parameters:postParam
                    success:^(id data) {
                            [SVProgressHUD dismiss];
                        
                        BOOL status=data[@"status"];
                        
                        if (status) {
                            
                            success (data);
                            
                            
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];

}

@end
