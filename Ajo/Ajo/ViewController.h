//
//  ViewController.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeVC.h"

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (strong , nonatomic) HomeVC *viewController;
@property (strong , nonatomic) IBOutlet UITextField *pswdTxt;
@property (strong , nonatomic) IBOutlet UITextField *userNameTxt;
@property (strong , nonatomic) UITabBarController *tabBarController;
@property (strong , nonatomic) UINavigationController *navController;
@property (weak, nonatomic) IBOutlet UIButton *btnDiscover;
@property (weak, nonatomic) IBOutlet UIImageView *imgPasVisible;

- (IBAction)loginPressed:(id)sender;
- (IBAction)crateAccountPressed:(id)sender;
- (IBAction)forgotPasswordPressed:(id)sender;

@end

