//
//  User.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "User.h"
#import "DataManager.h"
@implementation User
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        if ([DataManager sharedManager].isFromSignup) {
            self.userID  = [self validStringForObject:responseData [KEY_USER_ID]];
            self.name  = [self validStringForObject:responseData [KEY_NAME]];
            self.email  = [self validStringForObject:responseData [KEY_EMAIL]];
            // self.profilePicture = [self completeImageURL:responseData [@"image_path"]];
            self.profilePicture = [self validStringForObject:responseData[KEY_PROFILE_IMG]];
            self.proximity = @"50";
            self.phone_number = @"";
            self.city = @"";
            self.country = @"";
            self.address =@"";
            self.latitude = @"";
            self.longitude =@"";
            self.location = @"";
        }
        else{ self.userID  = [self validStringForObject:responseData [KEY_USER_ID]];
        self.name  = [self validStringForObject:responseData [KEY_NAME]];
        self.email  = [self validStringForObject:responseData [KEY_EMAIL]];
       // self.profilePicture = [self completeImageURL:responseData [@"image_path"]];
        self.profilePicture = [self validStringForObject:responseData[KEY_PROFILE_IMG]];
        self.proximity = [self validStringForObject:responseData[@"proximity"]];
        self.phone_number = [self validStringForObject:responseData[@"phone_number"]];
        self.city = [self validStringForObject:responseData[@"city"]];
        self.country = [self validStringForObject:responseData[@"country"]];
        self.address =[self validStringForObject:responseData[@"address"]];
        self.latitude = @"";//[self validStringForObject:responseData[@"latitude"]];
        self.longitude =@""; //[self validStringForObject:responseData[@"longitude"]];
        self.location = [self validStringForObject:responseData[@"location"]];
        }
    }
    
    return self;
}




+ (NSArray *)mapUserFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[User alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
}

@end
