//
//  FavouriteBaseController.m
//  Ajo
//
//  Created by Samreen on 30/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "FavouriteBaseController.h"
#import "Places.h"
#import "DataManager.h"
#import "PlaceService.h"

@interface FavouriteBaseController ()

@end

@implementation FavouriteBaseController

- (void)viewDidLoad {
    [super viewDidLoad];
    _favPlacesArray =[[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
}

- (void) addPlaceToFav : (UIButton *) sender {
    UIButton *btn = sender;
    Places *place = [_favPlacesArray objectAtIndex: sender.tag];
    if ([place.is_favourite isEqualToString:@"0"]) {
        place.is_favourite = @"1";
        [btn setImage:[UIImage imageNamed:@"heart_filled"] forState:UIControlStateNormal];
        [[DataManager sharedManager].favouritePlaces insertObject:place atIndex:0];

        [self updateFavPlaces:place.place_id];
    }
    else{
        place.is_favourite = @"0";
        [btn setImage:[UIImage imageNamed:@"heart_unfilled"] forState:UIControlStateNormal];
        [self removeFromFav:place.place_id];
        [self.tableView reloadData];
        [self updateFavPlaces:place.place_id];

    }
 
    NSLog(@"product....");
}
-(void) removeFromFav:(NSString *)placeId{
    for (Places *plc in [DataManager sharedManager].favouritePlaces) {
        if ([placeId isEqualToString:plc.place_id]) {
            [[DataManager sharedManager].favouritePlaces removeObject:plc];
            break;
        }
    }
    for (Places *plc in [DataManager sharedManager].nearBYPlaces) {
        if ([placeId isEqualToString:plc.place_id]) {
            plc.is_favourite = @"0";
            break;
        }
    }
    
}

-(void) updateFavPlaces:(NSString *)placId{
    [PlaceService addFavPlacesWithPlaceID:placId success:^(id data) {
     
    } failure:^(NSError *error) {
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
