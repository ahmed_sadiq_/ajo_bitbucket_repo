//
//  FavouriteBaseController.h
//  Ajo
//
//  Created by Samreen on 30/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouriteBaseController : UIViewController

@property (strong, nonatomic) NSMutableArray *favPlacesArray;
@property (strong, nonatomic) UITableView *tableView;

- (void) addPlaceToFav : (UIButton *) sender ;
@end
