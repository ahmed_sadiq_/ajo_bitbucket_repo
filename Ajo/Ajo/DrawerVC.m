//
//  DrawerVC.m
//  HydePark
//
//  Created by Mr on 22/04/2015.
//  Copyright (c) 2015 TxLabz. All rights reserved.
//

#import "DrawerVC.h"
#import "Constants.h"
#import "DrawerCell.h"
#import "DataManager.h"
#import "UIImageView+URL.h"

//#import "NavigationHandler.h"
@interface DrawerVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *menuStringArray;
    NSArray *menuImGsArray;

}
@end

static DrawerVC *DrawerVC_Instance= NULL;

@implementation DrawerVC

@synthesize _currentState,tag;
float _yLocation;

+(DrawerVC *)getInstance{
    
    if(DrawerVC_Instance == NULL)
    {
        
            DrawerVC_Instance = [[DrawerVC alloc] initWithNibName:@"DrawerVC" bundle:nil];
        
        
        _yLocation = 0.0f;
    }
    return DrawerVC_Instance;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        _currentState = OFF_HIDDEN;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    menuStringArray =[[NSArray alloc]initWithObjects:@"Blog",@"Help and support",@"Legal",@"About us", nil];
    menuImGsArray =[[NSArray alloc]initWithObjects:@"ajoblog",@"ajohelp",@"ajolegal",@"ajocompany", nil];
    User *user=[DataManager sharedManager].currentUser;
    [_profileImg setImageWithStringURL:user.profilePicture];
    _profileImg.layer.cornerRadius = _profileImg.frame.size.width/2;
    _profileImg.layer.masksToBounds = YES;
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (IS_IPHONE_6) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 667);
    }else if (IS_IPHONE_6Plus){
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 736);
    }
    
    DrawerVC_Instance = self;
    
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizer];
}

-(void)AddInView:(UIViewController *)_parentView{
    parentView = _parentView.view;
    parentViewController = _parentView;
    if(_currentState == OFF_HIDDEN)
        //        [self.view setFrame:CGRectMake(parentView.frame.size.width, _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
        if (IS_IPAD) {
            [self.view setFrame:CGRectMake(-424, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
        }else{
            
            [self.view setFrame:CGRectMake(-280, _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
            if (IS_IPHONE_6) {
                [self.view setFrame:CGRectMake(-285, _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
            }
        }
    [_parentView.view addSubview:self.view];
    
}
-(void)ShowInView{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    /*if(_currentState == OFF_HIDDEN)
    {
        [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            // final view elements attributes goes here
            // [self.view setFrame:CGRectMake(parentView.frame.size.width - self.view.frame.size.width, _yLocation, self.view.frame.size.width, parentView.frame.size.height-_yLocation)];
            [self.view setFrame:CGRectMake(0, _yLocation, screenWidth, parentView.frame.size.height-_yLocation)];
            
            [parentView bringSubviewToFront:self.view];
            
            
        } completion:^(BOOL done){
            dimmer.hidden = false;
        }];
    }
    
    else if(_currentState == ON_SCREEN)
    {
        [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
            
            _currentState = OFF_HIDDEN;
            if (IS_IPAD) {
                [self.view setFrame:CGRectMake(-screenWidth, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
            }else{
                [self.view setFrame:CGRectMake(-screenWidth, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
                if (IS_IPHONE_6) {
                    [self.view setFrame:CGRectMake(-screenWidth , _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
                }
            }
            dimmer.hidden = true;
            
        } completion:nil];
    }*/
    if(_currentState == OFF_HIDDEN)
    {
        parentViewController.navigationController.tabBarController.tabBar.hidden = YES;

        [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            _currentState = ON_SCREEN;
            
            // final view elements attributes goes here
            [self.view setFrame:CGRectMake(0, _yLocation, screenWidth, parentView.frame.size.height-_yLocation)];
            [parentView bringSubviewToFront:self.view];
            
            
        }completion:^(BOOL done){
            dimmer.hidden = false;
        }];
    }
    
    else if(_currentState == ON_SCREEN)
    {
        [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{

            _currentState = OFF_HIDDEN;
            dimmer.hidden = true;
            //[[parentView viewWithTag:HIDE_TAG] removeFromSuperview];
            
            // final view elements attributes goes here
            // [self.view setFrame:CGRectMake(parentView.frame.size.width, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
            if (IS_IPAD) {
                [self.view setFrame:CGRectMake(-screenWidth, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
            }else{
                [self.view setFrame:CGRectMake(-screenWidth, _yLocation, self.view.frame.size.width, parentView.frame.size.height -_yLocation)];
                if (IS_IPHONE_6) {
                    [self.view setFrame:CGRectMake(-screenWidth , _yLocation, self.view.frame.size.width, self.view.frame.size.height)];
                }
            }
            
        } completion:^(BOOL done){
            parentViewController.navigationController.tabBarController.tabBar.hidden = NO;

        }];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)generalAction:(id)sender{
    
    [self ShowInView];
}

- (IBAction)logTicketPressed:(id)sender {
    
    //[[NavigationHandler getInstance] NavigateToHomeScreen];
    [self ShowInView];
}

- (IBAction)appSettngs:(id)sender {
    //[[NavigationHandler getInstance] NavigateToLoginScreen];
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self ShowInView];
}
- (IBAction)editProfilePressed:(id)sender {
   // [[NavigationHandler getInstance] MoveToEditProfile];
}

- (IBAction)historyPressed:(id)sender {
    //[[NavigationHandler getInstance] MoveToHistory];
}
- (IBAction)orkinPressed:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.orkinlebanon.com/"]];

}
- (IBAction)facebookPressed:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/OrkinLB/"]];
}
- (IBAction)gamePressed:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/bn/app/bug-battle/id410912970?mt=8"]];
}

- (IBAction)btnHideDrawer:(id)sender {
    [self ShowInView];

}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    int rows = (int)([placesArray count]/2);
    //    if([placesArray count] %2 == 1) {
    //        rows++;
    //    }
    //    return rows;
    return menuStringArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DrawerCell * cell = (DrawerCell *)[_menutblView dequeueReusableCellWithIdentifier:@"DrawerCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"DrawerCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
   
    cell.lblTitle.text = menuStringArray[indexPath.row];
    cell.icon.image = [UIImage imageNamed:menuImGsArray[indexPath.row]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self ShowInView];

    if (indexPath.row==0) {
        parentViewController.navigationController.tabBarController.selectedIndex = 0;
        
    }
    else if (indexPath.row==1||indexPath.row==2||indexPath.row==3){
        parentViewController.navigationController.tabBarController.selectedIndex = 4;

    }
    
}


@end
