//
//  AccountService.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AccountService.h"
#import "UIImage+JS.h"
#import "SVProgressHUD.h"
@implementation AccountService

+(void)forgetPasswordWhereEmail:(NSString *)email
                        succuss:(serviceSuccess)success
                        failure:(serviceFailure)failure{
    NSDictionary * user = @{
                            
                            @"email"  : email
                            
                            
                            };
    
    NSString *forget_uri = [NSString stringWithFormat:URI_FORGET_PASSWORD];
    
    [NetworkManager postURI:forget_uri parameters:user success:^(id data) {
        [SVProgressHUD dismiss];
        success (data);
        
    } failure:^(NSError *error) {
        failure(error);
        
    }
     ];
    
}



+(void)loginUserWithEmail:(NSString *)email
                 password:(NSString *)pass
                  success:(serviceSuccess)success
                  failure:(serviceFailure)failure{
    
    
    NSDictionary * user = @{
                            KEY_EMAIL:email,
                            KEY_PASSWORD:pass,
                            @"method"  : @"login"
                            
                            
                            };
    
    
    
    
    NSLog(@"user: %@",user);
    
    [NetworkManager postURI:URI_SIGN_IN
                 parameters:user
                    success:^(id data) {
                        [SVProgressHUD dismiss];

                        BOOL status=data[@"status"];
                        if (status) {
                            [DataManager sharedManager].isFromSignup = NO;

                            User * user = [[User new]initWithDictionary:data[@"user_data"]];

                            [[DataManager sharedManager] setCurrentUser:user];
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is_logged_in"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"user_name"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"user_email"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"user_Id"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.profilePicture forKey:@"user_profile_pic"];
                            if (![user.location isEqualToString:@""]) {
                                [[NSUserDefaults standardUserDefaults] setObject:user.location forKey:@"location"];
                            }
                            
                            
                            [[NSUserDefaults standardUserDefaults] setObject:user.address forKey:@"address"];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:user.city forKey:@"City"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.phone_number forKey:@"phone"];
                             [[NSUserDefaults standardUserDefaults] setObject:user.country forKey:@"Country"];
                            if ([user.proximity isEqualToString:@"0"]) {
                                [[DataManager sharedManager].currentUser setProximity:@"50"];
                                [[NSUserDefaults standardUserDefaults] setObject:@"50" forKey:@"Proximity"];

                            }
                            else{
                             [[NSUserDefaults standardUserDefaults] setObject:user.proximity forKey:@"Proximity"];
                            }
                            
//                            if (![user.latitude isEqualToString:@"0"]) {
//                                [[NSUserDefaults standardUserDefaults] setObject:user.latitude forKey:@"placesLat"];
//                                [[NSUserDefaults standardUserDefaults] setObject:user.longitude forKey:@"placesLong"];
//                            }
//                           
                            [[NSUserDefaults standardUserDefaults] synchronize];


                            success(user);
                            
                            
                        }
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
}





+(void)registerUserWithFirstName:(NSString *)Name
                           email:(NSString *)email
                        password:(NSString *)password
                   andProfielImg:(UIImage *)img
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure {
    
    NSDictionary * userParam = @{
                                 
                                 KEY_NAME:Name,
                                 KEY_EMAIL:email,
                                 KEY_PASSWORD:password,
                                 
                                 };
    
    
    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);

    [NetworkManager postURIWithFormData:URI_SIGN_UP parameters:userParam formData:imageData success:^(id data) {
        [SVProgressHUD dismiss];

        BOOL status=data[@"status"];
        if (status) {
            [DataManager sharedManager].isFromSignup = YES;
            User * user = [[User new]initWithDictionary:data[@"user_data"]];
//            user.name = data[@"user_data"][KEY_NAME];
//            user.email = data[@"user_data"][KEY_EMAIL];
//            user.userID = data[@"user_data"][KEY_USER_ID];
//            user.profilePicture = data[@"user_data"][KEY_PROFILE_IMG];
//           user.proximity = @"50";
//            user.phone_number = @"";
//           user.city = @"";
//           user.country = @"";
//          user.address = @"";
////            user.latitude = data[@"user_info"][@"latitude"];
////            user.longitude = data[@"user_info"][@"longitude"];
//            user.location = @"";
//
            [[DataManager sharedManager] setCurrentUser:user];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is_logged_in"];

            [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"user_email"];
            [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"user_Id"];
            [[NSUserDefaults standardUserDefaults] setObject:user.profilePicture forKey:@"user_profile_pic"];
            
            [[NSUserDefaults standardUserDefaults] setObject:user.address forKey:@"address"];
            
            [[NSUserDefaults standardUserDefaults] setObject:user.city forKey:@"City"];
            [[NSUserDefaults standardUserDefaults] setObject:user.phone_number forKey:@"phone"];
            [[NSUserDefaults standardUserDefaults] setObject:user.country forKey:@"Country"];
        
            [[DataManager sharedManager].currentUser setProximity:@"50"];
            [[NSUserDefaults standardUserDefaults] setObject:@"50" forKey:@"Proximity"];
     
            [[NSUserDefaults standardUserDefaults] synchronize];
            

            
            
            
            success (data);
        }

    } failure:^(NSError *error) {
        failure(error);

    }];
    
    
    
}

+(void) fbLoginWithFirstName:(NSString *)Name
                           email:(NSString *)email
                   andProfielImg:(NSString *)img
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure {
    
    
    NSDictionary * userParam = @{
                                 
                                 KEY_NAME:Name,
                                 KEY_EMAIL:email,
                                 @"profile_image":img,
                                 @"is_social":@"1"
                                 
                                 
                                 };
    
    
    
    [NetworkManager postURI:URI_SIGN_UP parameters:userParam success:^(id data) {
        [SVProgressHUD dismiss];

        BOOL status=data[@"status"];
        if (status) {
            [DataManager sharedManager].isFromSignup = NO;

            User * user = [[User new]initWithDictionary:data[@"user_data"]];

            [[DataManager sharedManager] setCurrentUser:user];
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is_logged_in"];
            [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"user_Id"];
            [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"user_email"];
            [[NSUserDefaults standardUserDefaults] setObject:user.profilePicture forKey:@"user_profile_pic"];
            if (![user.location isEqualToString:@""]) {
                [[NSUserDefaults standardUserDefaults] setObject:user.location forKey:@"location"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:user.address forKey:@"address"];
            
            [[NSUserDefaults standardUserDefaults] setObject:user.city forKey:@"City"];
            [[NSUserDefaults standardUserDefaults] setObject:user.phone_number forKey:@"phone"];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:user.country forKey:@"Country"];
            if ([user.proximity isEqualToString:@"0"]) {
                [[DataManager sharedManager].currentUser setProximity:@"50"];
                [[NSUserDefaults standardUserDefaults] setObject:@"50" forKey:@"Proximity"];
                
            }
           
            

            
            
            success (user);
        }

    } failure:^(NSError *error) {
        failure(error);

    }];
    
    
}



+(void)editUserProfileWithName:(NSString *)Name
                        userID:(NSString *)userId
                           email:(NSString *)email
                     ProfielImg:(UIImage *)img
                          City:(NSString *)city
                         Phone:(NSString *)phone
                       Country:(NSString *)country
                       Address:(NSString *)address
                      Latitude:(NSString *)lat
                      Logitude:(NSString *)longi
                    PlanceName:(NSString *)placename
                  andProximity:(NSString *)proximity
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure {
    
    if (userId==nil) {
        userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_Id"];

    }
    
    NSDictionary * userParam = @{
                                 KEY_USER_ID:userId,
                                 KEY_NAME:Name,
                                 KEY_EMAIL:email,
                                 @"city":city,
                                 @"phone_number":phone,
                                 @"country":country,
                                 @"address":address,
                                 //@"proximity":proximity,
                                // @"location": placename,
                                // @"latitude": lat,
                                 //@"longitude":longi
                                 };
    
    
    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
    
    [NetworkManager postURIWithFormData:URL_EDIT_PROFILE parameters:userParam formData:imageData success:^(id data) {
        [SVProgressHUD dismiss];

        BOOL status=data[@"status"];
        if (status) {
            [DataManager sharedManager].isFromSignup = NO;

            User * user = [[User new]initWithDictionary:data[@"user_info"]];


            [[DataManager sharedManager] setCurrentUser:user];
            
            [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"user_email"];
            [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"user_Id"];
            [[NSUserDefaults standardUserDefaults] setObject:user.profilePicture forKey:@"user_profile_pic"];
                [[NSUserDefaults standardUserDefaults] setObject:user.location forKey:@"location"];
                [[NSUserDefaults standardUserDefaults] setObject:user.address forKey:@"address"];
            
                [[NSUserDefaults standardUserDefaults] setObject:user.city forKey:@"City"];
                [[NSUserDefaults standardUserDefaults] setObject:user.phone_number forKey:@"phone"];
            
            
                [[NSUserDefaults standardUserDefaults] setObject:user.country forKey:@"Country"];
                [[NSUserDefaults standardUserDefaults] setObject:user.proximity forKey:@"Proximity"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            
            
            success (data);
        }
        
    } failure:^(NSError *error) {
        failure(error);
        
    }];
    

    
    
    
    
//    [NetworkManager postURI:URL_EDIT_PROFILE
//                 parameters:userParam
//                    success:^(id data) {
//                        
//                        
//                        BOOL status=data[@"status"];
//                        if (status) {
//                            
//                            User * user = [User new];
//                            user.name = data[@"user_data"][KEY_NAME];
//                            user.email = data[@"user_data"][KEY_EMAIL];
//                            user.userID = data[@"user_data"][KEY_USER_ID];
//                            user.profilePicture = data[@"user_data"][KEY_PROFILE_IMG];
//                            
//                            [[DataManager sharedManager] setCurrentUser:user];
//                            
//                        
//                            [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"user_name"];
//                            [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"user_email"];
//                            
//                            
//                            
//                            success (data);
//                        }
//                        
//                        
//                        
//                        
//                    } failure:^(NSError *error) {
//                        
//                        
//                        
//                        failure(error);
//                        
//                        
//                    }];
//    
    
}

+(void) saveUserSettingsWithUserID:(NSString *)userId
                        City:(NSString *)city
                         State:(NSString *)state
                          Country:(NSString *)country
                           ZipCode:(NSString *)zipCode
                          Latitude:(NSString *)lat
                          Logitude:(NSString *)longi
                        PlanceName:(NSString *)placename
                      andProximity:(NSString *)proximity
                       success:(serviceSuccess)success
                       failure:(serviceFailure)failure {
    
    
    if (userId==nil) {
        userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_Id"];
        
    }

    NSDictionary * settingParam = @{
                                 KEY_USER_ID:userId,
                              @"city":city,
                                 @"state":state,
                                 @"country":country,
                                 @"zip_code":zipCode,
                                 @"proximity":proximity,
                                 @"location": placename,
                                 @"latitude": lat,
                                @"longitude":longi
                                 
                                 };
    
   
    [NetworkManager postURI:URL_SETTINGS
                 parameters:settingParam
                    success:^(id data) {
                        [SVProgressHUD dismiss];

                        
                        BOOL status=data[@"status"];
                        if (status) {
              
                            
                            success (data);
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}






+(void) saveLocationSettingsWithUserID:(NSString *)userId
                          Latitude:(NSString *)lat
                          Logitude:(NSString *)longi
                        PlanceName:(NSString *)placename
                          andProximity:(NSString *)proximity
                           success:(serviceSuccess)success
                           failure:(serviceFailure)failure {
    if (userId==nil) {
        userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_Id"];
        
    }
    if ([lat isEqualToString:@""]||lat==nil) {
        
        lat = @"";
        longi = @"";
    }
    
    NSDictionary * settingParam = @{
                                    KEY_USER_ID:userId,
                                    @"proximity":proximity,
                                    @"location": placename,
                                    @"latitude": lat,
                                    @"longitude":longi
                                    
                                    };
    
    
    [NetworkManager postURI:URL_EDIT_PROFILE
                 parameters:settingParam
                    success:^(id data) {
                        [SVProgressHUD dismiss];
                        
                        BOOL status=data[@"status"];
                        if (status) {
                            [DataManager sharedManager].isFromSignup = NO;
                            
                            User * user = [[User new]initWithDictionary:data[@"user_info"]];
                        
                            
                            [[DataManager sharedManager] setCurrentUser:user];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"user_name"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"user_email"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.userID forKey:@"user_Id"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.profilePicture forKey:@"user_profile_pic"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.location forKey:@"location"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.address forKey:@"address"];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:user.city forKey:@"City"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.phone_number forKey:@"phone"];
                            
                            
                            [[NSUserDefaults standardUserDefaults] setObject:user.latitude forKey:@"placesLat"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.longitude forKey:@"placesLong"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.country forKey:@"Country"];
                            [[NSUserDefaults standardUserDefaults] setObject:user.proximity forKey:@"Proximity"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            
                            
                            success (data);
                        }
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];


}


@end
