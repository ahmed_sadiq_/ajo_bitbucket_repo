//
//  Places.m
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Places.h"
#import "Pictures.h"
#import "Post.h"
#import "Reviews.h"
@implementation Places
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.placeName = [self validStringForObject:responseData[@"name"]];
        self.placePhone = [self validStringForObject:responseData[@"phone_number"]];
        self.placeEmail = [self validStringForObject:responseData[@"email"]];
        self.placeWebsite = [self validStringForObject:responseData[@"website"]];
        self.placeAddress = [self validStringForObject:responseData[@"address"]];
        self.placeDescription = [self validStringForObject:responseData[@"description"]];
        self.is_favourite = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"is_favourite"]]];
        self.latitude = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"latitude"]]];
        self.longitude = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"longitude"]]];
        self.distance = [NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"distance"]]];

        self.place_id = [self validStringForObject:responseData[@"place_id"]];
      //  self.place_picture =[self validStringForObject:responseData[@"place_picture"]];
        self.place_picture = [[Pictures alloc] mapArrayOfPics:responseData[@"place_picture"]];

        self.reviewCount =[NSString stringWithFormat:@"%@", [self validStringForObject:responseData[@"review_count"]]];
        self.postCount =[NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"posts_count"]]];
        self.post_pictures_count =[NSString stringWithFormat:@"%@",[self validStringForObject:responseData[@"post_pictures_count"]]];
       self.postPicturesArray = [Pictures mapPicturesFromArray:responseData[@"post_pictures"]];
        self.postsArray = [Post mapPostFromArray:responseData[@"posts"]];
          self.reviewsArray = [Reviews mapReviewsFromArray:responseData[@"reviews"]];
        self.rating = [responseData[@"rating"] floatValue];

    }
    
    return self;
}




+ (NSArray *)mapPlacesFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Places alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}


@end
