//
//  CustomTextView.m
//  Ajo
//
//  Created by Samreen on 04/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "CustomTextView.h"

@implementation CustomTextView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.layer.cornerRadius=5.0f;
    self.layer.masksToBounds=YES;
    self.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.layer.borderWidth= 1.0f;
    self.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
}


@end
