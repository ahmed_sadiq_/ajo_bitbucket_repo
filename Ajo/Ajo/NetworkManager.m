//
//  NetworkManager.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "NetworkManager.h"
#import "DataManager.h"
#import "Constant.h"

@implementation NetworkManager


+ (NSError *)createErrorMessageForObject:(id)responseObject
{
    
    NSString * responseStr = @"";
    
    if (responseObject[@"message"])
        responseStr = [NSString stringWithFormat:@"%@",responseObject[@"message"]];
    else
        responseStr = [NSString stringWithFormat:@"API Response Error!\n%@", responseObject];
    
    
    NSError *error = [NSError errorWithDomain:@"Failed!"
                                         code:100
                                     userInfo:@{
                                                // NSLocalizedDescriptionKey:responseObject[@"message"]
                                                
                                                NSLocalizedDescriptionKey:responseStr
                                                
                                                }];
    NSLog(@"Failed with Response: %@", responseObject);
    return error;
}


+(void) postURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    // [manager.requestSerializer setValue:[DataManager sharedManager].authToken forHTTPHeaderField:@"token"];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];

    [manager POST:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         
         NSLog(@"responseObject: %@", responseObject);
         
         BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
         
         if (_successCall){
             success(responseObject);
         }
         else
         {
             [SVProgressHUD dismiss];

             NSError *error = [self createErrorMessageForObject:responseObject];
             failure(error);
         }
         
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}

+(void)  getURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure{
    
    uri = [uri stringByAddingPercentEscapesUsingEncoding:
           NSUTF8StringEncoding];
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    [manager.requestSerializer setValue:[DataManager sharedManager].authToken forHTTPHeaderField:@"token"];
    
    [manager GET:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [SVProgressHUD dismiss];
         
         NSLog(@"responseObject: %@", responseObject);
         
         BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
         
         if (_successCall){
             success(responseObject);
         }
         else
         {
             NSError *error = [self createErrorMessageForObject:responseObject];
             failure(error);
         }
         
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}

+(void)  putURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    [manager.requestSerializer setValue:[DataManager sharedManager].authToken forHTTPHeaderField:@"token"];
    
    [manager PUT:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
     {
         [SVProgressHUD dismiss];
         
         NSLog(@"responseObject: %@", responseObject);
         
         BOOL _successCall = [[responseObject valueForKey:@"success"] boolValue] ;
         
         if (_successCall){
             success(responseObject);
         }
         else
         {
             NSError *error = [self createErrorMessageForObject:responseObject];
             failure(error);
         }
         
     }
         failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}

+(void) postURIWithFormData:(NSString *) uri
     parameters:(NSDictionary *) params
                   formData:(NSData *)formdata
        success:(loadSuccess) success
        failure:(loadFailure) failure{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    

    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [manager POST:uri parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:formdata name:KEY_PROFILE_IMG fileName:@"image.png" mimeType:@"image/png"];

    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];
        
        NSLog(@"responseObject: %@", responseObject);
        
        BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
        
        if (_successCall){
            success(responseObject);
        }
        else
        {
            NSError *error = [self createErrorMessageForObject:responseObject];
            failure(error);
        }

        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        [SVProgressHUD dismiss];
        
        NSLog(@"Failure: %@", error.localizedDescription);
        failure(error);

    }];
}
+(void) postURIWithFormDataArray:(NSArray *) formdataArray
                 parameters:(NSDictionary *) params
                   url:(NSString *)uri
                    success:(loadSuccess) success
                    failure:(loadFailure) failure{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    

    [manager POST:uri parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i=0; i<[formdataArray count]; i++) {
            NSString *picKey = [NSString stringWithFormat:@"picture[%d]",i];
            [formData appendPartWithFileData:[formdataArray objectAtIndex:i]
                                        name:picKey
                                    fileName:@"image.png"
                                    mimeType:@"image/png"];
        }
        
        //[formData appendPartWithFileData:formdata name:KEY_PROFILE_IMG fileName:@"image.png" mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];
        
        NSLog(@"responseObject: %@", responseObject);
        
        BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
        
        if (_successCall){
            success(responseObject);
        }
        else
        {
            NSError *error = [self createErrorMessageForObject:responseObject];
            failure(error);
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        [SVProgressHUD dismiss];
        
        NSLog(@"Failure: %@", error.localizedDescription);
        failure(error);
        
    }];
}
+(void) postNewPlaceURIWithFormDataArray:(NSArray *) formdataArray
                      parameters:(NSDictionary *) params
                             url:(NSString *)uri
                         success:(loadSuccess) success
                         failure:(loadFailure) failure{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    
    [manager POST:uri parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i=0; i<[formdataArray count]; i++) {
            NSString *picKey = [NSString stringWithFormat:@"place_picture[%d]",i];
            [formData appendPartWithFileData:[formdataArray objectAtIndex:i]
                                        name:picKey
                                    fileName:@"image.png"
                                    mimeType:@"image/png"];
        }
        
        //[formData appendPartWithFileData:formdata name:KEY_PROFILE_IMG fileName:@"image.png" mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];
        
        NSLog(@"responseObject: %@", responseObject);
        
        BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
        
        if (_successCall){
            success(responseObject);
        }
        else
        {
            NSError *error = [self createErrorMessageForObject:responseObject];
            failure(error);
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        [SVProgressHUD dismiss];
        
        NSLog(@"Failure: %@", error.localizedDescription);
        failure(error);
        
    }];
}
+(void) postFavPlacesURI:(NSString *) uri
     parameters:(NSDictionary *) params
        success:(loadSuccess) success
        failure:(loadFailure) failure{
    
    
    NSURL *baseURL = [[NSURL alloc] initWithString:BASE_URL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    
    // [manager.requestSerializer setValue:[DataManager sharedManager].authToken forHTTPHeaderField:@"token"];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager POST:uri parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         
         NSLog(@"responseObject: %@", responseObject);
         
         BOOL _successCall = [[responseObject valueForKey:@"status"] boolValue] ;
         
         if (_successCall){
             success(responseObject);
         }
         else
         {
             [SVProgressHUD dismiss];
             
             NSError *error = [self createErrorMessageForObject:responseObject];
             failure(error);
         }
         
     }
          failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         NSLog(@"Failure: %@", error.localizedDescription);
         failure(error);
     }];
}




@end
