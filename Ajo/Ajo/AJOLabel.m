//
//  AJOLabel.m
//  Ajo
//
//  Created by Samreen Noor on 01/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AJOLabel.h"

@implementation AJOLabel


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        
        CGFloat fontSize = self.font.pointSize;
        
        
        self.font = [UIFont fontWithName:@"OpenSans-Light" size:fontSize];
        
        
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
