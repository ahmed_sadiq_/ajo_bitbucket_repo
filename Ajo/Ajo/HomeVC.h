//
//  HomeVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouriteBaseController.h"
#import <MapKit/MapKit.h>

@interface HomeVC : FavouriteBaseController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *navImg;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationAllow;
@property (weak, nonatomic) IBOutlet UIView *plusBox;

@property (weak, nonatomic) IBOutlet UIButton *btnShowMap;
@property (strong, nonatomic) IBOutlet UITableView *mainTblView;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIView *searchBox;
@property (weak, nonatomic) IBOutlet UIView *placesBox;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNewPlace;

@end
