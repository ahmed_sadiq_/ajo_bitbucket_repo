//
//  AppDelegate.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "RecommendationVC.h"
#import "SettingsVC.h"
#import "ProfileVC.h"
#import "WalkThroughVC.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property ( strong , nonatomic ) UINavigationController *navigationController;
@property ( strong , nonatomic ) ViewController *viewController;
@property (strong, nonatomic) HomeVC *homeViewController;
@property (strong, nonatomic) WalkThroughVC *walkThroughVC;

@property ( strong , nonatomic ) UITabBarController *tabBarController;

@end

