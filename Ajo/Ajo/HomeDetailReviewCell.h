//
//  HomeDetailReviewCell.h
//  Ajo
//
//  Created by Ahmed Sadiq on 15/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"

@interface HomeDetailReviewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@property (strong, nonatomic) IBOutlet UIImageView *profileImg;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *desc;
@property (strong, nonatomic) IBOutlet ASStarRatingView *staticRatingView;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteReview;

@end
