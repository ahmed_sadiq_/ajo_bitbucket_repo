//
//  PlacesDetailHeader.h
//  Ajo
//
//  Created by Samreen on 31/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface PlacesDetailHeader : UITableViewCell
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *placeImg;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceDetail;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScroller;
@end
