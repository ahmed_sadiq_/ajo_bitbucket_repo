//
//  Reviews.m
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Reviews.h"

@implementation Reviews
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.review_id = [self validStringForObject:responseData[@"review_id"]];
        self.rating = [self validStringForObject:responseData[@"rating"]];
        self.text =[self validStringForObject:responseData[@"text"]];
        self.user_id =[self validStringForObject:responseData[@"user_id"]];
        self.place_id =[self validStringForObject:responseData[@"place_id"]];
        self.user_profile_pic =[self validStringForObject:responseData[@"user_profile_pic"]];

        
        
    }
    
    return self;
}




+ (NSArray *)mapReviewsFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Reviews alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

@end
