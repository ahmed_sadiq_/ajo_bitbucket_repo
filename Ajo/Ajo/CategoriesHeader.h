//
//  CategoriesHeader.h
//  Ajo
//
//  Created by Samreen on 03/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesHeader : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCatName;
@property (weak, nonatomic) IBOutlet UIImageView *catImg;
@property (weak, nonatomic) IBOutlet UIButton *btnShowSubCategory;
@property (weak, nonatomic) IBOutlet UIImageView *btndropDown;

@end
