//
//  Pictures.m
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Pictures.h"

@implementation Pictures
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.date_time = [self validStringForObject:responseData[@"date_time"]];
        self.mainPicture_text = [self validStringForObject:responseData[@"picture_text"]];
        self.place_id =[self validStringForObject:responseData[@"place_id"]];
        self.post_picture_id =[self validStringForObject:responseData[@"post_picture_id"]];
        self.user_id =[self validStringForObject:responseData[@"user_id"]];
        self.user_name =[self validStringForObject:responseData[@"user_name"]];
        self.user_profile_pic =[self validStringForObject:responseData[@"user_profile_pic"]];

        
        
    self.picturesArray = [self mapArrayOfPics:responseData[@"pictures"]];

        
    }
    
    return self;
}




+ (NSArray *)mapPicturesFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Pictures alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

-(NSArray *) mapArrayOfPics:(NSArray *)picsArray{
    NSMutableArray * mappedArr = [NSMutableArray new];

    for (NSDictionary *dic in picsArray) {
        Pictures *pic =[[Pictures alloc]init];
        pic.picture =[self validStringForObject:dic[@"picture"]];
        pic.picture_id =[self validStringForObject:dic[@"picture_id"]];
        [mappedArr addObject:pic];
    }
    return mappedArr;
}


@end
