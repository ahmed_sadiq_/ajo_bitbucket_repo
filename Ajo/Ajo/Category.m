//
//  Category.m
//  Ajo
//
//  Created by Samreen on 03/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "Category.h"

@implementation Category

- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.categoryName = responseData[@"CategoryName"];
        self.categoryImg =responseData[@"categoryImg"];

        NSArray * catArray = responseData[@"SubCategory"];
        
            self.subCategory = [self mapSubCategoryFromArray:catArray];
        
        
        
        
        
    }
    
    return self;
}




+ (NSArray *)mapCategoryFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Category alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}
- (NSMutableArray *)mapSubCategoryFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSString *subCat in arrlist) {
        [mappedArr addObject:subCat];
    }
    
    return mappedArr;
    
}
@end
