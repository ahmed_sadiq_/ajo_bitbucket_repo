//
//  PlaceService.m
//  Ajo
//
//  Created by Samreen Noor on 10/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "PlaceService.h"
#import "Places.h"
#import "PostModel.h"
#import "DataManager.h"
#import "SVProgressHUD.h"
@implementation PlaceService



+(void) getDiscoverPlacesWithLocation:(NSString *)location
                              Radius:(NSString *)radius
                            Pagetoken:(NSString *)pagetoken
                           success:(serviceSuccess)success
                           failure:(serviceFailure)failure {
    User *user = [DataManager sharedManager].currentUser;

    NSDictionary * discovrParam = @{
                                    @"location":location,
                                   // @"radius":radius,
                                    @"first_call":pagetoken,
                                      @"user_id": user.userID
                                    };
    NSLog(@"home params..%@",discovrParam);
    
    [NetworkManager postURI:URL_DISCOVER
                 parameters:discovrParam
                    success:^(id data) {
                        
                        
                        BOOL status=data[@"status"];
                        if (status) {
                            [DataManager sharedManager].discoverPageToken = data[@"pagetoken"];
                              NSMutableArray *placesList = [Places mapPlacesFromArray:data[@"places"]].mutableCopy;
                            
                            success (placesList);
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) getRecommendationPlacesWithLocation:(NSString *)location
                                  pageToken:(NSString *)token
                               Radius:(NSString *)radius
                              success:(serviceSuccess)success
                              failure:(serviceFailure)failure {
    
    NSDictionary * discovrParam = @{
                                    @"location":location,
                                    @"radius":radius,
                                    @"pagetoken":token
                                    
                                    
                                    };
    
    [NetworkManager postURI:URL_RECOMMENDATION
                 parameters:discovrParam
                    success:^(id data) {
                        
                        
                        BOOL status=data[@"status"];
                        if (status) {
                            
                            
                            NSMutableArray *placesList = [Places mapPlacesFromArray:data[@"places"]].mutableCopy;
                            
                            success (placesList);
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) filterRecommendationPlacesWithCountry:(NSString *)country
                                     city:(NSString *)city
                                      andType:(NSString *)type
                                    pageToken:(NSString *)token
                                    success:(serviceSuccess)success
                                    failure:(serviceFailure)failure {
    
    NSDictionary * discovrParam;
    if ([city isEqualToString:@""]&&[type isEqualToString:@""]&&![country isEqualToString:@""]) {
                     discovrParam = @{
                                        @"country":country,
                                        @"pagetoken":token

                                    
                                        };
    }
    else  if ([city isEqualToString:@""]&&![country isEqualToString:@""]&&![type isEqualToString:@""]) {
                    discovrParam = @{
                                        @"country":country,
                                        @"type":type,
                                        @"pagetoken":token

                                        
                                        };
    }
    else   if (![city isEqualToString:@""]&&![country isEqualToString:@""]&&[type isEqualToString:@""]) {

                 discovrParam = @{
                                    @"country":country,
                                    @"city":city,
                                    @"pagetoken":token

                                    };
        
    }
    else{
                discovrParam = @{
                                        @"country":country,
                                        @"city":city,
                                        @"type":type,

                                        @"pagetoken":token

                                        };

    }
    
    [NetworkManager postURI:URL_RECOMMENDATION
                 parameters:discovrParam
                    success:^(id data) {
                        
                        
                        BOOL status=data[@"status"];
                        if (status) {
                            
                            
                            NSMutableArray *placesList = [Places mapPlacesFromArray:data[@"places"]].mutableCopy;
                            
                            success (placesList);
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}

+(void) getPlacesDetailWithPlaceID:(NSString *)placeId
                                    success:(serviceSuccess)success
                                    failure:(serviceFailure)failure {
    
    User *user = [DataManager sharedManager].currentUser;

    
    NSDictionary *placeParam = @{
                                    @"place_id":placeId,
                                    @"user_id" :user.userID
                                    };
    
    [NetworkManager postURI:URL_PLACES_DETAIL
                 parameters:placeParam
                    success:^(id data) {

                        
                  
                            success (data);
                        
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) getRecommendationDetailWithPlaceID:(NSString *)placeId
                           success:(serviceSuccess)success
                           failure:(serviceFailure)failure {
    
    
    
    NSDictionary *placeParam = @{
                                 @"place_id":placeId
                                 
                                 };
    
    [NetworkManager postURI:URL_RECOMMENDATION_DETAIL
                 parameters:placeParam
                    success:^(id data) {
                        
                        [SVProgressHUD dismiss];

                        BOOL status=data[@"status"];
                        if (status) {
//                            NSMutableArray *placeDetail = [[NSMutableArray alloc]init];
//                            NSArray *reviewArray=data[@"places"][@"reviews"];
//                            for (NSDictionary *dic in reviewArray) {
//                                [placeDetail addObject:[[PostModel alloc]initWithDictionaryReviews:dic]];
//                            }
//                            NSArray *postArray=data[@"places"][@"posts"];
//                            for (NSDictionary *dic in postArray) {
//                                [placeDetail addObject:[[PostModel alloc]initWithDictionaryPost:dic]];
//                            }
//                            NSArray *picsArray=data[@"places"][@"post_pictures"];
//                            for (NSDictionary *dic in picsArray) {
//                                [placeDetail addObject:[[PostModel alloc]initWithDictionaryPics:dic]];
//                            }
//                            
//                            
                            
                         //   success (placeDetail);
                            success(data);
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}

+(void) searchPlacesWithUrl:(NSString *)uri
                   Location:(NSString *)location
                  SearchKeyword:(NSString *)keyword
                          Radius:(NSString *)radius
                       PageToken:(NSString *)pagetoken
                                    success:(serviceSuccess)success
                                    failure:(serviceFailure)failure {
    
    User *user=[DataManager sharedManager].currentUser;
   
    NSDictionary * searchparam = @{
                                    @"location":location,
                                    @"user_id": user.userID,
                                    @"keyword":keyword,
                                    @"radius" : radius,
                                    @"pagetoken":pagetoken
                                    };
    
    [NetworkManager postURI:uri
                 parameters:searchparam
                    success:^(id data) {
                        

                        BOOL status=data[@"status"];
                        if (status) {
                            NSMutableArray *placesList = [Places mapPlacesFromArray:data[@"places"]].mutableCopy;
                            
                            success (placesList);
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}
+(void) searchPlacesFromServerWithUrl:(NSString *)uri
              SearchKeyword:(NSString *)keyword
                  PageToken:(NSString *)pagetoken
                    success:(serviceSuccess)success
                    failure:(serviceFailure)failure {
    
    User *user=[DataManager sharedManager].currentUser;
    
    NSDictionary * searchparam = @{
                                   @"user_id": user.userID,
                                   @"keyword":keyword,
                                   @"pagetoken":pagetoken
                                   };
    
    [NetworkManager postURI:uri
                 parameters:searchparam
                    success:^(id data) {
                        
                        
                        BOOL status=data[@"status"];
                        if (status) {
                            NSMutableArray *placesList = [Places mapPlacesFromArray:data[@"places"]].mutableCopy;
                            
                            success (placesList);
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}



+(void) addNewPlaceWithPlaceName:(NSString *)placename
                       Description:(NSString *)des
                      latitude:(NSString *)lat
                       Longitude:(NSString *)longi
                       Type:(NSString *)type
                        SubCatgory:(NSString *)subCat
                       Website:(NSString *)web
                       Country:(NSString *)country
                       City:(NSString *)city
                            PhotoArray:(NSArray *)photosArray
                           Email:(NSString *)email
                        Address:(NSString *)address
                           Phone:(NSString *)phone
                        success:(serviceSuccess)success
                        failure:(serviceFailure)failure{
    
    User *user=[DataManager sharedManager].currentUser;
    NSMutableArray *formDataArray = [[NSMutableArray alloc]init];
    if (lat==nil) {
        lat=@"";
        longi=@"";
    }
    NSDictionary * postParam = @{
                                 @"name":placename,
                                 @"description":des,
                                 @"country":country,
                                 @"city":city,
                                 @"type":type,
                                 @"sub_category":subCat,
                                 @"latitude":lat,
                                 @"longitude":longi,
                                 @"website":web,
                                 @"address":address,
                                 @"phone_number":phone,
                                 @"email":email
                                 };
    
    if (photosArray.count>=1) {
    
    for (int i=0; i<[photosArray count]; i++) {
        NSData *imageData = UIImagePNGRepresentation(photosArray[i]);
        [formDataArray addObject:imageData];
    }
    
    
    [NetworkManager postNewPlaceURIWithFormDataArray:formDataArray parameters:postParam url:URL_UPLOAD_NEWPLACE success:^(id data) {
            [SVProgressHUD dismiss];
        success (data);

    } failure:^(NSError *error) {
        failure(error);

    }];
    
    }
    
    else{
    [NetworkManager postURI:URL_UPLOAD_NEWPLACE parameters:postParam success:^(id data) {
            [SVProgressHUD dismiss];
        success (data);

    } failure:^(NSError *error) {
        failure(error);

    }];
    
    }
}


+(void) getFavPlacesWithPagetoken:(NSString *)pagetoken
                              success:(serviceSuccess)success
                              failure:(serviceFailure)failure {
    User *user = [DataManager sharedManager].currentUser;
    
    NSDictionary * discovrParam = @{
                                    @"page_no":pagetoken,
                                    @"user_id": user.userID
                                    };
    
    [NetworkManager getURI:URL_GET_FAV_PLACES parameters:discovrParam success:^(id data) {
        
        BOOL status=data[@"status"];
        if (status) {
            
            NSMutableArray *placesList = [Places mapPlacesFromArray:data[@"places"]].mutableCopy;
            
            success (placesList);
        }
        
        
    } failure:^(NSError *error) {
        
        failure(error);

        
    }];
    
    
}
+(void) addFavPlacesWithPlaceID:(NSString *)placeId
                          success:(serviceSuccess)success
                          failure:(serviceFailure)failure {
    User *user = [DataManager sharedManager].currentUser;
    
    NSDictionary * discovrParam = @{
                                    @"place_id":placeId,
                                    @"user_id": user.userID
                                    };
    
    [NetworkManager postFavPlacesURI:URL_ADD_FAV_PLACES parameters:discovrParam success:^(id data) {
        
      
            success (data);
        
    } failure:^(NSError *error) {
        failure(error);

    }];
    
 
    
    
}

+(void) searchPlacesWithUrl:(NSString *)uri
                   PageToken:(NSString *)page
                   Location:(NSString *)location
              SearchKeyword:(NSString *)keyword
                     Category:(NSString *)category
                  SubCategory:(NSString *)subCategory
                    success:(serviceSuccess)success
                    failure:(serviceFailure)failure {
    
    User *user=[DataManager sharedManager].currentUser;
    
    NSDictionary * searchparam = @{
                                   @"location":location,
                                   @"user_id": user.userID,
                                   @"keyword":keyword,
                                   @"category" : category,
                                  @"sub_category":subCategory,
                                   @"first_call":page

                                   };
    
    [NetworkManager postURI:uri
                 parameters:searchparam
                    success:^(id data) {
                        
                        [SVProgressHUD dismiss];

                        BOOL status=data[@"status"];
                        if (status) {
                            NSMutableArray *placesList = [Places mapPlacesFromArray:data[@"places"]].mutableCopy;
                            
                            success (placesList);
                        }
                        
                        
                        
                        
                    } failure:^(NSError *error) {
                        
                        
                        
                        failure(error);
                        
                        
                    }];
    
    
}


@end
