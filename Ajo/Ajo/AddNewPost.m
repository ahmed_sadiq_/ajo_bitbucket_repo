//
//  AddNewPost.m
//  Ajo
//
//  Created by Samreen Noor on 03/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AddNewPost.h"
#import "PostService.h"
#import "UIImageView+URL.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "RecommendationVC.h"
#import "HomeVC.h"
#import "DataManager.h"
#import "Pictures.h"
@implementation AddNewPost
{
    HomeDetailVC *homeDetailVc;
    HomeVC *homeVC;
    RecommendationVC *recommendationVC;
    BOOL isHomeVc;
    BOOL isHomeDetailVc;
    BOOL isRecommendationVc;
    float reviewRating;
    NSString *postTypeStr;

}

-(void) setView{
    _postViewBottomConstrain.constant = -200;
    Pictures *pic;
    if (_postPlace.place_picture.count>0) {
        pic= _postPlace.place_picture[0];

    }
    [_placeImg setImageWithStringURL:pic.picture];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self addGestureRecognizer:tap];
    }



-(void)dismissKeyboard {
    
    
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        [_tfDescription resignFirstResponder];
        [_tfPhotoDescription resignFirstResponder];

        _SubmitBottomConstrain.constant = 42;
        _submitPhotoBottomConstrain.constant =42;
    } completion:^(BOOL finished) {
        
        
        
    }];
}

- (IBAction)btnHide:(id)sender {
    _postViewBottomConstrain.constant = -200;
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        self.alpha = 0.0;
        
        
    }];

    
    [self hide];
    
    
}


+ (id) loadWithNibWithDelegate:(id )delegate From:(NSString *)controllerStr andPlace:(Places *)place{
    AddNewPost *view = (AddNewPost *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AddNewPost class]) owner:nil options:nil][0];
    
    view.alpha = 0.0;
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.postPlace = place;
    
    if ([controllerStr isEqualToString:@"HomeDetailVC"]) {
        
        view->homeDetailVc = delegate;
        view->isHomeDetailVc = YES;
        view->isHomeVc = NO;
        view->isRecommendationVc = NO;


    }
    else if ([controllerStr isEqualToString:@"HomeVC"]) {
        
        view->homeVC = delegate;
        view->isHomeVc = YES;
        view->isHomeDetailVc = NO;
        view->isRecommendationVc = NO;
        
        
    }
    else if ([controllerStr isEqualToString:@"RecommendationVC"]) {
        
        view->recommendationVC = delegate;
        view->isRecommendationVc = YES;
        view->isHomeDetailVc = NO;
        view->isHomeVc = NO;
        
        
    }

    [view setView];
    [view setUpRtaing];

    
    
    return view;
    
}

+ (id) loadWithNibWithDelegate:(id )delegate From:(NSString *)controllerStr andPlace:(Places *)place andPostString:(NSString *)postStr{
    AddNewPost *view = (AddNewPost *)[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([AddNewPost class]) owner:nil options:nil][0];
    
    view.alpha = 0.0;
    
    [view setFrame:[UIScreen mainScreen].bounds];
    
    view.postPlace = place;
    
    if ([controllerStr isEqualToString:@"HomeDetailVC"]) {
        
        view->homeDetailVc = delegate;
        view->isHomeDetailVc = YES;
        view->isHomeVc = NO;
        view->isRecommendationVc = NO;
        view->postTypeStr = postStr;
        
    }
    
    [view setView];
    
    if ([postStr isEqualToString:@"Review"]) {
        view.ratingView.hidden = NO;
        [view setUpRtaing];
        
           // [view.delegate moveToNewPostTab:@"1"];
        
        [view showReviewView];
    }
    else  if ([postStr isEqualToString:@"Post"]) {
       // [view.delegate moveToNewPostTab:@"2"];

        view.ratingView.hidden = YES;
        [view showReviewView];
        
    }
    else{
       // [view.delegate moveToNewPostTab:@"3"];

        [view showPhotoView];

    }
    
    return view;
    

    
    
}

-(void) show{
    

    self.alpha = 1.0;

    [UIView animateKeyframesWithDuration:0.5 delay:0.50 options:0 animations:^{
        
        if (isHomeVc) {
            homeVC.navigationController.tabBarController.tabBar.hidden = YES;
 
        }
        else if (isRecommendationVc){
        recommendationVC.navigationController.tabBarController.tabBar.hidden = YES;
        }
        else
            _postViewBottomConstrain.constant = 0;


    } completion:^(BOOL finished) {
        _postViewBottomConstrain.constant = 0;

    }];
    
}


-(void) hide{
    
    
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        _postViewBottomConstrain.constant = -150;
        
    } completion:^(BOOL finished) {
        homeVC.navigationController.tabBarController.tabBar.hidden = NO;
        recommendationVC.navigationController.tabBarController.tabBar.hidden = NO;

        self.alpha = 0.0;
        
        
    }];
}

-(void) showReviewView{
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        self.reviewView.hidden = false;
        
    } completion:^(BOOL finished) {
        
        
        
    }];


}

-(void) hideReviewView{
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        self.reviewView.hidden = true;
        self.tfDescription.text =@"";
        
    } completion:^(BOOL finished) {
        
        
        
    }];
    


}

-(void) showPhotoView{
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        self.photoView.hidden = NO;
        
    } completion:^(BOOL finished) {
        
        
        
    }];
    
    
    
}-(void) hidePhotoView{
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        self.photoView.hidden = YES;
        self.tfPhotoDescription.text =@"";

    } completion:^(BOOL finished) {
        
        
        
    }];
    
    
    
}


#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([_tfDescription isEqual:textField]) {
    
    
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        _SubmitBottomConstrain.constant = 200;
        
    } completion:^(BOOL finished) {
        

        
    }];
    }
    else{
    
        
        [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
            
            _submitPhotoBottomConstrain.constant = 200;
            
        } completion:^(BOOL finished) {
            
            
            
        }];

    }

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([_tfDescription isEqual:textField]) {

    
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        _SubmitBottomConstrain.constant = 42;
        
    } completion:^(BOOL finished) {
        
        
        
    }];
    }
    else {
    
        [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
            
            _submitPhotoBottomConstrain.constant = 42;
            
        } completion:^(BOOL finished) {
            
            
            
        }];
    
    
    }

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)btnAddReview:(id)sender {
    if (isHomeDetailVc) {

    //[self.delegate moveToNewPostTab:@"1"];
    }
    _ratingView.hidden = NO;
    [self showReviewView];

    
}

- (IBAction)btnAddPost:(id)sender {
    _ratingView.hidden = YES;
    if (isHomeDetailVc) {

   // [self.delegate moveToNewPostTab:@"2"];
    }
    [self showReviewView];
    
}


- (IBAction)btnPostBack:(id)sender {
    [_tfDescription setText:@""];
    [_tfDescription resignFirstResponder];
    [self back];

    [self hideReviewView];
}


- (IBAction)btnAddPhoto:(id)sender {
    if (isHomeDetailVc) {

   // [self.delegate moveToNewPostTab:@"3"];
    }
    [self showPhotoView];
}


- (IBAction)btnSubmit:(id)sender {
    
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *date=  [dateFormatter stringFromDate:[NSDate date]];
    
    if (_ratingView.hidden == YES) {
    
        if ([_tfDescription.text isEqualToString:@""]) {
            [self showMessage:@"Review text must be required."];
        }
        else{
        
    [PostService addNewPostWithPlaceId:_postPlace.place_id PostText:_tfDescription.text DateTime:date success:^(id data) {
        
        int  count = [_postPlace.postCount intValue];
        count++;
        [_postPlace setPostCount:[NSString stringWithFormat:@"%d",count]];
        [self.delegate addNewPost:data ];
        [DataManager sharedManager].postObj = @"Post";
        [self back];

        [self hideReviewView];

   
    } failure:^(NSError *error) {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        [alertView show];
    }];
        }
    }
    else{
        
        if ([_tfDescription.text isEqualToString:@""]) {
            [self showMessage:@"Post text must be required."];

        }
        else{
        NSString *rat = [NSString stringWithFormat:@"%f",reviewRating];
    [PostService addNewPostWithPlaceId:_postPlace.place_id Rating:rat PostText:_tfDescription.text DateTime:date success:^(id data) {
        
        [self.delegate addNewPost:data ];
        [DataManager sharedManager].postObj = @"Review";
        [self back];

        [self hideReviewView];


        
        
    } failure:^(NSError *error) {
       
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        [alertView show];

        
    }];
        }
    
    }
    
    
}

- (IBAction)btnChoosePhotos:(id)sender {
    
    
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 100; //Set the maximum number of images to select to 100
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
    
    elcPicker.imagePickerDelegate = self;
    
    if (isHomeVc) {
        [homeVC presentViewController:elcPicker animated:YES completion:nil];

    }
    else if (isHomeDetailVc){
    
    [homeDetailVc presentViewController:elcPicker animated:YES completion:nil];
    }
    else{
        [recommendationVC presentViewController:elcPicker animated:YES completion:nil];

    }

}

- (IBAction)btnBackFromPhotoView:(id)sender {
    [_tfPhotoDescription resignFirstResponder];
    [_tfPhotoDescription setText:@""];
    [self back];

    [self hidePhotoView];
    
}



- (IBAction)btnSubmitPhotos:(id)sender {
    if ([_tfPhotoDescription.text isEqualToString:@""]) {
        
        [self showMessage:@"Type something text must be required."];
    }
    else if (photoArray.count ==0){
        [self showMessage:@"Please choose some photo"];
    }
    else{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *date=  [dateFormatter stringFromDate:[NSDate date]];

    
    [PostService addNewPhotosWithPlaceId:_postPlace.place_id Pictures:photoArray PhotoText:_tfPhotoDescription.text DateTime:date success:^(id data) {
        
        int  count = [_postPlace.post_pictures_count intValue];
        count++;
        [_postPlace setPost_pictures_count:[NSString stringWithFormat:@"%d",count]];
        [self.delegate addNewPost:data];
        [DataManager sharedManager].postObj = @"Photo";
        [self back];
        [self hidePhotoView];
        
        
    } failure:^(NSError *error) {
        
        
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        [alertView show];
        
    }];
    }
}

-(void) setUpScrollView{
    int scrollWidth = 100;
    //scrollVie.contentSize = CGSizeMake(scrollWidth,100);
    _scrrollView.frame=CGRectMake(0, 51, self.frame.size.width, 200);
   // _scrrollView.backgroundColor=[UIColor redColor];
    
    
    
    
    int xOffset = 0;
    
    for(int index=0; index < [photoArray count]; index++)
    {
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(xOffset,10,160, 200)];
        img.image = (UIImage*) [photoArray objectAtIndex:index];
        
        [_scrrollView addSubview:img];
        
        xOffset+=170;
        
        
    }
    
    
    _scrrollView.contentSize = CGSizeMake(scrollWidth+xOffset,200);

}

- (void)rateViewratingDidChange:(float)rating {
    
    NSLog(@"%f",rating);
    reviewRating = rating;
    //self.statusLabel.text = [NSString stringWithFormat:@"Rating: %f", rating];
}
-(void)setUpRtaing{
    _ratingView.canEdit = YES;
    _ratingView.maxRating = 5;
    _ratingView.rating = 0.0;
    reviewRating = 0.0;

    _ratingView.delegate = self;

}



#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
   [picker dismissViewControllerAnimated:YES completion:nil];
    
    for (UIView *v in [_scrrollView subviews]) {
        [v removeFromSuperview];
    }
    
    CGRect workingFrame = _scrrollView.frame;
    workingFrame.origin.x = 0;
    
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                [images addObject:image];
                
                UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
                [imageview setContentMode:UIViewContentModeScaleAspectFit];
                imageview.frame = workingFrame;
                
               // [_scrrollView addSubview:imageview];
                
                workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                
                [images addObject:image];
                
                UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
                [imageview setContentMode:UIViewContentModeScaleAspectFit];
                imageview.frame = workingFrame;
                
               // [_scrrollView addSubview:imageview];
                
                workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        }
        else {
            NSLog(@"Uknown asset type");
        }
    }
    
    photoArray = images;
    
    [_scrrollView setPagingEnabled:YES];
    [self setUpScrollView];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void) showMessage:(NSString *)msg{

    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
    [alertView show];

}

-(void) back{
    _postViewBottomConstrain.constant = -200;
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        self.alpha = 0.0;
        
    } completion:^(BOOL finished) {
        
        self.alpha = 0.0;
        
        
    }];
    
    
    [self hide];
    
}
@end
