//
//  HomeDetailVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 15/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "HomeDetailVC.h"
#import <SafariServices/SafariServices.h>
#import "HomeDetailReviewCell.h"
#import "HomeDetailPostCell.h"
#import "PostModel.h"
#import "HomeDetailPhotoCell.h"
#import "UIImageView+RoundImage.h"
#import "PlaceService.h"
#import "UIImageView+URL.h"
#import "Pictures.h"
#import "PostService.h"
#import "DataManager.h"
#import "AsyncImageView.h"
#import "SVProgressHUD.h"
#import "PlacesDetailHeader.h"
#import "EmptyPostCell.h"
#import "UIButton+Style.h"
#import "UIView+Style.h"
#import <MapKit/MapKit.h>

@interface HomeDetailVC ()<AddNewPostDelegate,MKMapViewDelegate>

{
    AddNewPost *addNewPost;
    NSUInteger currentSelectedIndex;
    NSUInteger currentPhotoSelectedIndex;

    NSMutableArray *selectedPhotoArray;
    PostModel *selectedPhotoPost;
    CGRect workingFrame;
    CGRect ScrollViewOriginalFrame;
    NSString *completePlaceDeatil;
    PEARImageSlideViewController *slideImageViewController;

}

@end


@implementation HomeDetailVC
@synthesize staticStarRatingView;
- (void)viewDidLoad {
    [super viewDidLoad];
    slideImageViewController = [PEARImageSlideViewController new];
    NSArray *imageLists = @[
                            [UIImage imageNamed:@"sample1.jpg"],
                            [UIImage imageNamed:@"sample2.jpg"],
                            [UIImage imageNamed:@"sample3.jpg"],
                            [UIImage imageNamed:@"sample4.jpg"],
                            [UIImage imageNamed:@"sample5.jpg"]
                            ].copy;
    
    [slideImageViewController setImageLists:imageLists];
    // Do any additional setup after loading the view from its nib.
    staticStarRatingView.canEdit = YES;
    staticStarRatingView.maxRating = 5;
    staticStarRatingView.rating = 2.5;
    _filteredArray = [[NSMutableArray alloc] init];
    _postDataSource = [[NSMutableArray alloc] init];
    [self setupView];
    if ([_photoCount.text isEqualToString:@"0"]&&[_reviewCount.text isEqualToString:@"0"]&&[_postCount.text isEqualToString:@"0"]) {
        _emptyPostView.hidden = NO;
    }
    

    
    if (_isFromDiscoverVc) {
        [self getPlaceDetail];

    }
    else{
    [self getRecommendationDetail];
    
}
        _menuBgImgView.layer.cornerRadius = 35.0/2;
    _menuBgImgView.layer.masksToBounds = YES;
    _menuInfoBgImgView.layer.cornerRadius = 35.0/2;
    _menuInfoBgImgView.layer.masksToBounds = YES;
   // [self fillDummyData];
}
-(void)viewDidLayoutSubviews{
    workingFrame = _photoScrollView.frame;
    ScrollViewOriginalFrame = _photoScrollView.frame;
    [self setUpImageScroll];


}
-(void) setupView{
    //[self.placeImg setImageWithStringURL:_place.place_picture];
    self.placeName.text = _place.placeName;
    self.lblTitle.text = _place.placeName;
    self.photoCount.text = _place.post_pictures_count;
    self.postCount.text = _place.postCount;
    self.reviewCount.text = _place.reviewCount;
    staticStarRatingView.rating = _place.rating;
    completePlaceDeatil = [NSString stringWithFormat:@"%@ %@ %@",_place.placeAddress,_place.placePhone,_place.placeEmail];
    [_plusBox roundCorner:30.0];

}
- (void) fillDummyData {
    _postDataSource = [[NSMutableArray alloc] init];
    _filteredArray = [[NSMutableArray alloc] init];
    for(int i=0; i<10; i++) {
        
        PostModel *pModel = [[PostModel alloc] init];
        
        
        if(i%3 == 0) {
            pModel.postTyper = 1;
            
        }
        else if(i%3 == 1) {
            pModel.postTyper = 2;
            
        }
        else {
            pModel.postTyper = 3;
            
        }
        
        [_filteredArray addObject:pModel];
        [_postDataSource addObject:pModel];
    }
    [_mainTblView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) setupTitleColor:(NSString *)post{
    if ([post isEqualToString:@"1"]) {
        
    _lblReviewTitle.textColor = [UIColor colorWithRed:181.0/255.0 green:196.0/255.0 blue:50.0/255.0 alpha:1];
    _reviewCount.textColor = [UIColor colorWithRed:181.0/255.0 green:196.0/255.0 blue:50.0/255.0 alpha:1];
    _lblPostTitle.textColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    _postCount.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    _lblPhotoTitle.textColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
    _photoCount.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        
    }
    else  if ([post isEqualToString:@"2"]) {
        _lblPostTitle.textColor = [UIColor colorWithRed:181.0/255.0 green:196.0/255.0 blue:50.0/255.0 alpha:1];
        _postCount.textColor = [UIColor colorWithRed:181.0/255.0 green:196.0/255.0 blue:50.0/255.0 alpha:1];
        _lblReviewTitle.textColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
        _reviewCount.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        _lblPhotoTitle.textColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
        _photoCount.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        
        
    }
    else{
        _lblPhotoTitle.textColor = [UIColor colorWithRed:181.0/255.0 green:196.0/255.0 blue:50.0/255.0 alpha:1];
        _photoCount.textColor = [UIColor colorWithRed:181.0/255.0 green:196.0/255.0 blue:50.0/255.0 alpha:1];
        _lblReviewTitle.textColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
        _reviewCount.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        _lblPostTitle.textColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];
        _postCount.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        

    
    }

}


-(void) setUpPostEmptyView{

    _emptyPostView.hidden = NO;
    _lblEmptyPostTitle.text=@"No one has posted about this place yet, be the first one to add a post.";
    [_btnEmptyPost setTitle:@"Add Post" forState:UIControlStateNormal];
}
-(void) setUpPhotoEmptyView{
    _emptyPostView.hidden = NO;
    _lblEmptyPostTitle.text=@"No one has posted any photo yet, be the first one to add a photo.";
    [_btnEmptyPost setTitle:@"Add Photo" forState:UIControlStateNormal];
    
}

-(void) setUpReviewEmptyView{
    _emptyPostView.hidden = NO;
    _lblEmptyPostTitle.text=@"No one has rated this place yet, be the first one to add a review.";
    [_btnEmptyPost setTitle:@"Add Review" forState:UIControlStateNormal];
    
}

-(void) setUpImageScroll{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    _pager.numberOfPages=_place.place_picture.count;

    _pager.currentPage=0;
    if (_place.place_picture.count==1) {
        _pager.hidden=YES;
    }
    if (_place.place_picture.count==0) {
        UIImageView   *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screenWidth,_imageScroller.frame.size.height )];
        
        [imgView setImageWithStringURL:@""];
        [_imageScroller addSubview:imgView];

        _imageScroller.contentSize = CGSizeMake(screenWidth, _imageScroller.frame.size.height);

    }

    _imageScroller.contentSize = CGSizeMake((screenWidth*_place.place_picture.count), _imageScroller.frame.size.height);
    CGFloat x=0;
    for (Pictures *pic in _place.place_picture) {
        UIImageView   *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0, screenWidth, _imageScroller.frame.size.height)];
        
        [imgView setImageWithStringURL:pic.picture];
        [_imageScroller addSubview:imgView];
        x = x+screenWidth;
    }

}


-(void) setUpinfoView{
    _detailViewTitle.text=_place.placeName;
    _lblPlaceDescription.text = _place.placeDescription;
    CGSize size = [_lblPlaceDescription.text sizeWithFont:[UIFont fontWithName:@"Avenir-Light" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    _descriptionHeightContrain.constant = size.height+20.0;
    if ([_place.placePhone isEqualToString:@""]) {
        _phoneViewHeight.constant=0;
    }
    if ([_place.placeEmail isEqualToString:@""]) {
        _emailViewHeight.constant=0;
    }
    if ([_place.placeAddress isEqualToString:@""]) {
        _addressViewHeight.constant=0;
    }
    if ([_place.placeWebsite isEqualToString:@""]) {
        _websiteViewHeight.constant=0;
    }
    _lblplacephone.text =_place.placePhone;
    _lblPlaceEmail.text= _place.placeEmail;
    _lblPlaceAddress.text = _place.placeAddress;
    CGSize size2 = [_lblPlaceAddress.text sizeWithFont:[UIFont fontWithName:@"Avenir-Light" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    
    _addressViewHeight.constant = size2.height+40.0;

    
    _lblPlaceWebsite.text =_place.placeWebsite;
    if ([_place.placeDescription isEqualToString:@""]&&[_place.placePhone isEqualToString:@""]&&[_place.placeEmail isEqualToString:@""]&&[_place.placeAddress isEqualToString:@""]&&[_place.placeWebsite isEqualToString:@""]) {
        [self showAlertMessage:@"Place details not available."];
    }
    else{
        _placesDetailViewHeight.constant = _placesDetailViewHeight.constant + size.height-100;
    _placeDetilShowView.hidden=NO;
    }
    if (![_place.placeDescription isEqualToString:@""]&&[_place.placePhone isEqualToString:@""]&&[_place.placeEmail isEqualToString:@""]&&[_place.placeAddress isEqualToString:@""]&&[_place.placeWebsite isEqualToString:@""]){
        _phoneViewHeight.constant=0;
        _emailViewHeight.constant=0;
        _addressViewHeight.constant=0;
        _websiteViewHeight.constant=0;
        _aboutTitleViewHeight.constant=0;
        _placesDetailViewHeight.constant = _placesDetailViewHeight.constant + size.height-200;
        _aboUtView.hidden=YES;


    }
    if ([_place.placeDescription isEqualToString:@""]){
        _descriptionHeightContrain.constant=0;
        _descriptionTitleViewHeight.constant=0;
        _desView.hidden=YES;
    }

}



-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //int indexOfPage = _imageScroller.contentOffset.x / _imageScroller.frame.size.width;
    //_pager.currentPage = indexOfPage;
}
- (IBAction)changePage:(id)sender {
    
}
- (IBAction)btnBackInfoView:(id)sender {
    _placeDetilShowView.hidden=YES;
}
- (IBAction)backPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


- (IBAction)btnPlaceInfoPressed:(id)sender {
    [self setUpinfoView];
}

- (IBAction)tab1Pressed:(id)sender {
  
    [self setupTitleColor:@"1"];
    
    if ([_reviewCount.text isEqualToString:@"0"]) {
        [self setUpReviewEmptyView];
       // _mainTblView.hidden= YES;

    }
    else{
        _emptyPostView.hidden = YES;
       // _mainTblView.hidden= NO;

    [_filteredArray removeAllObjects];
    for(int i=0; i<_postDataSource.count; i++) {
        
        PostModel *pModel = [_postDataSource objectAtIndex:i];
        
        
        if(pModel.postTyper == 1 ) {
            [_filteredArray addObject:pModel];
            
        }
    }
    [_mainTblView reloadData];
    }
}

- (IBAction)tab2Pressed:(id)sender {
    [self setupTitleColor:@"2"];
    if ([_postCount.text isEqualToString:@"0"]) {
        [self setUpPostEmptyView];
      //  _mainTblView.hidden= YES;
    }
    else{
        _mainTblView.hidden= NO;

        _emptyPostView.hidden = YES;

    [_filteredArray removeAllObjects];
    for(int i=0; i<_postDataSource.count; i++) {
        
        PostModel *pModel = [_postDataSource objectAtIndex:i];
        
        
        if(pModel.postTyper == 2) {
            [_filteredArray addObject:pModel];
            
        }
    }
    [_mainTblView reloadData];
    }
}

- (IBAction)tab3Pressed:(id)sender {
    [self setupTitleColor:@"3"];
    if ([_photoCount.text isEqualToString:@"0"]) {
        [self setUpPhotoEmptyView];
        //_mainTblView.hidden= YES;

    }
    else{
        _mainTblView.hidden= NO;

        _emptyPostView.hidden = YES;

    [_filteredArray removeAllObjects];
    for(int i=0; i<_postDataSource.count; i++) {
        
        PostModel *pModel = [_postDataSource objectAtIndex:i];
        
        
        if(pModel.postTyper == 3) {
            [_filteredArray addObject:pModel];
            
        }
    }
    [_mainTblView reloadData];
    }
}




#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"AJO!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}

#pragma mark - Table View Methods
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    PlacesDetailHeader   *headerView;
    headerView = [[[NSBundle mainBundle] loadNibNamed:@"PlacesDetailHeader" owner:self options:nil] objectAtIndex:0];

    headerView.lblPlaceName.text = _place.placeName;
    headerView.lblPlaceDetail.text = completePlaceDeatil;
    Pictures *pic;
    if (_place.place_picture.count>0) {
        pic= _place.place_picture[0];

    }
    [headerView.placeImg setImageWithStringURL:pic.picture];
     ////// map show //////
     CGFloat  longi = (CGFloat)[_place.longitude floatValue];
     CGFloat   lati = (CGFloat)[_place.latitude floatValue];
     
     MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
     [annotation setCoordinate:CLLocationCoordinate2DMake(lati, longi)];
     [annotation setTitle:_place.placeName]; //You can set the subtitle too
     [headerView.mapView addAnnotation:annotation];
     [headerView.mapView showAnnotations:headerView.mapView.annotations animated:YES];
    [headerView.mapView selectAnnotation:annotation animated:NO];

   ///////////////////////////
    if (_place.place_picture.count>0) {

    int scrollWidth = 0;

    int xOffset = 0;
    
    for(int index=0; index < [_place.place_picture count]; index++)
    {
        Pictures *pic = [_place.place_picture objectAtIndex:index];
        AsyncImageView *img = [[AsyncImageView alloc] initWithFrame:CGRectMake(xOffset,0,110, 100)];
        img.layer.cornerRadius=5.0f;
        img.layer.masksToBounds=YES;
        
        img.imageURL = [NSURL URLWithString: pic.picture];
        NSURL *url = [NSURL URLWithString: pic.picture];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
        // [img setImageWithStringURL: pic.picture];
        
        [headerView.imageScroller addSubview:img];
        
        
        UIButton *addProject = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        addProject.frame = CGRectMake(xOffset,0,110, 100);
        [addProject setTitle:@"" forState:UIControlStateNormal];
        [addProject addTarget:self action:@selector(showPlacesPhoto:) forControlEvents:UIControlEventTouchUpInside];
        addProject.tag = index;
       
        
        
        
        [headerView.imageScroller addSubview:addProject];
        
        
        
        xOffset+=120;
        
        
    }
    
    
    headerView.imageScroller.contentSize = CGSizeMake(scrollWidth+xOffset,100);
    }

    
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ( ![completePlaceDeatil isEqualToString:@""] && _place.place_picture.count>0) {
        
    return 516;
    }
    else if ( ![completePlaceDeatil isEqualToString:@""]&& _place.place_picture.count<1  ){
        return 410;
    }
    else if ( [completePlaceDeatil isEqualToString:@""]&& _place.place_picture.count<1  ){
        return 269;
    }
    return 269;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_filteredArray.count>0) {
        _btnAddNewPost.hidden = NO;
    return _filteredArray.count;
    }
    else{
        _btnAddNewPost.hidden = YES;

        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_filteredArray.count>0) {
        
    PostModel *pModel = [_filteredArray objectAtIndex:indexPath.row];
    if(pModel.postTyper == 1 ) {
        HomeDetailReviewCell * cell = (HomeDetailReviewCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"HomeDetailReviewCell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HomeDetailReviewCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        //[cell.profileImage roundImageCorner];
        [cell.profileImage setImageWithStringURL:pModel.reviewUser_profile_pic];
        cell.staticRatingView.canEdit = NO;
        //cell.staticRatingView.maxRating = pModel.rating;
        cell.staticRatingView.rating = pModel.rating;
        cell.desc.text = pModel.reviewtext;
        cell.userName.text = pModel.reviewUser_name;
        cell.btnDeleteReview.tag = indexPath.row;
        [cell.btnDeleteReview addTarget:self action:@selector(showDeleteView:) forControlEvents:UIControlEventTouchUpInside];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    else if(pModel.postTyper == 2 ) {
        HomeDetailPostCell * cell = (HomeDetailPostCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"HomeDetailPostCell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HomeDetailPostCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        [cell.profileImg roundImageCorner];
        [cell.profileImg setImageWithStringURL:pModel.postUser_profile_pic];
        cell.userName.text = pModel.postUser_name;
        cell.dataTime.text = pModel.postdate_time;
        cell.desc.text = pModel.post_text;
        cell.btnDelPost.tag = indexPath.row;
        [cell.btnDelPost addTarget:self action:@selector(showDeleteView:) forControlEvents:UIControlEventTouchUpInside];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    else {
        HomeDetailPhotoCell * cell = (HomeDetailPhotoCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"HomeDetailPhotoCell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"HomeDetailPhotoCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        
        [cell.profileImg roundImageCorner];
        [cell.profileImg setImageWithStringURL:pModel.pictureUser_profile_pic];
        cell.userName.text = pModel.pictureUser_name;
        cell.dataTime.text = pModel.date_time;
        cell.desc.text = pModel.mainPicture_text;
        cell.btnDeletePost.tag = indexPath.row;
        [cell.btnDeletePost addTarget:self action:@selector(showDeleteView:) forControlEvents:UIControlEventTouchUpInside];

        cell.mainScroller.contentSize = CGSizeMake(600, 30);
        cell.mainScroller.tag = indexPath.row;
        
        int scrollWidth = 100;
        
        
        
        
        int xOffset = 0;
        
        for(int index=0; index < [pModel.picturesArray count]; index++)
        {
            Pictures *pic = [pModel.picturesArray objectAtIndex:index];
            AsyncImageView *img = [[AsyncImageView alloc] initWithFrame:CGRectMake(xOffset,10,130, 130)];
            
            
                img.imageURL = [NSURL URLWithString: pic.picture];
                NSURL *url = [NSURL URLWithString: pic.picture];
                [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
           // [img setImageWithStringURL: pic.picture];
            
            [cell.mainScroller addSubview:img];
            
            
            UIButton *addProject = [UIButton buttonWithType: UIButtonTypeRoundedRect];
            addProject.frame = CGRectMake(xOffset,10,130, 130);
            [addProject setTitle:@"" forState:UIControlStateNormal];
            [addProject addTarget:self action:@selector(showPhotosPressed:) forControlEvents:UIControlEventTouchUpInside];
            addProject.tag = indexPath.row;
            NSLog(@" indexpath......%ld",(long)indexPath.row);
            UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                                  initWithTarget:self action:@selector(deleteLongPhotoPressed:)];
            lpgr.minimumPressDuration = 1.0;
            [addProject addGestureRecognizer:lpgr];
            [lpgr.view setTag:index];
            NSLog(@" indexpath......%ld",(long)indexPath.row);

            

            [cell.mainScroller addSubview:addProject];
            
            
            
            xOffset+=140;
            
            
        }
        
        
        cell.mainScroller.contentSize = CGSizeMake(scrollWidth+xOffset,130);
        

        
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    }
    else{
        EmptyPostCell * cell = (EmptyPostCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"EmptyPostCell"];
        
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"EmptyPostCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        [cell.btnAddPhoto addTarget:self action:@selector(showAddPhotoView:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnAddReview addTarget:self action:@selector(showAddReviewView:) forControlEvents:UIControlEventTouchUpInside];

        return cell;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_filteredArray.count>0) {
 
    PostModel *pModel = [_filteredArray objectAtIndex:indexPath.row];
    
   
    if(pModel.postTyper == 3 ) {
        
        CGSize size = [pModel.mainPicture_text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        
        
        return 280+size.height;
    }
    if(pModel.postTyper == 1 ) {
        
        CGSize size = [pModel.reviewtext sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
        if (size.height>50) {
            return 100+size.height;

        }
        return 120;

    }
    else {
        
        CGSize size = [pModel.post_text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];

        return 100+size.height;
    }
    }
    else{
        return 372;

    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
#pragma mark - Get place Detail API
-(void) mapPlacesDetailData:(id)data{
    Places *placeDetail = [[Places alloc]initWithDictionary:data[@"places"]];
    _place.placeAddress = placeDetail.placeAddress;
    _place.placePhone = placeDetail.placePhone;
    _place.placeEmail = placeDetail.placeEmail;
    _place.place_picture = placeDetail.place_picture;
    [self setupView];

        NSMutableArray *placeDetailArray = [[NSMutableArray alloc]init];
        NSArray *reviewArray=data[@"places"][@"reviews"];
        for (NSDictionary *dic in reviewArray) {
            [placeDetailArray addObject:[[PostModel alloc]initWithDictionaryReviews:dic]];
        }
        NSArray *postArray=data[@"places"][@"posts"];
        for (NSDictionary *dic in postArray) {
            [placeDetailArray addObject:[[PostModel alloc]initWithDictionaryPost:dic]];
        }
        NSArray *picsArray=data[@"places"][@"post_pictures"];
        for (NSDictionary *dic in picsArray) {
            [placeDetailArray addObject:[[PostModel alloc]initWithDictionaryPics:dic]];
        }

        _filteredArray = placeDetailArray;
        _postDataSource = _filteredArray.mutableCopy;
        [_mainTblView reloadData];
        if (_isFromNewPost) {
                    [self moveTabtoNewPost];
        }
    
    
}
-(void) getPlaceDetail{

    [PlaceService getPlacesDetailWithPlaceID:_place.place_id success:^(id data) {
        [SVProgressHUD dismiss];
        
        BOOL status=data[@"status"];
        if (status) {
            [self mapPlacesDetailData:data];
        }
        
    } failure:^(NSError *error) {
        
    }];

}
-(void) getRecommendationDetail{
    
    [PlaceService  getRecommendationDetailWithPlaceID:_place.place_id success:^(id data) {
        BOOL status=data[@"status"];
        if (status) {
            [self mapPlacesDetailData:data];
        }

//        _filteredArray = data;
//        _postDataSource = _filteredArray.mutableCopy;
//        [_mainTblView reloadData];
//        if (_isFromNewPost) {
//            [self moveTabtoNewPost];
//        }
        
    } failure:^(NSError *error) {
        
    }];
    
}

-(void) deleteReview:(PostModel *)post{
    [PostService deletePostWithReviewId:post.review_id success:^(id data) {
        [self hideDeleteView];
        int  count = [_place.reviewCount intValue];
        count--;
        [_place setReviewCount:[NSString stringWithFormat:@"%d",count]];
        [self setupView];

        [_filteredArray removeObjectAtIndex:currentSelectedIndex];
        for (PostModel *p in _postDataSource) {
            if ([p.review_id isEqualToString:post.review_id]) {
                [_postDataSource removeObject:p];
                break;
            }
        }
        [_mainTblView reloadData];
       // UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:data[@"message"] delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
      //  [alertView show];
        
    } failure:^(NSError *error) {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        [alertView show];
        
    } ];

}
-(void) deletePost:(PostModel *)post{
    [PostService deletePostWithPostId:post.post_id success:^(id data) {
        int  count = [_place.postCount intValue];
        count--;
        [_place setPostCount:[NSString stringWithFormat:@"%d",count]];
        [self setupView];

        [self hideDeleteView];
        [_filteredArray removeObjectAtIndex:currentSelectedIndex];
        for (PostModel *p in _postDataSource) {
            if ([p.post_id isEqualToString:post.post_id]) {
                [_postDataSource removeObject:p];
                break;
            }
        }
        [_mainTblView reloadData];
       // UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:data[@"message"] delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
      //  [alertView show];
        
    } failure:^(NSError *error) {
        
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        [alertView show];
        
    }];
    
    
}
-(void) deletePhotos:(PostModel *)post{
    NSMutableArray *picsIdArray = [[NSMutableArray alloc]init];
    for(int index=0; index < [post.picturesArray count]; index++)
    {
        Pictures *pic = [post.picturesArray objectAtIndex:index];
        
        [picsIdArray addObject:pic.picture_id];
        
        
    }
    
    
    [PostService deletePostedPicturesWithPitureId:post.post_picture_id andPictures:picsIdArray success:^(id data) {
        
        int  count = [_place.post_pictures_count intValue];
        count--;
        [_place setPost_pictures_count:[NSString stringWithFormat:@"%d",count]];
        [self setupView];

        [self hideDeleteView];
        for (PostModel *p in _postDataSource) {
            if ([p.post_picture_id isEqualToString:post.post_picture_id]) {
                [_postDataSource removeObject:p];
                break;
            }
        }
        [_filteredArray removeObjectAtIndex:currentSelectedIndex];
        [_mainTblView reloadData];
       // UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:data[@"message"] delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
       // [alertView show];

        
    } failure:^(NSError *error) {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        [alertView show];
        
    }];
}



#pragma mark - IBButton Action
- (IBAction)btnOpenWebUrl:(id)sender {
    NSString *weburl;
    
    if (![_place.placeWebsite containsString:@"http://"]) {
        
        weburl=[NSString stringWithFormat:@"http://%@",_place.placeWebsite];
        }
        else{
            weburl=[NSString stringWithFormat:@"%@",_place.placeWebsite];

        }

      NSURL*    url  = [[NSURL alloc] initWithString:weburl];

        [[UIApplication sharedApplication] openURL:url];

    
    
}

- (IBAction)btnEmptyPost:(id)sender {
    
    NSString *btnTitle = [_btnEmptyPost titleForState:UIControlStateNormal];
    if ([btnTitle isEqualToString:@"Add Review"]) {
            addNewPost   = [AddNewPost loadWithNibWithDelegate:self From:@"HomeDetailVC" andPlace:_place andPostString:@"Review"];
            [addNewPost setDelegate:self];
            [self.view addSubview:addNewPost];
        
        [addNewPost show];

    }
    else     if ([btnTitle isEqualToString:@"Add Post"]) {
            addNewPost   = [AddNewPost loadWithNibWithDelegate:self From:@"HomeDetailVC" andPlace:_place andPostString:@"Post"];
            [addNewPost setDelegate:self];
            [self.view addSubview:addNewPost];
        
        [addNewPost show];

    }
    else     if ([btnTitle isEqualToString:@"Add Photo"]) {
            addNewPost   = [AddNewPost loadWithNibWithDelegate:self From:@"HomeDetailVC" andPlace:_place andPostString:@"Photo"];
            [addNewPost setDelegate:self];
            [self.view addSubview:addNewPost];
        
        [addNewPost show];

    }
    else{
            addNewPost   = [AddNewPost loadWithNibWithDelegate:self From:@"HomeDetailVC" andPlace:_place];
            [addNewPost setDelegate:self];
            [self.view addSubview:addNewPost];
        
        [addNewPost show];
    }
    
    
}

- (IBAction)btnHiddenPhotoView:(id)sender {
    
    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        _showPhotoView.hidden = YES;
        
    } completion:^(BOOL finished) {
        
        
        
    }];
}

- (IBAction)btnDeletePost:(id)sender {
    PostModel *post = [_filteredArray objectAtIndex: currentSelectedIndex];
    if(post.postTyper == 1 ) {
        [self deleteReview:post];
    }
    else if (post.postTyper == 2)
    {
        [self deletePost:post]; //delete post

    }
    else{
        [self deletePhotos:post];   //delete photos

    }


    
}

- (IBAction)btnDeleteSpecificPhotoClicked:(id)sender {
    
    
    [PostService deletePostedPicturesWithPitureId:selectedPhotoPost.post_picture_id andPictures:selectedPhotoArray success:^(id data) {
        [self hideDeleteView];
        PostModel *post = [_filteredArray objectAtIndex: currentSelectedIndex];
        
        [post.picturesArray removeObjectAtIndex:currentPhotoSelectedIndex];
        
        [_mainTblView reloadData];
        // UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:data[@"message"] delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        // [alertView show];
        
        
    } failure:^(NSError *error) {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        [alertView show];
        
    }];

}

- (IBAction)btnHideDeletView:(id)sender {
    
    
    [self hideDeleteView];

}

- (IBAction)btnAddNewPost:(id)sender {
   if (!addNewPost) {
        addNewPost   = [AddNewPost loadWithNibWithDelegate:self From:@"HomeDetailVC" andPlace:_place];
        [addNewPost setDelegate:self];
        [self.view addSubview:addNewPost];
    }
    [addNewPost show];

    
    
}

#pragma mark - NEW POST DELEGATES
-(void)addNewPost:(PostModel *)postObj{
    if(postObj.postTyper == 1 ) {
        BOOL isReviewExist = NO;
        for (int i=0;i< _postDataSource.count;i++) {
            PostModel *p = [_postDataSource objectAtIndex:i];
            if ([p.review_id isEqualToString:postObj.review_id]) {
                [_postDataSource replaceObjectAtIndex:i withObject:postObj];
                
                isReviewExist = YES;
                break;
            }
        }
        for (int i=0;i< _filteredArray.count;i++) {
            PostModel *p = [_filteredArray objectAtIndex:i];
            if ([p.review_id isEqualToString:postObj.review_id]) {
                [_filteredArray replaceObjectAtIndex:i withObject:postObj];
                isReviewExist = YES;

                break;
            }
        }
        if (!isReviewExist) {
            int  count = [_place.reviewCount intValue];
            count++;
            [_place setReviewCount:[NSString stringWithFormat:@"%d",count]];
            [_filteredArray insertObject:postObj atIndex:0];
            [_postDataSource insertObject:postObj atIndex:0];

        }

        [self setupView];
        [_mainTblView reloadData];
        
    }
    else{
   // [_filteredArray addObject:postObj];
    //[_postDataSource addObject:postObj];
        [_filteredArray insertObject:postObj atIndex:0];
        [_postDataSource insertObject:postObj atIndex:0];

    [self setupView];
    [_mainTblView reloadData];

    }
    _mainTblView.hidden= NO;
    _emptyPostView.hidden= YES;
    


}
-(void)moveToNewPostTab:(NSString *)position{
    if ([position isEqualToString:@"1"]) {
        [self setupTitleColor:@"1"];
        [_filteredArray removeAllObjects];
        for(int i=0; i<_postDataSource.count; i++) {
            
            PostModel *pModel = [_postDataSource objectAtIndex:i];
            
            
            if(pModel.postTyper == 1 ) {
                [_filteredArray addObject:pModel];
                
            }
        }
        [_mainTblView reloadData];
    }
    
    else  if ([position isEqualToString:@"2"]) {
        [self setupTitleColor:@"2"];

        [_filteredArray removeAllObjects];
        for(int i=0; i<_postDataSource.count; i++) {
            
            PostModel *pModel = [_postDataSource objectAtIndex:i];
            
            
            if(pModel.postTyper == 2) {
                [_filteredArray addObject:pModel];
                
            }
        }
        [_mainTblView reloadData];
    }
    
    else
    
    {
        [self setupTitleColor:@"3"];

        [_filteredArray removeAllObjects];
        for(int i=0; i<_postDataSource.count; i++) {
            
            PostModel *pModel = [_postDataSource objectAtIndex:i];
            
            
            if(pModel.postTyper == 3) {
                
                [_filteredArray addObject:pModel];
                
            }
        }
        [_mainTblView reloadData];
    }

}

-(void)showDeleteView : (UIButton *) sender{
    
    currentSelectedIndex = sender.tag;
    PostModel *post = [_filteredArray objectAtIndex: currentSelectedIndex];
    User *user = [DataManager sharedManager].currentUser;
        if ([user.userID isEqualToString:post.reviewUser_id]||[user.userID isEqualToString:post.Postuser_id]||[user.userID isEqualToString:post.pictureUser_id]) {
            _btnDeletePost.hidden = NO;
            _btnDeleteSpecificPhoto.hidden = YES;
            _DeleteView.hidden = NO;
            [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
                
                _deletSubView.frame = CGRectMake(0, _DeleteView.frame.size.height-50, _DeleteView.frame.size.width, 50);
                
            } completion:^(BOOL finished) {
                
                
                
            }];

            
        }

}

-(void)showPhotosPressed:(UIButton * )sender{
   UIView *view =[sender superview];
//    
//    for (UIView *v in _photoScrollView.subviews) {
//        if ([v isKindOfClass:[UIImageView class]]) {
//            [v removeFromSuperview];
//        }
//    }
   NSInteger indexpath = view.tag;
   PostModel *post = [_filteredArray objectAtIndex: indexpath];
//    
//    workingFrame.origin.x = 0;
//
//    for(int index=0; index < [post.picturesArray count]; index++)
//    {
//        Pictures *pic = [post.picturesArray objectAtIndex:index];
//        UIImageView *img = [[UIImageView alloc]init];
//        img.frame = workingFrame;
//        //img.contentMode = UIViewContentModeScaleAspectFit;
//
//        [img setImageWithStringURL: pic.picture];
//        
//        [_photoScrollView addSubview:img];
//        workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
//
//        
//
//        
//    }
//    [_photoScrollView setPagingEnabled:YES];
//    [_photoScrollView setContentSize:CGSizeMake(workingFrame.origin.x, workingFrame.size.height)];
//    
//    workingFrame = ScrollViewOriginalFrame;
//    
//    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
//        
//        _showPhotoView.hidden = NO;
//        
//    } completion:^(BOOL finished) {
//        
//   
//        
//    }];
    
    ////////////////////////////////////////
    [slideImageViewController tapCloseButton];
    NSMutableArray *imageLists = [[NSMutableArray alloc]init];
    
    for(int index=0; index < [post.picturesArray count]; index++)
    {
        Pictures *pic = [post.picturesArray objectAtIndex:index];
        UIImageView *img = [[UIImageView alloc]init];
        img.contentMode = UIViewContentModeScaleAspectFit;
        
        [img setImageWithStringURL: pic.picture];
        
        
        [imageLists addObject:img.image];
        
        
    }
    slideImageViewController = [PEARImageSlideViewController new];
    
    
    [slideImageViewController setImageLists:imageLists];
    
    [slideImageViewController showAtIndex:sender.tag];

    

    
}

-(void)showAddPhotoView:(UIButton * )sender{
    addNewPost   = [AddNewPost loadWithNibWithDelegate:self From:@"HomeDetailVC" andPlace:_place andPostString:@"Photo"];
    [addNewPost setDelegate:self];
    [self.view addSubview:addNewPost];
    
    [addNewPost show];
    

}
-(void)showAddReviewView:(UIButton * )sender{
    addNewPost   = [AddNewPost loadWithNibWithDelegate:self From:@"HomeDetailVC" andPlace:_place andPostString:@"Review"];
    [addNewPost setDelegate:self];
    [self.view addSubview:addNewPost];
    
    [addNewPost show];

}
-(void)showPlacesPhoto:(UIButton * )sender{
//    
//    for (UIView *v in _photoScrollView.subviews) {
//        if ([v isKindOfClass:[UIImageView class]]) {
//            [v removeFromSuperview];
//        }
//    }
//    
//    workingFrame.origin.x = 0;
//    
//    for(int index=0; index < [_place.place_picture count]; index++)
//    {
//        Pictures *pic = [_place.place_picture objectAtIndex:index];
//        UIImageView *img = [[UIImageView alloc]init];
//        img.frame = workingFrame;
//        //img.contentMode = UIViewContentModeScaleAspectFit;
//        
//        [img setImageWithStringURL: pic.picture];
//        
//        [_photoScrollView addSubview:img];
//        workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
//        
//        
//        
//        
//    }
//    [_photoScrollView setPagingEnabled:YES];
//    [_photoScrollView setContentSize:CGSizeMake(workingFrame.origin.x, workingFrame.size.height)];
//    
//    workingFrame = ScrollViewOriginalFrame;
//    
//    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
//        
//        _showPhotoView.hidden = NO;
//        
//    } completion:^(BOOL finished) {
//        
//        
//        
//    }];
//
    [slideImageViewController tapCloseButton];
    NSMutableArray *imageLists = [[NSMutableArray alloc]init];
    
    for(int index=0; index < [_place.place_picture count]; index++)
            {
                Pictures *pic = [_place.place_picture objectAtIndex:index];
                UIImageView *img = [[UIImageView alloc]init];
                img.frame = workingFrame;
                img.contentMode = UIViewContentModeScaleAspectFit;
        
                [img setImageWithStringURL: pic.picture];
        
                [_photoScrollView addSubview:img];
                workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
                
                [imageLists addObject:img.image];
                
                
            }
    slideImageViewController = [PEARImageSlideViewController new];

    
    [slideImageViewController setImageLists:imageLists];
    
    [slideImageViewController showAtIndex:sender.tag];

}


-(void)deleteLongPhotoPressed : (UILongPressGestureRecognizer* ) sender{
    selectedPhotoArray = [[NSMutableArray alloc]init];

    NSInteger tag = sender.view.tag;
    UIView *view =[sender.view superview];
    currentSelectedIndex = view.tag;
    currentPhotoSelectedIndex = tag;
    selectedPhotoPost = [_filteredArray objectAtIndex: currentSelectedIndex];
    User *user = [DataManager sharedManager].currentUser;
    if ([user.userID isEqualToString:selectedPhotoPost.pictureUser_id]) {
        
    
   
        Pictures *pic = [selectedPhotoPost.picturesArray objectAtIndex:currentPhotoSelectedIndex];
        
        [selectedPhotoArray addObject:pic.picture_id];
        _btnDeletePost.hidden = YES;
        _btnDeleteSpecificPhoto.hidden = NO;
        _DeleteView.hidden = NO;
        [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
            
            _deletSubView.frame = CGRectMake(0, _DeleteView.frame.size.height-50, _DeleteView.frame.size.width, 50);
            
        } completion:^(BOOL finished) {
            
            
            
        }];
        

    

    }
    NSLog(@"%ld",(long)view.tag);
}
-(void) hideDeleteView{

    [UIView animateKeyframesWithDuration:0.5 delay:0.00 options:0 animations:^{
        
        _deletSubView.frame = CGRectMake(0, _DeleteView.frame.size.height+50, _DeleteView.frame.size.width, 50);
        
    } completion:^(BOOL finished) {
        
        _DeleteView.hidden = YES;
        _btnDeleteSpecificPhoto.hidden = YES;
        _btnDeletePost.hidden = YES;
        
    }];

}
-(void) moveTabtoNewPost{
    
    if ([[DataManager sharedManager].postObj isEqualToString:@"Review"]) {

        [_filteredArray removeAllObjects];
        for(int i=0; i<_postDataSource.count; i++) {
            
            PostModel *pModel = [_postDataSource objectAtIndex:i];
            
            
            if(pModel.postTyper == 1 ) {
                [_filteredArray addObject:pModel];
                
            }
        }
        [_mainTblView reloadData];
    }
    
    else if ([[DataManager sharedManager].postObj isEqualToString:@"Post"]) {

        [_filteredArray removeAllObjects];
        for(int i=0; i<_postDataSource.count; i++) {
            
            PostModel *pModel = [_postDataSource objectAtIndex:i];
            
            
            if(pModel.postTyper == 2) {
                [_filteredArray addObject:pModel];
                
            }
        }
        [_mainTblView reloadData];
    }
    
    else if ([[DataManager sharedManager].postObj isEqualToString:@"Photo"]) {

    [_filteredArray removeAllObjects];
        for(int i=0; i<_postDataSource.count; i++) {
            
            PostModel *pModel = [_postDataSource objectAtIndex:i];
            
            
            if(pModel.postTyper == 3) {
                
                [_filteredArray addObject:pModel];
                
            }
        }
        [_mainTblView reloadData];
    }
    


}
@end
