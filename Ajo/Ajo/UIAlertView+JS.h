//
//  UIAlertView+JS.h
//  iOSApps
//
//  Created by Junaid Muhammad.
//  Copyright (c) 2015 Junaid Muhammad. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UIAlertViewHandler)(UIAlertView *alertView, NSInteger buttonIndex);

@interface UIAlertView (JS)<UIAlertViewDelegate>

+ (void) showAlertMessage:(NSString *) message;

+ (void) showAlertwithTitle:(NSString *) title
                 andMessage:(NSString *) message;

- (void) showWithBlock:(UIAlertViewHandler)handler;

//+ (void)alertPlayAudioFileWithName:(NSString *)name type:(NSString *)type;

@end
