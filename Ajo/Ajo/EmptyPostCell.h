//
//  EmptyPostCell.h
//  Ajo
//
//  Created by Samreen on 31/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyPostCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnAddPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnAddReview;

@end
