//
//  UIImageView+URL.h
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (URL)
- (void) setImageWithStringURL:(NSString *) strURL;

@end
