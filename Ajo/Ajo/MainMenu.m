//
//  MainMenu.m
//  DoubleApiHit
//
//  Created by Pikes iOS on 25/08/2015.
//  Copyright (c) 2015 Pikes iOS. All rights reserved.
//

#import "MainMenu.h"

@implementation MainMenu

- (id)initWithDict:(NSDictionary *) dic
{
    self = [super init];
    if (self) {
        
        self.menuArray =[MainMenu mapDataArray:dic[@"predictions"]];

        
       
        
    }
    return self;
}
- (id)initWithArray:(NSDictionary *) dic
{
    self = [super init];
    if (self) {
       
        self.descipt = dic[@"description"];
        self.placeId=dic[@"place_id"];
    }
    return self;
}

+(NSArray *)mapDataArray:(NSArray *)arr
{
    
    NSMutableArray *mapArray=[NSMutableArray new];
    for (NSDictionary *dic in arr) {
        
        MainMenu * menu = [[MainMenu alloc] initWithArray:dic];
        
        if (menu.placeId)// filtered data to chek place_id,if it exist then data maped on mapArray..
        {
            [mapArray addObject:menu];
        }
        
    }
    
    return mapArray;
    
}




@end
