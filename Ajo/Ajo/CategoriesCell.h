//
//  CategoriesCell.h
//  Ajo
//
//  Created by Samreen on 03/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSubCatName;
@property (weak, nonatomic) IBOutlet UIImageView *subCatImg;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectSubCat;

@end
