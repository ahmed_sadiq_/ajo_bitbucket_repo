//
//  PlacesSubMenu.h
//  Ajo
//
//  Created by Samreen Noor on 14/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlacesSubMenu : NSObject
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *subTitle;

@property(nonatomic,strong) NSString *lat;
@property(nonatomic,strong) NSString *lng;
-(id)initWithDict:(NSDictionary *) dic;
@end
