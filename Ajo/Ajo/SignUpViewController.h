//
//  SignUpViewController.h
//  AJO
//
//  Created by Samreen Noor on 26/08/2016.
//  Copyright © 2016 Apple Txlabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfHeightConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfBottomConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tfBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxViewBottomConstrain;

@end
