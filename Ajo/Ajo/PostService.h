//
//  PostService.h
//  Ajo
//
//  Created by Samreen Noor on 04/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseService.h"
 
@interface PostService : BaseService
+(void) addNewPostWithPlaceId:(NSString *)placeId
                   PostText:(NSString *)text
              DateTime:(NSString *)dateTime
                    success:(serviceSuccess)success
                      failure:(serviceFailure)failure;


+(void) addNewPostWithPlaceId:(NSString *)placeId
                       Rating:(NSString *)rating
                     PostText:(NSString *)text
                     DateTime:(NSString *)dateTime
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure;

+(void) addNewPhotosWithPlaceId:(NSString *)placeId
                       Pictures:(NSArray *)picsArray
                      PhotoText:(NSString *)text
                       DateTime:(NSString *)dateTime
                        success:(serviceSuccess)success
                        failure:(serviceFailure)failure;

+(void) deletePostWithPostId:(NSString *)postId
                      success:(serviceSuccess)success
                      failure:(serviceFailure)failure;

+(void) deletePostWithReviewId:(NSString *)reviewId
                       success:(serviceSuccess)success
                       failure:(serviceFailure)failure;

+(void) deletePostedPicturesWithPitureId:(NSString *)picId
                             andPictures:(NSArray *) pics
                                 success:(serviceSuccess)success
                                 failure:(serviceFailure)failure;
@end
