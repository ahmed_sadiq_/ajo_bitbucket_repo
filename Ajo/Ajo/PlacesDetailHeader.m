//
//  PlacesDetailHeader.m
//  Ajo
//
//  Created by Samreen on 31/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "PlacesDetailHeader.h"
#import "Places.h"

@implementation PlacesDetailHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.placeImg.layer.cornerRadius=5.0f;
    self.placeImg.layer.masksToBounds=YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
