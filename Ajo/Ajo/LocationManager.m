//
//  LocationManager.m
//  Ajo
//
//  Created by Samreen Noor on 14/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//


#import "LocationManager.h"

@interface LocationManager ()<CLLocationManagerDelegate>
{
    
}
@end

@implementation LocationManager

#pragma mark - Self Method

static LocationManager *sharedInstance;

+ (LocationManager *) sharedManager
{
    
    
    
    if(sharedInstance == nil){
        sharedInstance  = [[LocationManager alloc] init];
        sharedInstance.currentLat         = 31.499806 ;
        sharedInstance.currentLong         = 74.317381;

    }
    
    return sharedInstance;
}


- (void) getUserCurrentLocationWithCompletionBlock:(handlerSuccess)completed
                                        errorBlock:(handlerError)errored{
    
    _success = completed;
    _failed  = errored;
    
    if ([self respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self requestWhenInUseAuthorization];
    
        
    }
    
    [self setDelegate:self];
    [self startUpdatingLocation];
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    NSLog(@"%@",locations);
    
    if (locations.count) {
        
        CLLocation * location = locations[0];
        [self stopUpdatingLocation];
        [self setDelegate:nil];
        _success(location.coordinate);
    }
    
    
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    NSLog(@"%@",error.localizedDescription);
    
    [self stopUpdatingLocation];
    
    _failed(error);
}



@end
