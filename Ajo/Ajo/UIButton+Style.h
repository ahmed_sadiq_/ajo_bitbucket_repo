//
//  UIButton+Style.h
//  Ajo
//
//  Created by Samreen on 07/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Style)
- (void) roundCorner:(CGFloat) size;

@end
