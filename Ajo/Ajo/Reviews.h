//
//  Reviews.h
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseEntity.h"

@interface Reviews : BaseEntity
@property (nonatomic, strong) NSString * review_id;
@property (nonatomic, strong) NSString * rating;
@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * place_id;
@property (nonatomic, strong) NSString * user_profile_pic;


- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapReviewsFromArray:(NSArray *) arrlist;

@end
