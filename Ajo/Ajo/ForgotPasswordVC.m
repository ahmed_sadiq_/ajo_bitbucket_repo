//
//  ForgotPasswordVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 07/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "SignUpVC.h"
#import "ViewController.h"
#import "AccountService.h"
#import "UITextField+Style.h"

@interface ForgotPasswordVC ()
{
    UITextField *currentField;

}
@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [singleTap setNumberOfTapsRequired:1];
    // singleTap.delegate=delegate;
   // [self.view addGestureRecognizer:singleTap];
    
    [_emailTxt setLeftSpaceSize:10.0];
    
    [[_btnReset layer] setCornerRadius:3.0f];
    
    [[_btnReset layer] setMasksToBounds:YES];
    
    [[_btnReset layer] setBorderWidth:1.0f];
    _btnReset.layer.borderColor = [UIColor whiteColor].CGColor;

}
-(void)viewWillDisappear:(BOOL)animated{
    [_emailTxt resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)validateEmail:(NSString *)candidate {
    
    
    /////////////////// Email Validations
    // NSString *email = [txtFldUsername.text lowercaseString];
    NSString *emailRegEx =@"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF  MATCHES %@", emailRegEx];
    BOOL *signupValidations = TRUE;
    
    /*  NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,4}";
     NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; */
    
    
    return [emailTest evaluateWithObject:candidate];
}


#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"AJO!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)resetPswdPressed:(id)sender {
    
    
        if(![self validateEmail:_emailTxt.text])
        {
            
            [self showAlertMessage:@"Please enter some valid email address." ];
            
        }else{
    
    [AccountService forgetPasswordWhereEmail:_emailTxt.text succuss:^(id data) {
        [self showAlertMessage:data[@"message"]];

    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];

    }];
        }
}

- (IBAction)createAccountPressed:(id)sender {
    SignUpVC *signupController = [[SignUpVC alloc] initWithNibName:@"SignUpVC" bundle:nil];
    [self.navigationController pushViewController:signupController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

- (IBAction)letsDiscoverPressed:(id)sender {
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[ViewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
        }
    }
}


-(void)reSetCall{


}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentField=textField;
    [self animateTextField:nil up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:nil up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(void)dismissKeyboard {
    
    
    NSLog(@"screen touch");
    
    [currentField resignFirstResponder];
    self.view.frame = CGRectMake(0, 0,self.view.frame.size.width,self.view.frame.size.height);

}
@end
