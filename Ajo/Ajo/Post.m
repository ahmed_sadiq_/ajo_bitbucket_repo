//
//  Post.m
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "Post.h"

@implementation Post
- (id)initWithDictionary:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
        self.post_id = [self validStringForObject:responseData[@"post_id"]];
        self.post_text =[self validStringForObject:responseData[@"post_text"]];
        self.user_id =[self validStringForObject:responseData[@"user_id"]];
        self.place_id =[self validStringForObject:responseData[@"place_id"]];
        self.user_profile_pic =[self validStringForObject:responseData[@"user_profile_pic"]];
        
        
        
    }
    
    return self;
}




+ (NSArray *)mapPostFromArray:(NSArray *)arrlist {
    
    
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    
    
    for (NSDictionary *dic in arrlist) {
        [mappedArr addObject:[[Post alloc]initWithDictionary:dic]];
    }
    
    return mappedArr;
    
}

@end
