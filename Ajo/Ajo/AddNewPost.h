//
//  AddNewPost.h
//  Ajo
//
//  Created by Samreen Noor on 03/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "PostModel.h"
#import "HomeDetailVC.h"
#import "Places.h"
#import "ELCAlbumPickerController.h"
#import "ELCImagePickerController.h"
#import "ELCAssetTablePicker.h"
@protocol AddNewPostDelegate
@optional
- (void) addNewPost:(PostModel *)postObj;
- (void) moveToNewPostTab:(NSString *)position;


@end
@interface AddNewPost : UIView <ASStarRatingViewDelegate,UITextFieldDelegate, ELCImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate>
{
    NSArray *photoArray;
}
@property (assign)id<AddNewPostDelegate> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submitPhotoBottomConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *SubmitBottomConstrain;
@property (weak, nonatomic) IBOutlet UITextField *tfDescription;

@property (weak, nonatomic) IBOutlet UIView *reviewView;
@property (weak, nonatomic) IBOutlet UIView *photoView;
@property (weak, nonatomic) IBOutlet UIView *addNewPostView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postViewBottomConstrain;
@property (weak, nonatomic) IBOutlet UIImageView *placeImg;
@property (weak, nonatomic) IBOutlet ASStarRatingView *ratingView;
@property (strong, nonatomic) Places *postPlace;

@property (weak, nonatomic) IBOutlet UIScrollView *scrrollView;

@property (weak, nonatomic) IBOutlet UITextField *tfPhotoDescription;

+ (id) loadWithNibWithDelegate:(id )delegate From:(NSString *)controllerStr andPlace:(Places *)place;
+ (id) loadWithNibWithDelegate:(id )delegate From:(NSString *)controllerStr andPlace:(Places *)place andPostString:(NSString *)postStr;
-(void) show;

-(void) hide;
@end
