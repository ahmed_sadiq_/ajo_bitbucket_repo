//
//  JSFacebookManager.h
//  Ajo
//
//  Created by Samreen Noor on 08/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import<Foundation/Foundation.h>
#import "JSUser.h"

@interface JSFacebookManager : NSObject

typedef void (^loadSuccess)(id data);
typedef void (^loadFailure)(NSError *error);

+ (JSFacebookManager *)sharedManager;

-(void) getUserInfoWithCompletion:(loadSuccess) completed
                          failure:(loadFailure) failed;

-(void) getListOfUserLikesWithCompletion:(loadSuccess) completed
                                 failure:(loadFailure) failed;

-(void) getFriendsListWithCompletion:(loadSuccess) completed
                             failure:(loadFailure) failed;


@end
