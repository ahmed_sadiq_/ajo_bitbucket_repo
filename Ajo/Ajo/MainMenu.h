//
//  MainMenu.h
//  DoubleApiHit
//
//  Created by Pikes iOS on 25/08/2015.
//  Copyright (c) 2015 Pikes iOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainMenu : NSObject
@property (nonatomic, strong) NSString *descipt;

@property (nonatomic, strong) NSString *placeId;
@property (nonatomic, strong) NSMutableArray *menuArray;
- (id)initWithDict:(NSDictionary *) dic;
- (id)initWithArray:(NSDictionary *) dic;
+(NSArray *)mapDataArray:(NSArray *)arr;


@end
