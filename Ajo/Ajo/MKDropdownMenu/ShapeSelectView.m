//
//  ShapeSelectView.m
//  MKDropdownMenuExample
//
//  Created by Max Konovalov on 18/03/16.
//  Copyright © 2016 Max Konovalov. All rights reserved.
//

#import "ShapeSelectView.h"

@implementation ShapeSelectView

- (void)awakeFromNib {
    [super awakeFromNib];
  
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    self.textLabel.font = [UIFont systemFontOfSize:self.textLabel.font.pointSize
                                            weight:selected ? UIFontWeightMedium : UIFontWeightLight];
}

@end
