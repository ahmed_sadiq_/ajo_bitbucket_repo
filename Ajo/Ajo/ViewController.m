//
//  ViewController.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ViewController.h"
#import "SignUpVC.h"
#import "RecommendationVC.h"
#import "ProfileVC.h"
#import "SettingsVC.h"
#import "ForgotPasswordVC.h"
#import "AccountService.h"
#import "JSAccountsService.h"
#import "JSFacebookManager.h"
#import "JSUser.h"
#import "Constant.h"
#import "LocationManager.h"
#import <Crashlytics/Crashlytics.h>
#import "UITextField+Style.h"
#import "FavouriteVC.h"

@interface ViewController ()
{
    UITextField *currentField;

}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [singleTap setNumberOfTapsRequired:1];
    // singleTap.delegate=delegate;
    [self.view addGestureRecognizer:singleTap];

    [_userNameTxt setLeftSpaceSize:10.0];
    [_pswdTxt setLeftSpaceSize:10.0];
    [[_btnDiscover layer] setCornerRadius:3.0f];
    
    [[_btnDiscover layer] setMasksToBounds:YES];
    
    [[_btnDiscover layer] setBorderWidth:1.0f];
    _btnDiscover.layer.borderColor = [UIColor whiteColor].CGColor;




}
- (IBAction)btnPasswordShow:(id)sender {
    if (self.pswdTxt.secureTextEntry == YES) {
        self.imgPasVisible.image = [UIImage imageNamed:@"invisible"];
        self.pswdTxt.secureTextEntry = NO;
        
    }
    
    else
    {
        self.imgPasVisible.image = [UIImage imageNamed:@"visible"];
        self.pswdTxt.secureTextEntry = YES;
    }

}
- (IBAction)crashButtonTapped:(id)sender {
    [[Crashlytics sharedInstance] crash];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)crateAccountPressed:(id)sender {
    SignUpVC *signupController = [[SignUpVC alloc] initWithNibName:@"SignUpVC" bundle:nil];
    [self.navigationController pushViewController:signupController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

- (IBAction)forgotPasswordPressed:(id)sender {
    ForgotPasswordVC *forgotPswd = [[ForgotPasswordVC alloc] initWithNibName:@"ForgotPasswordVC" bundle:nil];
    [self.navigationController pushViewController:forgotPswd animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

- (IBAction)loginPressed:(id)sender {
    if ([self checkLoginTextFields]) {
        [self loginCall];

    }

}

- (IBAction)btnFbLogin:(id)sender {
    
    [[JSFacebookManager sharedManager] getUserInfoWithCompletion:^(id data) {
        JSUser *user = data;
        [AccountService fbLoginWithFirstName:user.firstName email:user.email andProfielImg:user.imageURL success:^(id data) {
            [self createTabBarAndControl];

        } failure:^(NSError *error) {
            [self showAlertMessage:error.localizedDescription];

        }];
        
        NSLog(@"%@",data);
    } failure:^(NSError *error) {
        NSLog(@"%@",error.localizedDescription);
        if (error.localizedDescription == nil) {//Cancelled
           // [self showAlertMessage:@"Cancelled"];
            
        }
        else
        [self showAlertMessage:error.localizedDescription];

    } ];
}

-(void)createTabBarAndControl {
//    
//    self.viewController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
//    
//    self.navController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
//    [self.navController.tabBarItem setSelectedImage:[[UIImage imageNamed:DISCOVER_SELECTED_iMG]
//                                                     imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
//    
//    [self.navController.tabBarItem setImage:[[UIImage imageNamed:DISCOVER_iMG] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    
//    
//    self.navController.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
//    [self.navController setNavigationBarHidden:YES animated:NO];
//    
//    UIViewController *recommendationVC;
//    recommendationVC = [[RecommendationVC alloc] initWithNibName:@"RecommendationVC" bundle:[NSBundle mainBundle]];
//    
//    
//    UINavigationController *recommendationNavController = [[UINavigationController alloc] initWithRootViewController:recommendationVC];
//    
//    
//    [recommendationVC.tabBarItem setSelectedImage:[[UIImage imageNamed:RECOMMENDATION_SELECTED_iMG]
//                                            imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
//    [recommendationVC.tabBarItem setImage:[[UIImage imageNamed:RECOMMENDATION_iMG] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    
//    
//    recommendationVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
//    
//    [recommendationVC.navigationController setNavigationBarHidden:YES animated:NO];
//    
//    UIViewController *profileVC;
//    profileVC = [[ProfileVC alloc] initWithNibName:@"ProfileVC" bundle:[NSBundle mainBundle]];
//    UINavigationController *profileNavController = [[UINavigationController alloc] initWithRootViewController:profileVC];
//    
//    [profileVC.tabBarItem setSelectedImage:[[UIImage imageNamed:PROFILE_SELECTED_iMG]
//                                          imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
//    [profileVC.tabBarItem setImage:[[UIImage imageNamed:PROFILE_iMG] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    
//    profileVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
//    
//    [profileVC.navigationController setNavigationBarHidden:YES animated:NO];
//    
//    UIViewController *settingVC;
//    settingVC = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:[NSBundle mainBundle]];
//    UINavigationController *settingNavController = [[UINavigationController alloc] initWithRootViewController:settingVC];
//    
//    [settingVC.tabBarItem setSelectedImage:[[UIImage imageNamed:SETTINGS_SELECTED_iMG]
//                                            imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal]];
//    [settingVC.tabBarItem setImage:[[UIImage imageNamed:SETTINGS_iMG] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//    
//    settingVC.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
//    
//    [settingVC.navigationController setNavigationBarHidden:YES animated:NO];
//    
//    self.tabBarController = [[UITabBarController alloc] init] ;
//    self.tabBarController.viewControllers = [NSArray arrayWithObjects:self.navController, recommendationNavController,profileNavController,settingNavController,nil];
//    
//    [self.tabBarController.tabBar setBackgroundColor:[UIColor whiteColor]];
//    
//    self.viewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
//    //self.viewController.navigationController.navigationBar
//    [[[UIApplication sharedApplication]delegate] window].rootViewController = self.tabBarController;

    
    
        self.viewController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
        UINavigationController *homeNAv = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
  
    
    UIImage *img = [UIImage imageNamed:@"ajonear"];
    
    [self.viewController.tabBarItem initWithTitle:@"NEARBY" image:[img imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    self.viewController.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [self.viewController.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *recommendationVC;
    recommendationVC = [[RecommendationVC alloc] initWithNibName:@"RecommendationVC" bundle:[NSBundle mainBundle]];
    
    
    UINavigationController *recommendationNavController = [[UINavigationController alloc] initWithRootViewController:recommendationVC];
    
    
    
    UIImage *search = [UIImage imageNamed:@"ajosearch"];
    
    [recommendationNavController.tabBarItem initWithTitle:@"SEARCH" image:[search imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    recommendationNavController.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [recommendationVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *favouriteVC;
    favouriteVC = [[FavouriteVC alloc] initWithNibName:@"FavouriteVC" bundle:[NSBundle mainBundle]];
    
    
    UINavigationController *favouriteVCNavController = [[UINavigationController alloc] initWithRootViewController:favouriteVC];
    
    UIImage *favIcon = [UIImage imageNamed:@"ajoFavorite"];
    
    [favouriteVC.tabBarItem initWithTitle:@"FAVOURITE" image:[favIcon imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    favouriteVC.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [favouriteVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *profileVC;
    profileVC = [[ProfileVC alloc] initWithNibName:@"ProfileVC" bundle:[NSBundle mainBundle]];
    UINavigationController *profileNavController = [[UINavigationController alloc] initWithRootViewController:profileVC];
    
    
    UIImage *profilicon = [UIImage imageNamed:@"ajoprofile"];
    
    [profileVC.tabBarItem initWithTitle:@"PROFILE" image:[profilicon imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    profileVC.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [profileVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *settingVC;
    settingVC = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:[NSBundle mainBundle]];
    UINavigationController *settingNavController = [[UINavigationController alloc] initWithRootViewController:settingVC];
    
    
    UIImage *settingIcon = [UIImage imageNamed:@"ajosettings"];
    
    [settingVC.tabBarItem initWithTitle:@"SETTINGS" image:[settingIcon imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    settingVC.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [settingVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.tabBarController = [[UITabBarController alloc] init] ;
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:homeNAv, recommendationNavController,favouriteVCNavController,profileNavController,settingNavController,nil];
    
    [self.tabBarController.tabBar setBackgroundColor:[UIColor whiteColor]];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:59.0/255.0 green:197.0/255.0 blue:186.0/255.0 alpha:1];
    self.viewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //self.viewController.navigationController.navigationBar
    [[[UIApplication sharedApplication]delegate] window].rootViewController = self.tabBarController;
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentField = textField;
    [self animateTextField:nil up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:nil up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"AJO!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}

-(BOOL)checkLoginTextFields{
    
    BOOL willEnabled = YES;
    
    if ([_userNameTxt.text length]==0 ||[_pswdTxt.text  length]==0) {
        [self showAlertMessage:@"Please enter valid informtation"];
        return NO;
    }
    if (![self emailValidate:_userNameTxt.text]) {
        [self showAlertMessage:@"Please enter valid  email"];
        return NO;
    }
    
    
    
    return willEnabled;
}


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}
-(void) loginCall{
    [AccountService loginUserWithEmail:_userNameTxt.text password:_pswdTxt.text success:^(id data) {
        
        [self createTabBarAndControl];

        
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];

    }];
    
    
    
    


}

-(void)dismissKeyboard {
    
    NSLog(@"screen touch");
    
    [currentField resignFirstResponder];
    
}
@end
