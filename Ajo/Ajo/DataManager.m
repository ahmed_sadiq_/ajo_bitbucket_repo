//
//  DataManager.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager
@synthesize categoryArray;
static DataManager *sharedManager;

+ (DataManager *) sharedManager
{
    if(sharedManager == nil)
    {
        
        sharedManager = [[DataManager alloc] init];
        sharedManager.authToken         = @"";
        sharedManager.favouritePlaces = [[NSMutableArray alloc]init];
        sharedManager.nearBYPlaces = [[NSMutableArray alloc]init];
    }
    
    return sharedManager;
}


@end
