//
//  PlaceService.h
//  Ajo
//
//  Created by Samreen Noor on 10/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseService.h"

@interface PlaceService : BaseService


+(void) getDiscoverPlacesWithLocation:(NSString *)location
                               Radius:(NSString *)radius
                            Pagetoken:(NSString *)pagetoken
                              success:(serviceSuccess)success
                              failure:(serviceFailure)failure;

+(void) getRecommendationPlacesWithLocation:(NSString *)location
                                  pageToken:(NSString *)token
                                     Radius:(NSString *)radius
                                    success:(serviceSuccess)success
                                    failure:(serviceFailure)failure;


+(void) getPlacesDetailWithPlaceID:(NSString *)placeId
                           success:(serviceSuccess)success
                           failure:(serviceFailure)failure;

+(void) getRecommendationDetailWithPlaceID:(NSString *)placeId
                                   success:(serviceSuccess)success
                                   failure:(serviceFailure)failure;
+(void) filterRecommendationPlacesWithCountry:(NSString *)country
                                         city:(NSString *)city
                                      andType:(NSString *)type
                                    pageToken:(NSString *)token
                                      success:(serviceSuccess)success
                                      failure:(serviceFailure)failure;
+(void) searchPlacesWithUrl:(NSString *)uri
                   Location:(NSString *)location
              SearchKeyword:(NSString *)keyword
                     Radius:(NSString *)radius
                  PageToken:(NSString *)pagetoken
                    success:(serviceSuccess)success
                    failure:(serviceFailure)failure  ;

+(void) searchPlacesFromServerWithUrl:(NSString *)uri
                        SearchKeyword:(NSString *)keyword
                            PageToken:(NSString *)pagetoken
                              success:(serviceSuccess)success
                              failure:(serviceFailure)failure;
+(void) addNewPlaceWithPlaceName:(NSString *)placename
                     Description:(NSString *)des
                        latitude:(NSString *)lat
                       Longitude:(NSString *)longi
                            Type:(NSString *)type
                      SubCatgory:(NSString *)subCat
                         Website:(NSString *)web
                         Country:(NSString *)country
                            City:(NSString *)city
                      PhotoArray:(NSArray *)photosArray
                           Email:(NSString *)email
                         Address:(NSString *)address
                           Phone:(NSString *)phone
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure;

+(void) getFavPlacesWithPagetoken:(NSString *)pagetoken
                          success:(serviceSuccess)success
                          failure:(serviceFailure)failure;

+(void) addFavPlacesWithPlaceID:(NSString *)placeId
                        success:(serviceSuccess)success
                        failure:(serviceFailure)failure;

+(void) searchPlacesWithUrl:(NSString *)uri
                  PageToken:(NSString *)page
                   Location:(NSString *)location
              SearchKeyword:(NSString *)keyword
                   Category:(NSString *)category
                SubCategory:(NSString *)subCategory
                    success:(serviceSuccess)success
                    failure:(serviceFailure)failure ;
@end
