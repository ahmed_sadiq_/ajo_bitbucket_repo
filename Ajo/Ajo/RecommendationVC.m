//
//  RecommendationVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "RecommendationVC.h"
#import "HomeCell.h"
#import "HomeDetailVC.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+URL.h"
#import "PlaceService.h"
#import "Places.h"
#import "AddNewPost.h"
#import "ShapeSelectView.h"
#import "AddNewPlaceViewController.h"
#import "SVProgressHUD.h"
#import "UIAlertView+JS.h"
#import "CategoriesCell.h"
#import "CategoriesHeader.h"
#import "Category.h"
#import "DrawerVC.h"
#import "SearchResultVC.h"
#import "MainMenu.h"
#import "PlacesSubMenu.h"
#import "MenuCell.h"
#import "UIButton+Style.h"
#import "UIView+Style.h"


NS_ENUM(NSInteger, DropdownComponents) {
    DropdownComponentCountry = 0,
    DropdownComponentCity,
    DropdownComponentCategory,
    DropdownComponentsCount
};

@interface RecommendationVC ()

{
  
    
    NSMutableArray *placesArray;
    NSUInteger currentIndex;
    NSUInteger currentSelectedIndex;
    UITextField *currentTxtField;

    NSMutableArray *categoryArray;
    NSString *selectedCategory;
    NSString *selectedSubCategory;
    MainMenu *m;
    PlacesSubMenu *subMenu;
    NSArray *catList;
    NSUInteger selectedSection;
    NSString *searchKeyword;
    NSString *latitude;
    NSString *longitude;
    NSString *location;
}
@end

@implementation RecommendationVC



-(void) mapCategoryData:(NSArray *)data{
    catList = [Category mapCategoryFromArray:data];
    NSLog(@"-----%@",catList);
    selectedSection =catList.count + 1;
}

-(void) setUpView{
    self.searchBoxView.layer.cornerRadius = 3;
    self.searchBoxView.layer.masksToBounds = YES;
    self.locationBox.layer.cornerRadius = 3;
    self.locationBox.layer.masksToBounds = YES;
    
    self.btnSearch.layer.cornerRadius = 3;
    self.btnSearch.layer.masksToBounds = YES;
    self.btnSearch.layer.borderWidth = 1.0f;
    
    self.btnSearch.layer.borderColor = [UIColor whiteColor].CGColor;
    self.navView.layer.shadowOffset = CGSizeMake(-2, 1);
    self.navView.layer.shadowRadius = 3;
    self.navView.layer.shadowOpacity = 0.5;
    [self.plusBox roundCorner:30.0];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self setUpView];
    
    placesArray = [[NSMutableArray alloc]init];
    
       catList = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Categories" ofType:@"plist"]];
    [self mapCategoryData:catList];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.mainTblView addGestureRecognizer:tap];
 
   searchKeyword = @"";
   selectedCategory =@"";
    selectedSubCategory = @"";
  latitude = [DataManager sharedManager].currentUser.latitude;
  longitude =  [DataManager sharedManager].currentUser.longitude;
}

-(void)viewWillAppear:(BOOL)animated{
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dismissKeyboard {
    [_tfSearch resignFirstResponder];
    _tblSearch.hidden = YES;
    //[_tfSearch setText:@""];
    //    recommendationArray = searchArray.mutableCopy;
    //    [_mainTblView reloadData];
    //
}


#pragma mark - Table View Methods
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    CategoriesHeader   *headerView;
    headerView = [[[NSBundle mainBundle] loadNibNamed:@"CategoriesHeader" owner:self options:nil] objectAtIndex:0];
    Category *cat = catList[section];
    headerView.lblCatName.text = cat.categoryName;
    headerView.catImg.image = [UIImage imageNamed:cat.categoryImg];
    if (cat.subCategory.count>0) {
        headerView.btndropDown.hidden = NO;
    }
    else{
        headerView.btndropDown.hidden = YES;

    }
    [headerView.btnShowSubCategory addTarget:self action:@selector(showSubCategoryBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    headerView.btnShowSubCategory.tag = section;

    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView.tag==10|| tableView.tag==20) {
        return 0;
    }
    else
    return 60;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==10|| tableView.tag==20) {
        return 1;
    }
    else
    return catList.count;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag==10|| tableView.tag==20) {
      return  m.menuArray.count;
    }
    else{
    if (section == selectedSection) {
        Category *cat = catList[section];
        return cat.subCategory.count;
    }
    else
        return 0;
    }


}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==10|| tableView.tag==20) {
        static NSString *cellIdentifier = @"cell";
        MenuCell * cell = (MenuCell *)[_tblLocation dequeueReusableCellWithIdentifier:@"MenuCell"];
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MenuCell" owner:self options:nil];
            cell=[nib objectAtIndex:0];
        }
        MainMenu *menu=[m.menuArray objectAtIndex:indexPath.row];
        [cell.placeName setText:[NSString stringWithFormat:@"%@",menu.descipt]];
        
        return cell;

    }
    else{
    currentIndex = indexPath.row ;
    
    CategoriesCell * cell= (CategoriesCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"CategoriesCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"CategoriesCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    Category *cat = catList[selectedSection];
    cell.lblSubCatName.text = cat.subCategory[indexPath.row];
        [cell.btnSelectSubCat addTarget:self action:@selector(selectSubCategoryBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnSelectSubCat.tag = indexPath.row;
        return cell;

    }
   
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag==10|| tableView.tag==20) {
        return 39;
    }
    else
    return 60;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==10) {
        
        MainMenu *menu=[m.menuArray objectAtIndex:indexPath.row];
        _tblLocation.hidden = YES;
        _tfLocation.text = menu.descipt;
        [self getLatLongWithPlanceID:menu.placeId];
    }
    else if (tableView.tag==20) {
        
        MainMenu *menu=[m.menuArray objectAtIndex:indexPath.row];
        _tblSearch.hidden = YES;
        _tfSearch.text = menu.descipt;
    }
    
 
    
}


#pragma mark - Cell Utility Methods
- (void) showSubCategoryBtnPressed : (UIButton *) sender {
    _tblSearch.hidden = YES;
    _tblLocation.hidden = YES;
    Category *cat = catList[sender.tag];
    selectedCategory = cat.categoryName;
    _tfSearch.text = selectedCategory;
    _btnCancel.hidden = NO;
    [_tfSearch resignFirstResponder];
    _tfSearch.userInteractionEnabled = NO;
    if (selectedSection == sender.tag) {
        selectedSection = catList.count + 1;
    }
    else
    selectedSection = sender.tag;
    
    [_mainTblView reloadData];
    searchKeyword = @"";


}
- (void) selectSubCategoryBtnPressed : (UIButton *) sender {
    Category *cat = catList[selectedSection];
    if ([selectedCategory isEqualToString:cat.categoryName]) {
        selectedSubCategory = cat.subCategory[sender.tag];
        _tfSearch.text = [NSString stringWithFormat:@"%@,%@",selectedCategory,selectedSubCategory];
    }
    
}
#pragma mark -IBBUTTON ACTION
- (IBAction)btnMenuClicked:(id)sender {
    [[DrawerVC getInstance] AddInView:self];
    [[DrawerVC getInstance] ShowInView];
    
}

- (IBAction)btnCancel:(id)sender {
    _tfSearch.text = @"";
    [_tfSearch becomeFirstResponder];
    _btnCancel.hidden = YES;
    _tfSearch.userInteractionEnabled = YES;
    _tblSearch.hidden = YES;
    _tblLocation.hidden = YES;
}

- (IBAction)btnAddnewPlace:(id)sender {
    
    AddNewPlaceViewController *newPlaceController = [[AddNewPlaceViewController alloc] initWithNibName:@"AddNewPlaceViewController" bundle:nil];
    newPlaceController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:newPlaceController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];

    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - api calling


-(void) searchApi{
    location = [NSString stringWithFormat:@"%@,%@",latitude,longitude];

    [PlaceService searchPlacesWithUrl:URL_PLACE_SEARCH PageToken:@"1" Location:location SearchKeyword:searchKeyword Category:selectedCategory SubCategory:selectedSubCategory success:^(id data) {
        placesArray = data;
        SearchResultVC *searchVC = [[SearchResultVC alloc] initWithNibName:@"SearchResultVC" bundle:nil];
        
        searchVC.placesArray = placesArray;
        searchVC.searchTitle = _tfSearch.text;
        searchVC.location = location;
        searchVC.category = selectedCategory;
        searchVC.subCategory = selectedSubCategory;
        searchVC.keyword = searchKeyword;
        searchVC.hidesBottomBarWhenPushed = YES;

        [self.navigationController pushViewController:searchVC animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
        
    } failure:^(NSError *error) {
        
        [self showAlertMessage:error.localizedDescription];
    }];
}

- (IBAction)btnSearch:(id)sender {
    if (![_tfSearch.text isEqualToString:@""]) {
        if ([selectedCategory isEqualToString:@""]) {
            searchKeyword = _tfSearch.text;
            
        }
        else
            searchKeyword = @"";
        
        _tblSearch.hidden = YES;
        _tblLocation.hidden = YES;
        [self searchApi];
    }
   
    
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTxtField = textField;
    if ([textField isEqual:_tfSearch]) {
        _tblLocation.hidden = YES;
    }
    else if ([textField isEqual:_tfLocation]){
        _tblSearch.hidden = YES;

    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    if (![_tfSearch.text isEqualToString:@""]) {
        if ([selectedCategory isEqualToString:@""]) {
            searchKeyword = _tfSearch.text;
        }
        else
            searchKeyword = @"";
        
        _tblSearch.hidden = YES;
        _tblLocation.hidden = YES;
        [self searchApi];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([_tfLocation isEqual:textField]) {
        if (![searchStr isEqualToString:@""]) {
            [self loadDataFromNetwork:_tfLocation.text];
            
        }
        else{
            
            
            latitude = [DataManager sharedManager].currentUser.latitude;
            longitude =  [DataManager sharedManager].currentUser.longitude;
            _tblLocation.hidden = YES;
            
        }
    }
    else if ([_tfSearch isEqual:textField]){
        if (![searchStr isEqualToString:@""]) {
            _btnCancel.hidden = NO;
            selectedCategory = @"";
            selectedSubCategory = @"";
            [self loadDataFromNetwork:_tfSearch.text];

        }
        else{
            _btnCancel.hidden = YES;
        _tblSearch.hidden = YES;
        }
    }
    
    return YES;
}
////////   ################### /////////////////
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"AJO!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}

#pragma mark - LOcation GET Method

-(void) getLatLongWithPlanceID:(NSString *)placeId
{
    
    NSString *str3=[[NSString alloc] initWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=AIzaSyCAd8TOUYgbF2nBggLgkmJd3NT8hJPrals",placeId  ];//& usedin url string to  concatination
    
    NSURL * url  = [NSURL URLWithString:str3];
    NSURLRequest * resquest = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:resquest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               id respose = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                               
                               NSLog(@"%@",respose);
                               [self mappPlaceDetail:respose];
                               
                               
                               
                           }];
    
    
}


-(void) loadDataFromNetwork:(NSString *)city

{
    NSString *cityString = [city stringByReplacingOccurrencesOfString:@" " withString:@""];
    float lati = [latitude floatValue];
    float longi =[longitude floatValue];
    
    NSString *str3=[[NSString alloc] initWithFormat:@"https://maps.googleapis.com/maps/api/place/queryautocomplete/json?input=%@&location=%f,%f&radius=500&key=AIzaSyCAd8TOUYgbF2nBggLgkmJd3NT8hJPrals",cityString ,lati,longi  ];//& usedin url string to  concatination
    
    NSURL * url  = [NSURL URLWithString:str3];
    
    NSURLRequest *resquest  = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:resquest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (![data isEqual:nil]) {
                                   
                                   id respose = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                   
                                   NSLog(@"%@",respose);
                                   
                                   [self mappData:respose];
                                   
                               }
                               
                               
                           }];
    
}
-(void) mappData:(NSDictionary *) value{
    
    
    
    m =    [[MainMenu alloc]initWithDict:value];
    
    if ([currentTxtField isEqual:_tfLocation]) {
    _tblLocation.hidden = NO;
    [_tblLocation reloadData];
    }
    else if ([currentTxtField isEqual:_tfSearch]){
        _tblSearch.hidden = NO;
        [_tblSearch reloadData];
    }
}



-(void) mappPlaceDetail:(NSDictionary *) value{
    
    
    
    
    subMenu =    [[PlacesSubMenu alloc]initWithDict:value];
    latitude = subMenu.lat;
    longitude = subMenu.lng;
    NSLog(@"%@",value);
    
}

@end
