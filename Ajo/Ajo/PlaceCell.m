//
//  PlaceCell.m
//  Ajo
//
//  Created by Samreen on 29/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "PlaceCell.h"

@implementation PlaceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.boxView.layer.cornerRadius = 3;
   self.boxView.layer.masksToBounds = YES;
    self.backgroundColor =[UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
