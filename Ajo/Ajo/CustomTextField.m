//
//  CustomTextField.m
//  Ajo
//
//  Created by Samreen on 30/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    self.layer.cornerRadius=5.0f;
    self.layer.masksToBounds=YES;
    self.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.layer.borderWidth= 1.0f;
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 40.0)];
    [spaceView setBackgroundColor:[UIColor clearColor]];
    self.leftView = spaceView;
    self.leftViewMode = UITextFieldViewModeAlways;
}


@end
