//
//  DrawerCell.m
//  Ajo
//
//  Created by Samreen on 29/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "DrawerCell.h"

@implementation DrawerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
