//
//  JSUser.m
//  Ajo
//
//  Created by Samreen Noor on 08/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "JSUser.h"

@implementation JSUser

+ (NSString *) validStringForObject:(NSString *) object{
    
    if (object)
        return object;
    
    return @"";
}

@end
