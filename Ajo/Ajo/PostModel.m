//
//  PostModel.m
//  Ajo
//
//  Created by Ahmed Sadiq on 16/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "PostModel.h"



@implementation PostModel


- (id)initWithDictionaryReviews:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        self.reviewdate_time = [self validStringForObject:responseData[@"date_time"]];

        self.reviewUser_name = [self validStringForObject:responseData[@"user_name"]];
        
            self.review_id = [self validStringForObject:responseData[@"review_id"]];
            self.rating = [responseData[@"rating"] floatValue];
            self.reviewtext =[self validStringForObject:responseData[@"text"]];
            self.reviewUser_id =[self validStringForObject:responseData[@"user_id"]];
            self.reviewPlace_id =[self validStringForObject:responseData[@"place_id"]];
            self.reviewUser_profile_pic =[self validStringForObject:responseData[@"user_profile_pic"]];
            
            self.postTyper = 1;
        
        
     
        
    }
    
    return self;
}
- (id)initWithDictionaryPost:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        
        
       
        self.postUser_name = [self validStringForObject:responseData[@"user_name"]];

        self.postdate_time = [self validStringForObject:responseData[@"date_time"]];

        self.post_id = [self validStringForObject:responseData[@"post_id"]];
        self.post_text =[self validStringForObject:responseData[@"post_text"]];
        self.Postuser_id =[self validStringForObject:responseData[@"user_id"]];
        self.postPlace_id =[self validStringForObject:responseData[@"place_id"]];
        self.postUser_profile_pic =[self validStringForObject:responseData[@"user_profile_pic"]];
        self.postTyper = 2;
        
    
        
        
        
        
    }
    
    return self;
}
- (id)initWithDictionaryPics:(NSDictionary *) responseData
{
    
    self = [super init];
    
    if (self) {
        

        
        self.date_time = [self validStringForObject:responseData[@"date_time"]];
        self.mainPicture_text = [self validStringForObject:responseData[@"picture_text"]];
        self.picturePlace_id =[self validStringForObject:responseData[@"place_id"]];
        self.post_picture_id =[self validStringForObject:responseData[@"post_picture_id"]];
        self.pictureUser_id =[self validStringForObject:responseData[@"user_id"]];
        self.pictureUser_name =[self validStringForObject:responseData[@"user_name"]];
        self.pictureUser_profile_pic =[self validStringForObject:responseData[@"user_profile_pic"]];
        
        
        
        self.picturesArray = [self mapArrayOfPics:responseData[@"pictures"]];
        self.postTyper = 3;
        
        
        
        
    }
    
    return self;
}







-(NSArray *) mapArrayOfPics:(NSArray *)picsArray{
    NSMutableArray * mappedArr = [NSMutableArray new];
    
    for (NSDictionary *dic in picsArray) {
        Pictures *pic =[[Pictures alloc]init];
        pic.picture =[self validStringForObject:dic[@"picture"]];
        pic.picture_id =[self validStringForObject:dic[@"picture_id"]];
        [mappedArr addObject:pic];
    }
    return mappedArr;
}

@end
