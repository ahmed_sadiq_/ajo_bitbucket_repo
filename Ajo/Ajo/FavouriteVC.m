//
//  FavouriteVC.m
//  Ajo
//
//  Created by Samreen on 29/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "FavouriteVC.h"
#import "PlaceCell.h"
#import "DrawerVC.h"
#import "Places.h"
#import "Pictures.h"
#import "UIImageView+URL.h"
#import "HomeDetailVC.h"
#import "PlaceService.h"



@interface FavouriteVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSUInteger currentSelectedIndex;
    NSMutableArray *placesArray;
    BOOL serverCall;
    int pageNo;


}
@end

@implementation FavouriteVC


-(void) setupView{
    self.navView.layer.shadowOffset = CGSizeMake(-2, 1);
    self.navView.layer.shadowRadius = 3;
    self.navView.layer.shadowOpacity = 0.5;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    placesArray = [[NSMutableArray alloc]init];
    serverCall = NO;
    pageNo = 1;
    
    [self setupRefreshControl];
    [self getFavPlaces];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    serverCall = NO;
    placesArray = [DataManager sharedManager].favouritePlaces;
    self.favPlacesArray = placesArray;
    self.tableView = _tblView;
    [_tblView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor       = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refreshHome:) forControlEvents:UIControlEventValueChanged];
    [_tblView addSubview:_refreshControl];
}

- (void)refreshHome:(id)sender
{
    pageNo = 1;
        [self getFavPlaces];
    
}
#pragma mark - UIButton Action
- (IBAction)btnMenuClicked:(id)sender {
    [[DrawerVC getInstance] AddInView:self];
    [[DrawerVC getInstance] ShowInView];
    
}





- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    NSArray *visibleRows = [_tblView visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [_tblView indexPathForCell:lastVisibleCell];
    NSLog(@"%ld",(long)path.row);
    if(path.section == 0 && path.row == (placesArray.count)-1)
    {
        if ( !serverCall) {
                pageNo++;
                
                [self getFavPlaces];
                
            
            
        }
    }
}
#pragma mark - Tableview Delegates


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return placesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Places *plac = [placesArray objectAtIndex: indexPath.row];
    
    PlaceCell * cell = (PlaceCell *)[_tblView dequeueReusableCellWithIdentifier:@"PlaceCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PlaceCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    Pictures *pic;
    if (plac.place_picture.count>0) {
        pic  = plac.place_picture[0];
        
    }
    [cell.placeImg setImageWithStringURL:pic.picture];
    cell.lblTitle.text = plac.placeName;
    cell.lblLocation.text = plac.placeAddress;
    cell.postCount.text = plac.postCount;
    cell.reviewCount.text = plac.reviewCount;
    cell.ratingView.canEdit = NO;
    cell.ratingView.rating = plac.rating;
    if ([plac.is_favourite isEqualToString:@"0"]) {
        //cell.favImg.image = [UIImage imageNamed:@"heart_unfilled"];
        [cell.btnFavourite setImage: [UIImage imageNamed:@"heart_unfilled"] forState:UIControlStateNormal];
        
    }
    else{
        //cell.favImg.image = [UIImage imageNamed:@"heart_filled"];
        [cell.btnFavourite setImage: [UIImage imageNamed:@"heart_filled"] forState:UIControlStateNormal];
        
    }
    cell.btnFavourite.tag = indexPath.row;
    cell.btnDetail.tag = indexPath.row;
    [cell.btnFavourite addTarget:self action:@selector(addPlaceToFav:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDetail addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 137;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

    
    
}

#pragma mark - Cell Utility Methods
- (void) btnDetailPressed : (UIButton *) sender {
    
    
    currentSelectedIndex = sender.tag;
    
    Places *place = [placesArray objectAtIndex: currentSelectedIndex];
    
    HomeDetailVC *homeDetailController = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC" bundle:nil];
    homeDetailController.place = place;
    homeDetailController.isFromDiscoverVc = YES;
    homeDetailController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:homeDetailController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    
}
#pragma mark - Api Call


-(void) getFavPlaces{
    serverCall = YES;
    NSString *page = [NSString stringWithFormat:@"%d",pageNo];
    [PlaceService getFavPlacesWithPagetoken:page success:^(id data) {
        [self.refreshControl endRefreshing];

        NSArray *tempPlace = data;
        if (tempPlace.count == 0) {
            serverCall = YES;

        }
        else{
        
        
        //////////
        if (pageNo==1) {
            [placesArray removeAllObjects];
            
            placesArray = data;
            [_tblView reloadData];
            serverCall = NO;
            
        }
        else{

            for (Places *place in tempPlace) {
                [placesArray addObject:place];
            }
            NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
            int startIndex = (pageNo-1) *20;
            for (int i = startIndex ; i < startIndex+20; i++) {
                if(i<placesArray.count) {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                }
            }
            [_tblView beginUpdates];
            [_tblView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
            [_tblView endUpdates];
            serverCall = NO;
            
        }
        self.favPlacesArray = placesArray;
        [DataManager sharedManager].favouritePlaces = placesArray;

        }

    } failure:^(NSError *error) {
        [self.refreshControl endRefreshing];

        [self showAlertMessage:error.localizedDescription];
    }];
}

#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"AJO!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
