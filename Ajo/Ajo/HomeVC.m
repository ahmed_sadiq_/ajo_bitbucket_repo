////
//  HomeVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "HomeVC.h"
#import "HomeCell.h"
#import "HomeDetailVC.h"
#import <QuartzCore/QuartzCore.h>
#import "LocationManager.h"
#import "PlaceService.h"
#import "UIImageView+URL.h"
#import "Places.h"
#import "Pictures.h"
#import "AddNewPost.h"
#import "SVProgressHUD.h"
#import "PlaceCell.h"
#import "PlacesCollectionCell.h"
#import "DrawerVC.h"
#import "AddNewPlaceViewController.h"
#import "UIButton+Style.h"
#import "UIView+Style.h"


@interface HomeVC ()<AddNewPostDelegate,UICollectionViewDelegate,UICollectionViewDataSource,MKMapViewDelegate>
{
    AddNewPost *addNewPost;
    NSMutableArray *placesArray;
    NSUInteger currentIndex;
    NSUInteger currentSelectedIndex;
    NSMutableArray *searchArray;
    NSMutableArray *placesDtaArray;
    NSString *pageToken;
    int pageNo;
    BOOL isSearch;
    BOOL serverCall;
    CLLocationCoordinate2D coordinate;
    Places *VisiblePlace;
}
@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    isSearch=NO;
    pageNo=1;
    serverCall = NO;
    placesArray = [[NSMutableArray alloc]init];
    searchArray = [[NSMutableArray alloc]init];
    placesDtaArray = [[NSMutableArray alloc]init];
    self.searchBox.layer.cornerRadius = 3;
    self.searchBox.layer.masksToBounds = YES;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"PlacesCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    // Do any additional setup after loading the view from its nib.
    
    self.navImg.layer.shadowOffset = CGSizeMake(-2, 1);
    self.navImg.layer.shadowRadius = 3;
    self.navImg.layer.shadowOpacity = 0.5;
    [self.plusBox roundCorner:30.0];
    [self setupRefreshControl];
    [self getCurrentUserLocation];
  
}
-(void)viewWillAppear:(BOOL)animated{
    
    serverCall = NO;
    if ([DataManager sharedManager].updateSettings) {
        pageNo=1;

        [self getDiscoverPlaces];
        [DataManager sharedManager].updateSettings = NO;
    }
    else{
        
        placesArray =   [DataManager sharedManager].nearBYPlaces;
    self.favPlacesArray = placesArray;

    [_mainTblView reloadData];
    }

}


-(void)dismissKeyboard {
    [_tfSearch resignFirstResponder];
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

  
    return placesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //currentIndex = (indexPath.row * 2);
    currentIndex = indexPath.row;
    Places *plac = [placesArray objectAtIndex: currentIndex];

    PlaceCell * cell = (PlaceCell *)[_mainTblView dequeueReusableCellWithIdentifier:@"PlaceCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PlaceCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    Pictures *pic;
        if (plac.place_picture.count>0) {
          pic  = plac.place_picture[0];
    
        }
    [cell.placeImg setImageWithStringURL:pic.picture];
    cell.lblTitle.text = plac.placeName;
    cell.lblLocation.text = plac.placeAddress;
    cell.postCount.text = plac.postCount;
    cell.reviewCount.text = plac.reviewCount;
    cell.ratingView.canEdit = NO;
    cell.ratingView.rating = plac.rating;
    if ([plac.is_favourite isEqualToString:@"0"]) {
        //cell.favImg.image = [UIImage imageNamed:@"heart_unfilled"];
        [cell.btnFavourite setImage: [UIImage imageNamed:@"heart_unfilled"] forState:UIControlStateNormal];

    }
    else{
       //cell.favImg.image = [UIImage imageNamed:@"heart_filled"];
        [cell.btnFavourite setImage: [UIImage imageNamed:@"heart_filled"] forState:UIControlStateNormal];

    }
       cell.btnFavourite.tag = indexPath.row;
       cell.btnDetail.tag = indexPath.row;
       [cell.btnFavourite addTarget:self action:@selector(addPlaceToFav:) forControlEvents:UIControlEventTouchUpInside];
       [cell.btnDetail addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];

    
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 137;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    
    
}


-(void) showPlaces{
    self.mapView.delegate = self;
    for (Places *plc in placesArray) {
        
      CGFloat  longi = (CGFloat)[plc.longitude floatValue];
     CGFloat   lati = (CGFloat)[plc.latitude floatValue];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:CLLocationCoordinate2DMake(lati, longi)];
    [annotation setTitle:plc.placeName]; //You can set the subtitle too
    [self.mapView addAnnotation:annotation];
    [self.mapView showAnnotations:self.mapView.annotations animated:YES];
        [plc setAnnotation:annotation];
    }
    if (placesArray.count>0) {
        VisiblePlace = placesArray[0];
        [_mapView selectAnnotation:VisiblePlace.annotation animated:YES];

    }
  
}

//
//-(void)zoomInToMyLocation
//{
//    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
//    region.center.latitude = [VisiblePlace.latitude floatValue] ;
//    region.center.longitude = [VisiblePlace.longitude floatValue] ;;
//    region.span.longitudeDelta = 0.15f;
//    region.span.latitudeDelta = 0.15f;
//    [_mapView setRegion:region animated:YES];
//}
#pragma mark -UIBUTTON Action
- (IBAction)btnMenuClicked:(id)sender {
    [[DrawerVC getInstance] AddInView:self];
    [[DrawerVC getInstance] ShowInView];
    
}
- (IBAction)btnShowPlaces:(id)sender {
    
    
    if (_placesBox.hidden == YES) {
        [self showPlaces];
        _placesBox.hidden = NO;
        [_collectionView reloadData];
        [_btnShowMap setImage:[UIImage imageNamed:@"ajo_icon_bullet_white"] forState:UIControlStateNormal];

    }
    else{
        _placesBox.hidden = YES;
        
        [_btnShowMap setImage:[UIImage imageNamed:@"ajo_icon_map"] forState:UIControlStateNormal];
    
    }
  
}

#pragma mark - Cell Utility Methods
- (void) btnDetailPressed : (UIButton *) sender {
    
    
    currentSelectedIndex = sender.tag;
    [self dismissKeyboard];
    
    Places *place = [placesArray objectAtIndex: currentSelectedIndex];
    
    HomeDetailVC *homeDetailController = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC" bundle:nil];
    homeDetailController.place = place;
    homeDetailController.isFromDiscoverVc = YES;
    homeDetailController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:homeDetailController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    
}
- (void) leftViewBtnPressed : (UIButton *) sender {
    
    
    currentSelectedIndex = sender.tag;

    Places *place = [placesArray objectAtIndex: currentSelectedIndex];

    HomeDetailVC *homeDetailController = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC" bundle:nil];
    homeDetailController.place = place;
    homeDetailController.isFromDiscoverVc = YES;
    homeDetailController.hidesBottomBarWhenPushed = YES;

    //[self.navigationController pushViewController:homeDetailController animated:YES];
    [[UIApplication sharedApplication].keyWindow.rootViewController.navigationController pushViewController:homeDetailController animated:YES
     ];    [self.navigationController setNavigationBarHidden:YES];
    
}

- (void) rightViewBtnPressed : (UIButton *) sender {
    currentSelectedIndex = sender.tag;
    Places *place = [placesArray objectAtIndex: currentSelectedIndex];

    HomeDetailVC *homeDetailController = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC" bundle:nil];
    homeDetailController.place = place;
    homeDetailController.isFromDiscoverVc = YES;
    homeDetailController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:homeDetailController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void) menuBtnPressed : (UIButton *) sender {

    currentSelectedIndex = sender.tag;
    Places *place = [placesArray objectAtIndex: currentSelectedIndex];
    
        addNewPost   = [AddNewPost loadWithNibWithDelegate:self From:@"HomeVC" andPlace:place];
        [addNewPost setDelegate:self];
        [self.view addSubview:addNewPost];
    
    [addNewPost show];

}

#pragma mark - NEW POST DELEGATES
-(void)addNewPost:(PostModel *)postObj{
    if(postObj.postTyper == 1 ) {
        
        BOOL isReviewExist = NO;
        Places *p = [placesArray objectAtIndex:currentSelectedIndex];

        for (int i=0;i< p.reviewsArray.count;i++) {
            Reviews *review = [p.reviewsArray objectAtIndex:i];
            if ([review.review_id isEqualToString:postObj.review_id]) {
                [review setText:postObj.reviewtext];
                [review setRating:[NSString stringWithFormat:@"%f",postObj.rating]];
                isReviewExist = YES;
                break;
            }
        }
            if (!isReviewExist) {

                int  count = [p.reviewCount intValue];
                count++;
                [p setReviewCount:[NSString stringWithFormat:@"%d",count]];
                
            }

        }

    [_mainTblView reloadData];
    
    Places *place = [placesArray objectAtIndex: currentSelectedIndex];
    
    HomeDetailVC *homeDetailController = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC" bundle:nil];
    homeDetailController.place = place;
    homeDetailController.isFromDiscoverVc = YES;
    homeDetailController.isFromNewPost = YES;
    homeDetailController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:homeDetailController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if ([searchStr isEqualToString:@""]) {
        
    placesArray = placesDtaArray.mutableCopy;
        [DataManager sharedManager].nearBYPlaces = placesArray;

        self.favPlacesArray = placesArray;

    [_mainTblView reloadData];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (![textField.text isEqualToString:@""]) {
    [self searchApi];
    }
    return YES;
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    if (_placesBox.hidden == YES) {
        
    NSArray *visibleRows = [_mainTblView visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [_mainTblView indexPathForCell:lastVisibleCell];
    NSLog(@"%ld",(long)path.row);
    if(path.section == 0 && path.row == (placesArray.count)-1)
    {
        if (!isSearch&& !serverCall) {
            NSString *token =[DataManager sharedManager].discoverPageToken;
            if (![token isEqualToString:@""]) {
                pageNo++;

                [self getDiscoverPlaces];

            }
        
        }
   }
    }
    else{
        for (UICollectionViewCell *cell in [self.collectionView visibleCells]) {
            NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
            NSLog(@"%@",indexPath);
            VisiblePlace = placesArray[indexPath.row];
            //[self zoomInToMyLocation];
        
            [_mapView selectAnnotation:VisiblePlace.annotation animated:YES];

            
        }
    
    }
}


-(void)getDiscoverPlaces{
    isSearch=NO;
    serverCall = YES;

    pageToken = [NSString stringWithFormat:@"%d", pageNo];
    if (pageNo!=1) {
        pageToken = [DataManager sharedManager].discoverPageToken;
    }
    NSString *distance = @"300";
    NSString *lat = [DataManager sharedManager].currentUser.latitude;
    NSString *longi =  [DataManager sharedManager].currentUser.longitude;
    NSString *location = [NSString stringWithFormat:@"%@,%@",lat,longi];
    [PlaceService getDiscoverPlacesWithLocation:location Radius:distance Pagetoken:pageToken success:^(id data) {
        [self.refreshControl endRefreshing];
        _tfSearch.text = @"";
        NSArray *tempPlace = data;
        //if (tempPlace.count>0) {
        if (pageNo==1) {
            [placesArray removeAllObjects];

        placesArray = data;
        placesDtaArray = placesArray.mutableCopy;
        [_mainTblView reloadData];
            serverCall = NO;
            [SVProgressHUD dismiss];

        }
        else{
            for (Places *place in tempPlace) {
                [placesArray addObject:place];
            }
            NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
            int startIndex = (pageNo-1) *20;
            for (int i = startIndex ; i < startIndex+20; i++) {
                if(i<placesArray.count) {
                    [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                }
            }
                [_mainTblView beginUpdates];
                [_mainTblView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                [_mainTblView endUpdates];
            serverCall = NO;
            
            [SVProgressHUD dismiss];
        }
        self.favPlacesArray = placesArray;
        [DataManager sharedManager].nearBYPlaces = placesArray;

        if (placesArray.count>0) {
            _lblLocationAllow.hidden = YES;
            [_btnShowMap setEnabled:YES];
        }
        else
        {
            _lblLocationAllow.hidden = NO;
            [_btnShowMap setEnabled:NO];

        }
//        }
//        else{
//            serverCall = NO;
//            [SVProgressHUD dismiss];
//
//        }
    } failure:^(NSError *error) {
        [self.refreshControl endRefreshing];

        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        [alertView show];
    }];

}
-(void) resetPlaceView{
    _placesBox.hidden = YES;
    
    [_btnShowMap setImage:[UIImage imageNamed:@"ajo_icon_map"] forState:UIControlStateNormal];
    


}

-(void) searchApi{
    [self resetPlaceView];
    isSearch=YES;
    pageNo=1;
    serverCall=YES;
    NSString *distance = @"300";
    NSString *lat = [DataManager sharedManager].currentUser.latitude;
    NSString *longi =  [DataManager sharedManager].currentUser.longitude;
    NSString *location = [NSString stringWithFormat:@"%@,%@",lat,longi];
    NSString *page = [NSString stringWithFormat:@"%d",pageNo];

     [PlaceService searchPlacesWithUrl:URL_PLACE_SEARCH PageToken:page Location:location SearchKeyword:_tfSearch.text Category:@"" SubCategory:@"" success:^(id data) {
        
        searchArray = data;
        placesArray = searchArray;
         [DataManager sharedManager].nearBYPlaces = placesArray;

         self.favPlacesArray = placesArray;

        [_mainTblView reloadData];
        serverCall=NO;
        [SVProgressHUD dismiss];

    } failure:^(NSError *error) {
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
        [alertView show];
    }
    ];
}
- (IBAction)btnSearch:(id)sender {
    if (![_tfSearch.text isEqualToString:@""]) {
        [_tfSearch resignFirstResponder];
    [self searchApi];
    }
}
- (IBAction)btnAddNewPost:(id)sender {
    
    AddNewPlaceViewController *newPlaceController = [[AddNewPlaceViewController alloc] initWithNibName:@"AddNewPlaceViewController" bundle:nil];
    newPlaceController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:newPlaceController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];

    
}


- (void)setupRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor       = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(refreshHome:) forControlEvents:UIControlEventValueChanged];
    [_mainTblView addSubview:_refreshControl];
}

- (void)refreshHome:(id)sender
{
    pageNo=1;
    if (!serverCall) {

 //   [self getDiscoverPlaces];
        [self getCurrentUserLocation];

    }
}




-(void) getCurrentUserLocation{
    [[LocationManager sharedManager] getUserCurrentLocationWithCompletionBlock:^(CLLocationCoordinate2D location) {
        
        coordinate = location;
        [LocationManager sharedManager].currentLat = location.latitude ;
        [LocationManager sharedManager].currentLong  = location.longitude ;

        [DataManager sharedManager].currentUser.latitude =[NSString stringWithFormat:@"%f",location.latitude];
          [DataManager sharedManager].currentUser.longitude =[NSString stringWithFormat:@"%f",location.longitude];
            _lblLocationAllow.hidden = YES;
        [self getDiscoverPlaces];

        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",location.latitude] forKey:@"placesLat"];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",location.longitude] forKey:@"placesLong"];
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:loc
                       completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error){
                 NSLog(@"Geocode failed with error: %@", error);
                 return;
             }
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
             NSLog(@"locality %@",placemark.locality);
             NSLog(@"postalCode %@",placemark.postalCode);
             NSLog(@"location %@",placemark.subLocality);
             NSLog(@"location %@",placemark.name);
             
             [[NSUserDefaults standardUserDefaults] setObject:placemark.name forKey:@"location"];
             
             
         }];
        
        
        
        
        
    } errorBlock:^(NSError *error) {
        
        if (placesArray.count>0) {
            _lblLocationAllow.hidden = YES;
        }
        else
        {
            _lblLocationAllow.hidden = NO;
            
        }
        
        NSLog(@"%@",error.localizedDescription);
    }];
    
}
#pragma mark - collectionView Methods


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //return [self.dataArray count];
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return placesArray.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Places *plac = [placesArray objectAtIndex: indexPath.row];
    
    PlacesCollectionCell * cell = (PlacesCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PlacesCollectionCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    Pictures *pic;
    if (plac.place_picture.count>0) {
        pic  = plac.place_picture[0];
        
    }
    [cell.placeImg setImageWithStringURL:pic.picture];
    cell.lblTitle.text = plac.placeName;
    cell.lblLocation.text = plac.placeAddress;
    cell.postCount.text = plac.postCount;
    cell.reviewCount.text = plac.reviewCount;
    cell.ratingView.canEdit = NO;
    cell.ratingView.rating = plac.rating;
    if ([plac.is_favourite isEqualToString:@"0"]) {
        [cell.btnFavourite setImage: [UIImage imageNamed:@"heart_unfilled"] forState:UIControlStateNormal];
    }
    else{
        [cell.btnFavourite setImage: [UIImage imageNamed:@"heart_filled"] forState:UIControlStateNormal];
        
    }
    cell.btnFavourite.tag = indexPath.row;
    cell.btnDetail.tag = indexPath.row;
    [cell.btnFavourite addTarget:self action:@selector(addPlaceToFav:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDetail addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    

 
    return cell;
    
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return CGSizeMake(334, 135);
}




@end
