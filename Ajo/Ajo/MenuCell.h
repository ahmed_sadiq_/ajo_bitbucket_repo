//
//  MenuCell.h
//  Ajo
//
//  Created by Samreen Noor on 14/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *placeName;

@end
