//
//  Places.h
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseEntity.h"
#import <MapKit/MapKit.h>

@interface Places : BaseEntity
@property (strong, nonatomic) NSString *placeName;
@property (strong, nonatomic) NSString *placePhone;
@property (strong, nonatomic) NSString *placeEmail;
@property (strong, nonatomic) NSString *placeWebsite;
@property (strong, nonatomic) NSString *placeAddress;
@property (strong, nonatomic) NSString *placeDescription;
@property (strong, nonatomic) NSString *is_favourite;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *distance;

@property (strong, nonatomic) NSString *place_id;
@property (strong, nonatomic) NSArray *place_picture;
@property (strong, nonatomic) NSArray *postPicturesArray;
@property (strong, nonatomic) NSString *post_pictures_count;
@property (strong, nonatomic) NSArray *postsArray;
@property (strong, nonatomic) NSString *postCount;
@property (strong, nonatomic) NSString *reviewCount;
@property (strong, nonatomic) NSArray *reviewsArray;
@property (assign, nonatomic) float rating;
@property (strong, nonatomic) MKPointAnnotation *annotation;




- (id)initWithDictionary:(NSDictionary *) responseData;
+ (NSArray *)mapPlacesFromArray:(NSArray *)arrlist;

@end
