//
//  CustomBoxView.m
//  Ajo
//
//  Created by Samreen on 04/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "CustomBoxView.h"

@implementation CustomBoxView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 
- (void)drawRect:(CGRect)rect {
    self.layer.cornerRadius=5.0f;
    self.layer.masksToBounds=YES;
    self.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.layer.borderWidth= 1.0f;
}


@end
