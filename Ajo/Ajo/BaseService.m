//
//  BaseService.m
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseService.h"

@implementation BaseService

+ (NSString *) currentUserID{
    
    return [DataManager sharedManager].currentUser.userID;
}
+ (NSError *)createErrorWithLocalizedDescription:(NSString *) localizeDescription
{
    NSError *error = [NSError errorWithDomain:@"Failed!"
                                         code:100
                                     userInfo:@{
                                                NSLocalizedDescriptionKey:localizeDescription
                                                }];
    return error;
}


@end
