//
//  HomeCell.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *leftView;
@property (strong, nonatomic) IBOutlet UIView *rightView;
@property (strong, nonatomic) IBOutlet UIButton *leftBtn;
@property (strong, nonatomic) IBOutlet UIButton *rightBtn;
@property (strong, nonatomic) IBOutlet UIImageView *leftImg;
@property (strong, nonatomic) IBOutlet UIImageView *rightImg;
@property (weak, nonatomic) IBOutlet UILabel *leftPostCount;
@property (weak, nonatomic) IBOutlet UILabel *leftReviewCount;
@property (weak, nonatomic) IBOutlet UILabel *rightPostCount;
@property (weak, nonatomic) IBOutlet UILabel *rightReviewCount;
@property (weak, nonatomic) IBOutlet UILabel *lblRightPlaceName;

@property (weak, nonatomic) IBOutlet UILabel *lblLeftPlaceName;
@property (weak, nonatomic) IBOutlet UIButton *leftMenuBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightMenuBtn;
@end
