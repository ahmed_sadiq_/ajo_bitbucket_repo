//
//  HomeDetailPhotoCell.h
//  Ajo
//
//  Created by Ahmed Sadiq on 16/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDetailPhotoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileImg;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UILabel *dataTime;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScroller;

@property (weak, nonatomic) IBOutlet UIButton *btnDeletePost;
@end
