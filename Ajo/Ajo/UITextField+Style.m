//
//  UITextField+Style.m
//  Ajo
//
//  Created by Samreen on 27/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "UITextField+Style.h"

@implementation UITextField (Style)
- (void) setLeftSpaceSize:(CGFloat) size
{
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size, 40.0)];
    [spaceView setBackgroundColor:[UIColor clearColor]];
    self.leftView = spaceView;
    self.leftViewMode = UITextFieldViewModeAlways;
    
}

- (void) setRightSpaceSize:(CGFloat) size
{
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size, 40.0)];
    [spaceView setBackgroundColor:[UIColor clearColor]];
    self.rightView = spaceView;
    self.rightViewMode = UITextFieldViewModeAlways;
}



@end
