//
//  SettingsVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKDropdownMenu.h"
@interface SettingsVC : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnSaveLocation;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *navImg;

@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIView *emailview;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *tfLocation;
@property (weak, nonatomic) IBOutlet UITextField *tfProximity;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *dropDownMenu;

@property (weak, nonatomic) IBOutlet UIView *rateView;

@property (strong, nonatomic) UINavigationController *navController;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIView *webBoxView;
@property (weak, nonatomic) IBOutlet UILabel *lblTermandCondition;

@end
