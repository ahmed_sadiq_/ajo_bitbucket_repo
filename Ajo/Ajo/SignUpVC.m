//
//  SignUpVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SignUpVC.h"
#import "ProfileVC.h"
#import "SettingsVC.h"
#import "ViewController.h"
#import "RecommendationVC.h"
#import "UIImageView+RoundImage.h"
#import "AccountService.h"
#import "UIImage+JS.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "ForgotPasswordVC.h"
#import "Constant.h"
#import "LocationManager.h"
#import "UITextField+Style.h"
#import "JSFacebookManager.h"
#import "FavouriteVC.h"

@interface SignUpVC ()
{
    UITextField *currentField;

}
@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_profileImg roundImageCorner];
    

    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [singleTap setNumberOfTapsRequired:1];
    // singleTap.delegate=delegate;
    [self.view addGestureRecognizer:singleTap];
    
    [_nameTxt setLeftSpaceSize:10.0];
    [_emailTxt setLeftSpaceSize:10.0];
    [_tfpassword setLeftSpaceSize:10.0];
    [_retypePasswordTxt setLeftSpaceSize:10.0];

    [[_btnSignUp layer] setCornerRadius:3.0f];
    
    [[_btnSignUp layer] setMasksToBounds:YES];
    
    [[_btnSignUp layer] setBorderWidth:1.0f];
    _btnSignUp.layer.borderColor = [UIColor whiteColor].CGColor;

}
-(void)dismissKeyboard {
    
    NSLog(@"screen touch");
    
    [currentField resignFirstResponder];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnRetypePassShow:(id)sender {
    if (self.retypePasswordTxt.secureTextEntry == YES) {
        self.imgRetypeVisible.image = [UIImage imageNamed:@"invisible"];
        self.retypePasswordTxt.secureTextEntry = NO;
        
    }
    
    else
    {
        self.imgRetypeVisible.image = [UIImage imageNamed:@"visible"];
        self.retypePasswordTxt.secureTextEntry = YES;
    }
}

- (IBAction)btnPassShow:(id)sender {
    if (self.tfpassword.secureTextEntry == YES) {
        self.imgPassVisible.image = [UIImage imageNamed:@"invisible"];
        self.tfpassword.secureTextEntry = NO;
        
    }
    
    else
    {
        self.imgPassVisible.image = [UIImage imageNamed:@"visible"];
        self.tfpassword.secureTextEntry = YES;
    }

}
- (IBAction)btnFbSignUp:(id)sender {
    
    
    [[JSFacebookManager sharedManager] getUserInfoWithCompletion:^(id data) {
        JSUser *user = data;
        [AccountService fbLoginWithFirstName:user.firstName email:user.email andProfielImg:user.imageURL success:^(id data) {
            [self createTabBarAndControl];
            
        } failure:^(NSError *error) {
            [self showAlertMessage:error.localizedDescription];
            
        }];
        
        NSLog(@"%@",data);
    } failure:^(NSError *error) {
        NSLog(@"%@",error.localizedDescription);
        if (error.localizedDescription == nil) {//Cancelled
            //[self showAlertMessage:@"Cancelled"];

        }
        else
        [self showAlertMessage:error.localizedDescription];
        
    } ];

}
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"AJO!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}

#pragma mark - TextField Methods


-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}


-(BOOL)checktextFields{
    
    BOOL willEnabled = NO;
    if( [_nameTxt.text length] >= 1 && [self emailValidate:_emailTxt.text] == YES && [_tfpassword.text length] > 1  ){
        willEnabled = YES;
    }
    if ([_nameTxt.text  length]==0 ||[_emailTxt.text  length]==0 ||[_tfpassword.text length]==0) {
        [self showAlertMessage:@"Please enter valid informtation"];
        return NO;
    }
    if (![self emailValidate:_emailTxt.text]) {
        [self showAlertMessage:@"Please enter valid  email"];
        return NO;
    }
    if (![_tfpassword.text isEqualToString: _retypePasswordTxt.text]) {
        [self showAlertMessage:@"Please enter password carefully"];
        return NO;
    }
 
    
    return willEnabled;
}




#pragma mark - api

-(void) registerAPICall{
    
    [AccountService registerUserWithFirstName:_nameTxt.text email:_emailTxt.text password:_tfpassword.text andProfielImg:_profileImg.image success:^(id data) {
        [self createTabBarAndControl];

    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];


    }];
    



}

- (IBAction)cameraBtnPressed:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }else{
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Camera Unavailable" message:@"Unable to find a camera on your device." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (IBAction)galleryBtnPressed:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)signUpPressed:(id)sender {
    if ([self checktextFields]) {
        
      [self registerAPICall];

    }
}

- (IBAction)loginPressed:(id)sender {
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[ViewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
        }
    }
}

- (IBAction)forgotPswdPressed:(id)sender {
        ForgotPasswordVC *forgotPswd = [[ForgotPasswordVC alloc] initWithNibName:@"ForgotPasswordVC" bundle:nil];
        [self.navigationController pushViewController:forgotPswd animated:YES];
        [self.navigationController setNavigationBarHidden:YES];
    
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    currentField=textField;
    [self animateTextField:nil up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:nil up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


#pragma mark -  camera delegate methods


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = nil; // [info objectForKey:UIImagePickerControllerEditedImage];
    
    if (image == nil){
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        if (image == nil){
            NSLog(@"Image with some error.");
        }
    }
    
    
    
    UIImage *chosenImage = [image imageWithScaledToSize:CGSizeMake(204, 204)];
    
    
    
    _profileImg.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark -  tabbar creation methods after sign up

-(void)createTabBarAndControl {
    
    
    self.viewController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    
    
    UIImage *img = [UIImage imageNamed:@"ajonear"];
    
    [self.viewController.tabBarItem initWithTitle:@"NEARBY" image:[img imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    self.viewController.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [self.viewController.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *recommendationVC;
    recommendationVC = [[RecommendationVC alloc] initWithNibName:@"RecommendationVC" bundle:[NSBundle mainBundle]];
    
    
    UINavigationController *recommendationNavController = [[UINavigationController alloc] initWithRootViewController:recommendationVC];
    
    
    
    UIImage *search = [UIImage imageNamed:@"ajosearch"];
    
    [recommendationNavController.tabBarItem initWithTitle:@"SEARCH" image:[search imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    recommendationNavController.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [recommendationVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *favouriteVC;
    favouriteVC = [[FavouriteVC alloc] initWithNibName:@"FavouriteVC" bundle:[NSBundle mainBundle]];
    
    
    UINavigationController *favouriteVCNavController = [[UINavigationController alloc] initWithRootViewController:favouriteVC];
    
    UIImage *favIcon = [UIImage imageNamed:@"ajoFavorite"];
    
    [favouriteVC.tabBarItem initWithTitle:@"FAVOURITE" image:[favIcon imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    favouriteVC.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [favouriteVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *profileVC;
    profileVC = [[ProfileVC alloc] initWithNibName:@"ProfileVC" bundle:[NSBundle mainBundle]];
    UINavigationController *profileNavController = [[UINavigationController alloc] initWithRootViewController:profileVC];
    
    
    UIImage *profilicon = [UIImage imageNamed:@"ajoprofile"];
    
    [profileVC.tabBarItem initWithTitle:@"PROFILE" image:[profilicon imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    profileVC.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [profileVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    UIViewController *settingVC;
    settingVC = [[SettingsVC alloc] initWithNibName:@"SettingsVC" bundle:[NSBundle mainBundle]];
    UINavigationController *settingNavController = [[UINavigationController alloc] initWithRootViewController:settingVC];
    
    
    UIImage *settingIcon = [UIImage imageNamed:@"ajosettings"];
    
    [settingVC.tabBarItem initWithTitle:@"SETTINGS" image:[settingIcon imageWithRenderingMode:UIImageRenderingModeAutomatic] tag:2];
    settingVC.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [settingVC.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.tabBarController = [[UITabBarController alloc] init] ;
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:self.navController, recommendationNavController,favouriteVCNavController,profileNavController,settingNavController,nil];
    
    [self.tabBarController.tabBar setBackgroundColor:[UIColor whiteColor]];
    self.tabBarController.tabBar.tintColor = [UIColor colorWithRed:59.0/255.0 green:197.0/255.0 blue:186.0/255.0 alpha:1];
    self.viewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //self.viewController.navigationController.navigationBar
    [[[UIApplication sharedApplication]delegate] window].rootViewController = self.tabBarController;
}


@end
