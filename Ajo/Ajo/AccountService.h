//
//  AccountService.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseService.h"

@interface AccountService : BaseService
+(void) loginUserWithEmail:(NSString *) email
                  password:(NSString *) pass
                   success:(serviceSuccess) success
                   failure:(serviceFailure) failure;


+(void)registerUserWithFirstName:(NSString *)Name
                           email:(NSString *)email
                        password:(NSString *)password
                   andProfielImg:(UIImage *)img
                         success:(serviceSuccess)success
                         failure:(serviceFailure)failure;
+(void) fbLoginWithFirstName:(NSString *)Name
                       email:(NSString *)email
               andProfielImg:(NSString *)img
                     success:(serviceSuccess)success
                     failure:(serviceFailure)failure;


+ (void) forgetPasswordWhereEmail : (NSString *) email
                           succuss:(serviceSuccess) success
                           failure:(serviceFailure) failure;


+(void)editUserProfileWithName:(NSString *)Name
                        userID:(NSString *)userId
                         email:(NSString *)email
                     ProfielImg:(UIImage *)img
                          City:(NSString *)city
                         Phone:(NSString *)phone
                       Country:(NSString *)country
                       Address:(NSString *)address
                      Latitude:(NSString *)lat
                      Logitude:(NSString *)longi
                    PlanceName:(NSString *)placename
                  andProximity:(NSString *)proximity
                       success:(serviceSuccess)success
                       failure:(serviceFailure)failure;



+(void) saveUserSettingsWithUserID:(NSString *)userId
                              City:(NSString *)city
                             State:(NSString *)state
                           Country:(NSString *)country
                           ZipCode:(NSString *)zipCode
                          Latitude:(NSString *)lat
                          Logitude:(NSString *)longi
                        PlanceName:(NSString *)placename
                      andProximity:(NSString *)proximity
                           success:(serviceSuccess)success
                           failure:(serviceFailure)failure;

+(void) saveLocationSettingsWithUserID:(NSString *)userId
                              Latitude:(NSString *)lat
                              Logitude:(NSString *)longi
                            PlanceName:(NSString *)placename
                          andProximity:(NSString *)proximity
                               success:(serviceSuccess)success
                               failure:(serviceFailure)failure;
@end
