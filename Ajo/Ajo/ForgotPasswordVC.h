//
//  ForgotPasswordVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 07/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;

- (IBAction)resetPswdPressed:(id)sender;
- (IBAction)createAccountPressed:(id)sender;
- (IBAction)letsDiscoverPressed:(id)sender;

@end
