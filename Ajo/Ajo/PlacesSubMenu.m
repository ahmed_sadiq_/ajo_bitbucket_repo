//
//  PlacesSubMenu.m
//  Ajo
//
//  Created by Samreen Noor on 14/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "PlacesSubMenu.h"

@implementation PlacesSubMenu


-(id)initWithDict:(NSDictionary *) dic
{
    self = [super init];
    if (self) {
        
        NSDictionary *temp=dic[@"result"];
        NSDictionary *tem=temp[@"geometry"];
        self.title = temp[@"name"];
        self.subTitle = temp[@"formatted_address"];
        self.lat=[NSString stringWithFormat:@"%@", tem[@"location"][@"lat"]];
        self.lng= [NSString stringWithFormat:@"%@", tem[@"location"][@"lng"]];
        
        
        
    }
    return self;
}

@end
