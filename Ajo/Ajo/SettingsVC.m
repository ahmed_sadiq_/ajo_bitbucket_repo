//
//  SettingsVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "SettingsVC.h"
#import "AccountService.h"
#import "DataManager.h"
#import "User.h"
#import "ViewController.h"
#import "LocationManager.h"
#import "MainMenu.h"
#import "PlacesSubMenu.h"
#import "MenuCell.h"
#import <MessageUI/MessageUI.h>
#import "UIAlertView+JS.h"
#import "DrawerVC.h"

@interface SettingsVC ()<UITableViewDataSource,MKDropdownMenuDelegate,MKDropdownMenuDataSource, UITableViewDelegate,MFMailComposeViewControllerDelegate>
{
 
    NSString *userId;
    MFMailComposeViewController *mailComposer;
   

 


}
@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    mailComposer = [[MFMailComposeViewController alloc] init];

  User *user  =[DataManager sharedManager].currentUser;
    userId = user.userID;
    
    self.navImg.layer.shadowOffset = CGSizeMake(-2, 1);
    self.navImg.layer.shadowRadius = 3;
    self.navImg.layer.shadowOpacity = 0.5;
    


  
}


-(void) setUpProfileView{
    User *user=[DataManager sharedManager].currentUser;
    


    userId = user.userID;

    
    
    //########   header view ########


    
    
}


-(void)viewWillAppear:(BOOL)animated{
   // [self setUpView];
    [self setUpProfileView];



}
-(void)viewDidLayoutSubviews{
   // [_scrollView setContentSize:(CGSizeMake(375, 700))];
    if (IS_IPHONE_5 || IS_IPAD) {
        _scrollView.contentSize = CGSizeMake(320, 700);
        
    }
    else  if (IS_IPHONE_6) {
        _scrollView.contentSize = CGSizeMake(375, 700);
        
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





- (IBAction)btnLogout:(id)sender {
    ViewController *viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    self.navController = [[UINavigationController alloc] initWithRootViewController:viewController];
    self.navController.navigationBar.hidden = YES;
    [[DataManager sharedManager].nearBYPlaces removeAllObjects];
    [[DataManager sharedManager].favouritePlaces removeAllObjects ];

    [[[UIApplication sharedApplication]delegate] window].rootViewController = self.navController;

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_logged_in"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_Id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_email"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user_profile_pic"];

   
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"City"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"State"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Country"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ZipCode"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Proximity"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"placesLong"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"placesLat"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"location"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"address"];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"phone"];
    
    
 

    [[NSUserDefaults standardUserDefaults] synchronize];

    
    
}


#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"AJO!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}



- (IBAction)btnFacebook:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/ajoafrica"]];

}
- (IBAction)btnTwit:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/@ajo_africa"]];

}
- (IBAction)btnInstagram:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://instagram.com/ajo_africa"]];
    
}
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissModalViewControllerAnimated:YES];
    
}


#pragma mark -UIBUTTON Action
- (IBAction)btnMenuClicked:(id)sender {
    [[DrawerVC getInstance] AddInView:self];
    [[DrawerVC getInstance] ShowInView];
    
}
- (IBAction)btnEmail:(id)sender {
    _popupView.hidden =NO;
    _rateView.hidden = YES;
    _emailview.hidden = NO;
}
- (IBAction)btnRatingViewShow:(id)sender {
    
    _popupView.hidden =NO;
    _rateView.hidden = NO;
    _emailview.hidden = YES;
}

- (IBAction)btnPhone:(id)sender {
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://2135554321"]];

    NSString *phNo = @"+18323821712";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }

}

- (IBAction)btnSecondPhoneNumber:(id)sender {
    
    NSString *phNo = @"+2347034837030";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }

}

- (IBAction)hideBtn:(id)sender {
    _popupView.hidden =YES;
    _rateView.hidden = YES;
    _emailview.hidden = YES;
}
- (IBAction)btnRate:(id)sender {
    NSString *iTunesLink = @"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=604760686&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

- (IBAction)btnEmailpressed:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {

    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:@"AJO"];
    [mailComposer setMessageBody:@"Testing AJO mail" isHTML:NO];
    [mailComposer setToRecipients:@[@"support@ajoafrica.com"]];

    [self presentModalViewController:mailComposer animated:YES];
    }
    else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Configuration Error!"
                                                         message:@"Please configure email account in device settings"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles: nil];
        [alert show];
    }
    _popupView.hidden =YES;
    _rateView.hidden = YES;
    _emailview.hidden = YES;
}
- (IBAction)btnTermAndConditions:(id)sender {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"tems_conditions" ofType:@"pdf"];

    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.scrollTo(0.0, 50.0)"]];
    [_webView loadRequest:request];
    // [self.view addSubview:_webView];
    _lblTermandCondition.text = @"Term and Conditions";
    _webBoxView.hidden = NO;

    
}
- (IBAction)btnPrivacy:(id)sender {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Ajo_Privacy_Policy_2" ofType:@"pdf"];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [_webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.scrollTo(0.0, 50.0)"]];
    [_webView loadRequest:request];
   // [self.view addSubview:_webView];
    _webBoxView.hidden = NO;
    _lblTermandCondition.text = @"Privacy";

}
- (IBAction)btnHideWebView:(id)sender {
    _webBoxView.hidden = YES;
}
@end
