//
//  CustomTextFieldBox.m
//  Ajo
//
//  Created by Samreen on 04/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "CustomTextFieldBox.h"

@implementation CustomTextFieldBox


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    self.layer.cornerRadius=5.0f;
    self.layer.masksToBounds=YES;
    self.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.layer.borderWidth= 1.0f;
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40.0)];
    [spaceView setBackgroundColor:[UIColor clearColor]];
    self.leftView = spaceView;
    self.leftViewMode = UITextFieldViewModeAlways;
}


@end
