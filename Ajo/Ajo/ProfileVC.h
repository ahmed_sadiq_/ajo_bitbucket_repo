//
//  ProfileVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKDropdownMenu.h"

@interface ProfileVC : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate, MKDropdownMenuDataSource, MKDropdownMenuDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet MKDropdownMenu *proxiMneu;
@property (weak, nonatomic) IBOutlet UITextView *tfAddress;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxView2Constrain;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *cityDropDown;
@property (weak, nonatomic) IBOutlet UIView *contryPicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *personalLabelLeftConstrain;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationLabelLeftConstrain;

@property (weak, nonatomic) IBOutlet MKDropdownMenu *dropDownMneu;
@property (weak, nonatomic) IBOutlet UILabel *lblCountry;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UITextField *nameTxt;
@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UITextField *tfLocation;
@property (weak, nonatomic) IBOutlet UITextField *tfProximity;
@property (weak, nonatomic) IBOutlet UITextField *tfPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;

@property (weak, nonatomic) IBOutlet UIView *cityBox;
@property (weak, nonatomic) IBOutlet UIView *updatePhotoBox;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) IBOutlet UIView *countryBox;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *boxView1Constrain;
@property (weak, nonatomic) IBOutlet UIView *_personalBoxView;
@property (weak, nonatomic) IBOutlet UIView *_locationBoxView;
- (IBAction)cameraBtnPressed:(id)sender;
- (IBAction)galleryBtnPressed:(id)sender;

- (IBAction)savePressed:(id)sender;
@end
