//
//  Constant.h
//  Ajo
//
//  Created by Samreen Noor on 09/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#pragma mark Screen Sizes
#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480)
#define IS_IPAD ([[UIScreen mainScreen] bounds].size.height == 1024)
#define IS_IPHONE_6Plus ([[UIScreen mainScreen] bounds].size.height == 736)
/*  ------------------- BASE URL ------------------- */

#if DEBUG
#define BASE_URL            @"http://app.ajoworldwide.com"   // Stagging
#else
#define BASE_URL            @"http://app.ajoworldwide.com"  // Production
/// old url   http://ajo.witsapplication.com
#endif

/* BASE URL END */

/*  ------------------ LOGIN ------------------ */

#define URI_SIGN_UP             @"/user/signup"
#define URI_SIGN_IN             @"/user/login"
#define URI_FORGET_PASSWORD     @"/user/forgot_password"
#define URL_EDIT_PROFILE        @"/User/edit_profile"
#define URL_SETTINGS            @"/user/save_setting"
#define URL_SAVE_POST           @"/Post/save"
#define URL_DELETE_POST          @"/Post/delete"
#define URL_UPDATE_REVIEWS       @"/Review/save_review"
#define URL_DELETE_REVIEWS       @"/Review/delete_review"
#define URL_UPLOAD_PICTURE       @"/Picture/save_picture"
#define URL_UPLOAD_NEWPLACE     @"/Places/add_place"

#define URL_DELETE_POSTED_PICTURE         @"/Picture/delete_picture"
#define URL_SEARCH                     @"/Places/search"
//#define URL_SEARCH_FROM_SERVER         @"/Places/search_from_server"
#define URL_SEARCH_FROM_SERVER         @"/Places/search_from_server2"
#define URL_PLACE_SEARCH         @"/Places/search_new2"

//#define URL_DISCOVER        @"/Places/discover"
#define URL_DISCOVER        @"/Places/nearby2"
#define URL_GET_FAV_PLACES        @"/Places/get_favourite"
#define URL_ADD_FAV_PLACES        @"/Places/make_favourite"

#define URL_RECOMMENDATION       @"/Places/recommendation"
//#define URL_PLACES_DETAIL       @"/Places/detail"
#define URL_PLACES_DETAIL       @"/Places/detail_new"

#define URL_RECOMMENDATION_DETAIL       @"/Places/detail_recomendation"

/*  LOGIN END */

// SIGN UP AND SIGN IN
#define KEY_NAME               @"name"
#define KEY_EMAIL                   @"email"
#define KEY_PASSWORD                @"password"
#define KEY_FIRST_NAME                @"first_name"
#define KEY_LAST_NAME                @"last_name"
#define KEY_USER_ID                @"user_id"


#define KEY_PROFILE_IMG                    @"profile_image"

// TAB BAR IMAGES

#define DISCOVER_iMG                  @"discover1.png"
#define DISCOVER_SELECTED_iMG                  @"discover_sel.png"
#define SETTINGS_iMG                  @"settings1.png"
#define SETTINGS_SELECTED_iMG                  @"settings_sel.png"
#define PROFILE_iMG                  @"profile1.png"
#define PROFILE_SELECTED_iMG                  @"profile_sel.png"
#define RECOMMENDATION_iMG                  @"recommendation-1.png"
#define RECOMMENDATION_SELECTED_iMG                  @"recommendation_sel.png"


#endif /* Constant_h */
