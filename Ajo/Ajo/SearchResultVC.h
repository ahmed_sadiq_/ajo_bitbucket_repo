//
//  SearchResultVC.h
//  Ajo
//
//  Created by Samreen on 03/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouriteBaseController.h"

@interface SearchResultVC : FavouriteBaseController

@property (strong, nonatomic) NSString *searchTitle;
@property (strong, nonatomic) NSString *location;
@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *subCategory;
@property (strong, nonatomic) NSString *keyword;

@property (strong, nonatomic) NSMutableArray *placesArray;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblSearch;

@end
