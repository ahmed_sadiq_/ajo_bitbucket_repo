//
//  SearchResultVC.m
//  Ajo
//
//  Created by Samreen on 03/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "SearchResultVC.h"
#import "PlaceCell.h"
#import "Places.h"
#import "Pictures.h"
#import "UIImageView+URL.h"
#import "PlaceService.h"
#import "HomeDetailVC.h"

@interface SearchResultVC ()<UITableViewDelegate,UITableViewDataSource>
{
    BOOL serverCall;

    int page;
}
@end

@implementation SearchResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _lblTitle.text = _searchTitle;
    page =1;
    serverCall=NO;

}

-(void)viewWillAppear:(BOOL)animated{
    self.favPlacesArray = _placesArray;

}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    NSArray *visibleRows = [_tblSearch visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [_tblSearch indexPathForCell:lastVisibleCell];
    NSLog(@"%ld",(long)path.row);
    if(path.section == 0 && path.row == (_placesArray.count)-1)
    {
        if (!serverCall) {
        
                
                [self searchApi];
                
            }
            
        }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    return _placesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Places *plac = [_placesArray objectAtIndex: indexPath.row];
    
    PlaceCell * cell = (PlaceCell *)[_tblSearch dequeueReusableCellWithIdentifier:@"PlaceCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"PlaceCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    Pictures *pic;
    if (plac.place_picture.count>0) {
        pic  = plac.place_picture[0];
        
    }
    [cell.placeImg setImageWithStringURL:pic.picture];
    cell.lblTitle.text = plac.placeName;
    cell.lblLocation.text = plac.placeAddress;
    cell.postCount.text = plac.postCount;
    cell.reviewCount.text = plac.reviewCount;
    cell.ratingView.canEdit = NO;
    cell.ratingView.rating = plac.rating;
    if ([plac.is_favourite isEqualToString:@"0"]) {
        //cell.favImg.image = [UIImage imageNamed:@"heart_unfilled"];
        [cell.btnFavourite setImage: [UIImage imageNamed:@"heart_unfilled"] forState:UIControlStateNormal];
        
    }
    else{
        //cell.favImg.image = [UIImage imageNamed:@"heart_filled"];
        [cell.btnFavourite setImage: [UIImage imageNamed:@"heart_filled"] forState:UIControlStateNormal];
        
    }
    cell.btnFavourite.tag = indexPath.row;
    cell.btnDetail.tag = indexPath.row;
    [cell.btnFavourite addTarget:self action:@selector(addPlaceToFav:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDetail addTarget:self action:@selector(btnDetailPressed:) forControlEvents:UIControlEventTouchUpInside];
    
   
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 137;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
}
-(void) searchApi{
    page++;
    NSString *pageNO = [NSString stringWithFormat:@"%d",page];
    [PlaceService searchPlacesWithUrl:URL_PLACE_SEARCH PageToken:pageNO Location:_location SearchKeyword:_keyword Category:_category SubCategory:_subCategory success:^(id data) {
        NSArray *tempPlace = data;
        int startIndex = (int)_placesArray.count;
        int dataCount = (int)tempPlace.count;

        for (Places *place in tempPlace) {
            [_placesArray addObject:place];
        }
        self.favPlacesArray = _placesArray;

        NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
        //int startIndex = (page-1) * 20;
        for (int i = startIndex ; i < (startIndex+dataCount); i++) {
            if(i<_placesArray.count) {
                [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        [_tblSearch beginUpdates];
        [_tblSearch insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [_tblSearch endUpdates];
        serverCall = NO;
        if (tempPlace.count==0) {
            serverCall = YES;

        }
        
        
    } failure:^(NSError *error) {
        
        [self showAlertMessage:error.localizedDescription];
    }];
}

#pragma mark - Cell Utility Methods
- (void) btnDetailPressed : (UIButton *) sender {
    
    
  NSInteger  currentSelectedIndex = sender.tag;
    
    Places *place = [_placesArray objectAtIndex: currentSelectedIndex];
    
    HomeDetailVC *homeDetailController = [[HomeDetailVC alloc] initWithNibName:@"HomeDetailVC" bundle:nil];
    homeDetailController.place = place;
    homeDetailController.isFromDiscoverVc = YES;
    homeDetailController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:homeDetailController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    
}
#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"AJO!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
