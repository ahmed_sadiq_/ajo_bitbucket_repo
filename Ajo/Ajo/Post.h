//
//  Post.h
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseEntity.h"

@interface Post : BaseEntity
@property (nonatomic, strong) NSString * post_id;
@property (nonatomic, strong) NSString * post_text;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * place_id;
@property (nonatomic, strong) NSString * user_profile_pic;


- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapPostFromArray:(NSArray *) arrlist;

@end
