//
//  UIButton+Style.m
//  Ajo
//
//  Created by Samreen on 07/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "UIButton+Style.h"

@implementation UIButton (Style)
- (void) roundCorner:(CGFloat) size
{
    self.layer.cornerRadius = size;
    self.layer.masksToBounds = NO;
    
    self.layer.shadowColor = [UIColor grayColor].CGColor;
    self.layer.shadowOpacity =  0.5f;
    self.layer.shadowRadius = 5.0f;
    self.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    
}
@end
