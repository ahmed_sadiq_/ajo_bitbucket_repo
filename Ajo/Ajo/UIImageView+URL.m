//
//  UIImageView+URL.m
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "UIImageView+URL.h"
#import "UIImageView+WebCache.h"

@implementation UIImageView (URL)
- (void) setImageWithStringURL:(NSString *) strURL
{if([strURL isEqualToString:@""]||strURL== nil){

    
    [self sd_setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:[UIImage imageNamed:@"unavailable"]];
}
else{
    
    [self sd_setImageWithURL:[NSURL URLWithString:strURL] placeholderImage:[UIImage imageNamed:@"loading"]];
}
}

@end
