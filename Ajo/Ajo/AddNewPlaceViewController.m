//
//  AddNewPlaceViewController.m
//  Ajo
//
//  Created by Samreen Noor on 11/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "AddNewPlaceViewController.h"
#import "MKDropdownMenu.h"
#import "LocationManager.h"
#import "MenuCell.h"
#import "PlacesSubMenu.h"
#import "MainMenu.h"
#import "CountryPicker.h"
#import "ELCAlbumPickerController.h"
#import "ELCImagePickerController.h"
#import "ELCAssetTablePicker.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "PlaceService.h"
#import "UIAlertView+JS.h"
#import "DataManager.h"
#import "SVProgressHUD.h"
#import "LocationManager.h"
#import "Category.h"


@interface AddNewPlaceViewController ()<MKDropdownMenuDataSource, MKDropdownMenuDelegate,CountryPickerDelegate,ELCImagePickerControllerDelegate>
{
    UITextView *currentTextView;
    UITextField *currentField;
    NSArray *catArray;
    NSMutableArray *cityArray;
    NSMutableArray *photoArray;
    BOOL isResponce;
    MainMenu *m;
    PlacesSubMenu *subMenu;
    id countryrespose;
    CLLocationCoordinate2D coordinate;
    NSString *lat;
    NSString *logi;
    NSString *selectedCity;
    NSString *selectedCategory;
    NSString *selectedSubCategory;
    Category *selectedCat;
}
@end

@implementation AddNewPlaceViewController


-(void) mapCategoryData:(NSArray *)data{
    catArray = [Category mapCategoryFromArray:data];
}

-(void)setUpView{
    catArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Categories" ofType:@"plist"]];
    [self mapCategoryData:catArray];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    selectedCity=@"";
    selectedCategory =@"";
    selectedSubCategory =@"";
    photoArray = [[NSMutableArray alloc]init];
    cityArray = [[NSMutableArray alloc]init];
    [self setUpView];

    self.countryPickerView.delegate = self;
    self.countryPickerView.dataSource = self;
    self.cityPickerView.delegate = self;
    self.cityPickerView.dataSource = self;
    self.categoryDropDown.delegate = self;
    self.categoryDropDown.dataSource = self;
    self.subCategoryDropDown.delegate = self;
    self.subCategoryDropDown.dataSource = self;
    self.btnAddPlace.layer.masksToBounds = NO;
    self.btnAddPlace.layer.shadowOffset = CGSizeMake(-3, 1);
    self.btnAddPlace.layer.shadowRadius = 3;
    self.btnAddPlace.layer.shadowOpacity = 0.4;
    self.btnChoosePhoto.layer.masksToBounds = NO;
    self.btnChoosePhoto.layer.shadowOffset = CGSizeMake(-3, 1);
    self.btnChoosePhoto.layer.shadowRadius = 3;
    self.btnChoosePhoto.layer.shadowOpacity = 0.4;

}

-(void)viewWillAppear:(BOOL)animated{
    isResponce=NO;
        [self getcity];



}
- (IBAction)btnShowCountryPicker:(id)sender {
    [currentField resignFirstResponder];
    [currentTextView resignFirstResponder];

    _pickerView.hidden=NO;
    [self hideDropDown];
}
- (IBAction)btnHideKeyboard:(id)sender {
    if (isResponce) {
        _pickerView.hidden = YES;
        
    }
    [currentTextView resignFirstResponder];

    [currentField resignFirstResponder];
    _tblPlaces.hidden= YES;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"hidePickerView"
     object:nil];
}
-(void) hideDropDown{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"hidePickerView"
     object:nil];
}

- (IBAction)btnHidePicker:(id)sender {
    if (!isResponce) {
        [SVProgressHUD show];
  
    }
    _pickerView.hidden = YES;

}
- (IBAction)btnChoosephoto:(id)sender {
    
    
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 100; //Set the maximum number of images to select to 100
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
    
    elcPicker.imagePickerDelegate = self;

        [self presentViewController:elcPicker animated:YES completion:nil];
        
    
    

    
}
- (IBAction)btnBack:(id)sender {

    [self.navigationController popViewControllerAnimated:true];

}
- (IBAction)btnAddPlace:(id)sender {
    [self newPlaceAdd];
}
////////   ################### /////////////////
#pragma mark - MKDropdownMenuDataSource

- (NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    if ([dropdownMenu isEqual:_countryPickerView]) {
        return 0;
    }
    else if ([dropdownMenu isEqual:_cityPickerView]){
        return cityArray.count;
    }
     else if ([dropdownMenu isEqual:_categoryDropDown]){
            return catArray.count;;
     }
     else{
         if (![selectedCategory isEqualToString:@""]) {
             return selectedCat.subCategory.count;
         }
    else
       return 0;
     }
    
}

#pragma mark - MKDropdownMenuDelegate

- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
    //    if (component == DropdownComponentCity) {
    //        return 20;
    //    }
    return 0; // use default row height
}

- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu widthForComponent:(NSInteger)component {
        return MAX(dropdownMenu.bounds.size.width/3, 125);
}

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    [currentField resignFirstResponder];
    [currentTextView resignFirstResponder];
    self.pickerView.hidden = YES;

    if ([dropdownMenu isEqual:_countryPickerView]) {
        _pickerView.hidden=NO;
        return YES;
    }
   else if ([dropdownMenu isEqual:_cityPickerView]&&cityArray.count==0) {
       
       
       UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"please select a country!" delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
       [alertView show];
       
       
       [alertView showWithBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
           
           if (buttonIndex == 0) {
               [[NSNotificationCenter defaultCenter]
                postNotificationName:@"hidePickerView"
                object:nil];
           }
       }] ;

        return YES;
    }
    
    return NO;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
               return [[NSAttributedString alloc] initWithString:@""
                                                   attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightLight],
                                                                NSForegroundColorAttributeName: self.view.tintColor}];
    
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForSelectedComponent:(NSInteger)component {
                return [[NSAttributedString alloc] initWithString:@""
                                                   attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightRegular],
                                                                NSForegroundColorAttributeName: self.view.tintColor}];
    
    
    
    
}

- (NSString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if ([dropdownMenu isEqual:_countryPickerView]) {
        return @"";

    }
    else if ([dropdownMenu isEqual:_cityPickerView]){
        return cityArray[row];
    }
    else if ([dropdownMenu isEqual:_categoryDropDown]){
        Category *cat =catArray[row];
    return cat.categoryName;
    }
    else{
        if (![selectedCategory isEqualToString:@""]) {
            return selectedCat.subCategory[row];
        }
    
        return @"";
    }
   
    
    
}
- (UIColor *)dropdownMenu:(MKDropdownMenu *)dropdownMenu backgroundColorForRow:(NSInteger)row forComponent:(NSInteger)component {
    //    if (component == DropdownComponentCity) {
    //        return [self colorForRow:row];
    //    }
    return nil;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
     if ([dropdownMenu isEqual:_cityPickerView]){
         _tfCity.text = cityArray[row];
         selectedCity= cityArray[row];
     }
     else if ([dropdownMenu isEqual:_categoryDropDown]){
         Category *cat = catArray[row];
         selectedCategory = cat.categoryName;
         _tfSelectCateory.text = selectedCategory;
         selectedCat = cat;
         [_subCategoryDropDown reloadAllComponents];
     }
     else if ([dropdownMenu isEqual:_subCategoryDropDown]){
         selectedSubCategory = selectedCat.subCategory[row];
         _tfSubCategory.text = selectedSubCategory;
     }

    
}



#pragma mark - LOcation GET Method

-(void) getLatLongWithPlanceID:(NSString *)placeId
{
    
    NSString *str3=[[NSString alloc] initWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=AIzaSyCAd8TOUYgbF2nBggLgkmJd3NT8hJPrals",placeId  ];//& usedin url string to  concatination
    
    NSURL * url  = [NSURL URLWithString:str3];
    NSURLRequest * resquest = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:resquest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               id respose = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                               
                               NSLog(@"%@",respose);
                               [self mappPlaceDetail:respose];
                               
                               
                               
                           }];
    
    
}


-(void) loadDataFromNetwork:(NSString *)city

{
    NSString *cityString = [city stringByReplacingOccurrencesOfString:@" " withString:@""];
    float lati =[LocationManager sharedManager].currentLat;
    float longi =[LocationManager sharedManager].currentLong;
    
    NSString *str3=[[NSString alloc] initWithFormat:@"https://maps.googleapis.com/maps/api/place/queryautocomplete/json?input=%@&location=%f,%f&radius=500&key=AIzaSyCAd8TOUYgbF2nBggLgkmJd3NT8hJPrals",cityString ,lati,longi  ];//& usedin url string to  concatination
    
    NSURL * url  = [NSURL URLWithString:str3];
    
    NSURLRequest *resquest  = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:resquest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (![data isEqual:nil]) {
                                   
                                   id respose = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                   
                                   NSLog(@"%@",respose);
                                   
                                   [self mappData:respose];
                                   
                               }
                               
                               
                           }];
    
}

-(void) getcity{

    
    NSString *str3=[[NSString alloc] initWithFormat:@"https://raw.githubusercontent.com/David-Haim/CountriesToCitiesJSON/master/countriesToCities.json"];//& usedin url string to  concatination
    
    NSURL * url  = [NSURL URLWithString:str3];
    
    NSURLRequest *resquest  = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:resquest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (![data isEqual:nil]) {
                                   
                                   countryrespose = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                   isResponce=YES;
                                   [SVProgressHUD dismiss];
                                   if ([_tfCountry.text length]>0) {
                                       cityArray =countryrespose[[NSString stringWithFormat:@"%@",_tfCountry.text]];
                                       [_cityPickerView reloadAllComponents];
                                   }
                                   [DataManager sharedManager].countryResponse = countryrespose;
                                   
                               }
                               
                               
                           }];
    
}



#pragma mark - Text VIEW Delegate Methods

-(BOOL) checkFileds{
    
    if ([_tfName.text length]>=1&&[_tfSelectCateory.text length]>=1&&[selectedCity length]>=1&&[_tfCountry.text length]>=1&&[_tfEmail.text length]==0) {
        return true;
    }
    if ([_tfName.text length]>=1&&[_tfSelectCateory.text length]>=1&&[selectedCity length]>=1&&[_tfCountry.text length]>=1&&[_tfEmail.text length]>1) {
        if ([self emailValidate:_tfEmail.text]) {
            return true;

        }
        else{
            [self showMessage:@"please enter valid email"];

            return false;

        }
    }
    else if([_tfName.text length]==0){
        [self showMessage:@"please enter place name"];
        return false;
 
    }
    else if([_tfSelectCateory.text length]==0){
        [self showMessage:@"please select a Category"];
        return false;
 
    } else if([_tfCountry.text length]==0){
        [self showMessage:@"please select a country"];
        return false;
  
    } else if([selectedCity length]==0){
        [self showMessage:@"please select a city"];
        return false;
  
    }
    
    
    return false;

}
-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    currentTextView=textView;
    _pickerView.hidden = YES;
    [self hideDropDown];

    [self animateTextField:nil up:YES];
    
    
}
-(void)textViewDidChange:(UITextView *)textView
{
    
    
}
-(void)textViewDidChangeSelection:(UITextView *)textView
{
    
    /*YOUR CODE HERE*/
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    
    [self animateTextField:nil up:NO];
    
    /*YOUR CODE HERE*/
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}


#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _pickerView.hidden = YES;
    [self hideDropDown];
    currentField = textField;
    if (textField.tag==1) {
        [self animateTextField:nil up:YES];

    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{    if (textField.tag==1) {

    [self animateTextField:nil up:NO];
}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([_tfName isEqual:textField]) {
        if (![searchStr isEqualToString:@""]) {
            [self loadDataFromNetwork:_tfName.text];
            
        }
        else{
            
            
            // [m.menuArray removeAllObjects];
            //[_tblView reloadData];
            _tblPlaces.hidden = YES;
            
        }
    }
    
 else   if([textField isEqual:_tfPhone]){
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++) {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c]) {
                return NO;
            }
        }
    }
    return YES;
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}




#pragma mark -tableview Methods



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 39;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return m.menuArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    MenuCell * cell = (MenuCell *)[_tblPlaces dequeueReusableCellWithIdentifier:@"MenuCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MenuCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    MainMenu *menu=[m.menuArray objectAtIndex:indexPath.row];
    [cell.placeName setText:[NSString stringWithFormat:@"%@",menu.descipt]];
    
    
    
    //  [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenu *menu=[m.menuArray objectAtIndex:indexPath.row];
    _tfName.text = menu.descipt;
    _tblPlaces.hidden = YES;
    //_btnsave.enabled = YES;
    
    [self getLatLongWithPlanceID:menu.placeId];
}

-(void) mappData:(NSDictionary *) value{
    
    
    
    m =    [[MainMenu alloc]initWithDict:value];
    
    
    _tblPlaces.hidden = NO;
    [_tblPlaces reloadData];
}



-(void) mappPlaceDetail:(NSDictionary *) value{
    
    
    
    
    subMenu =    [[PlacesSubMenu alloc]initWithDict:value];
    
    NSLog(@"%@",value);
    
}

- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    _tfCountry.text = name;
   _tfCity.text =  @"Select City";
    selectedCity=@"";
    cityArray =countryrespose[[NSString stringWithFormat:@"%@",name]];
    [_cityPickerView reloadAllComponents];
}



#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    for (UIView *v in [_photoScroller subviews]) {
        [v removeFromSuperview];
    }
    
    CGRect workingFrame = _photoScroller.frame;
    workingFrame.origin.x = 0;
    
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                [images addObject:image];
                
                UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
                [imageview setContentMode:UIViewContentModeScaleAspectFit];
                imageview.frame = workingFrame;
                
                // [_scrrollView addSubview:imageview];
                
                workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                
                [images addObject:image];
                
                UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
                [imageview setContentMode:UIViewContentModeScaleAspectFit];
                imageview.frame = workingFrame;
                
                // [_scrrollView addSubview:imageview];
                
                workingFrame.origin.x = workingFrame.origin.x + workingFrame.size.width;
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        }
        else {
            NSLog(@"Uknown asset type");
        }
    }
    
    photoArray = images;
    
    [_photoScroller setPagingEnabled:YES];
    [self setUpScrollView];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void) setUpScrollView{
    int scrollWidth = 100;
    //scrollVie.contentSize = CGSizeMake(scrollWidth,100);
  //  _photoScroller.frame=CGRectMake(0, 51, self.view.frame.size.width, 200);
    
    // _scrrollView.backgroundColor=[UIColor redColor];
    
    for (UIView *v in [_photoScroller subviews]) {
        [v removeFromSuperview];
    }
    
    
    int xOffset = 0;
    
    for(int index=0; index < [photoArray count]; index++)
    {
        
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(xOffset,10,160, 200)];
        img.image = (UIImage*) [photoArray objectAtIndex:index];
        
        [_photoScroller addSubview:img];
        
        UIButton *addProject = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        addProject.frame = CGRectMake(xOffset,10,160, 200);
        [addProject setTitle:@"" forState:UIControlStateNormal];
        //addProject.tag = index;
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(deleteLongPhotoPressed:)];
        lpgr.minimumPressDuration = 1.0;
        [addProject addGestureRecognizer:lpgr];
        [lpgr.view setTag:index];
        [_photoScroller addSubview:addProject];

        xOffset+=170;
        
        
    }
    if (photoArray.count>0) {
        
    
    _photoScroller.contentSize = CGSizeMake(scrollWidth+xOffset,200);
    _photoScrollHeight.constant = 200;
    _photoScroller.hidden = NO;
    _mainScrollHeightConstrain.constant = 1270;
    }
    else{
        _mainScrollHeightConstrain.constant = 1090;

        _photoScrollHeight.constant = 20;
        _photoScroller.hidden = YES;

    }
}

-(void) showMessage:(NSString *)msg{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
    [alertView show];
    
}

-(void)deleteLongPhotoPressed : (UILongPressGestureRecognizer* ) sender{
    if (sender.state == UIGestureRecognizerStateEnded) {
        NSLog(@"UIGestureRecognizerStateEnded");
        NSInteger tag = sender.view.tag;
        //UIView *view =[sender.view superview];
        NSUInteger currentSelectedIndex = tag;
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Wait!" message:@"Do you want to delete this photo?" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
        
        
        [alert showWithBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            
            if (buttonIndex == 1) {
                
                if (photoArray.count>0) {
                    [photoArray removeObjectAtIndex:currentSelectedIndex];
                    
                    [self setUpScrollView];
                    
                }
                
                
            }
            else{
                
            }
        }] ;
    }
    else if (sender.state == UIGestureRecognizerStateBegan){
        NSLog(@"UIGestureRecognizerStateBegan.");
        //Do Whatever You want on Began of Gesture
    }
    
    


  
}

#pragma mark - api
-(void) newPlaceAdd{
    if (subMenu.lat!=nil && ![subMenu.lat isEqualToString:@""]) {
    lat= subMenu.lat;
       logi= subMenu.lng;
    }
  
    if ([self checkFileds]) {
        [PlaceService addNewPlaceWithPlaceName:_tfName.text Description:_tfDescrption.text latitude:lat Longitude:logi Type:selectedCategory SubCatgory:selectedSubCategory Website:_tfWebsite.text Country:_tfCountry.text City:_tfCity.text PhotoArray:photoArray Email:_tfEmail.text Address:_tfAddress.text Phone:_tfPhone.text success:^(id data) {
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Place added Successfully" delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:nil, nil];
            [alertView show];
            
            
            [alertView showWithBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                
                if (buttonIndex == 0) {
                    [self.navigationController popViewControllerAnimated:true];

                }
            }] ;

            
        } failure:^(NSError *error) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:@"Use Current Location", nil];
            [alertView show];
            
            
            [alertView showWithBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                
                if (buttonIndex == 1) {
                                      [self getCurrentUserLocation];
                }
            }] ;
            
        }];
    }
  

  
}

-(void) getCurrentUserLocation{
    [[LocationManager sharedManager] getUserCurrentLocationWithCompletionBlock:^(CLLocationCoordinate2D location) {
        
        coordinate = location;
        [LocationManager sharedManager].currentLat = location.latitude ;
        [LocationManager sharedManager].currentLong  = location.longitude ;
        lat=  [NSString stringWithFormat:@"%f", [LocationManager sharedManager].currentLat] ;
        logi= [NSString stringWithFormat:@"%f",[LocationManager sharedManager].currentLong ];

        [self newPlaceAdd];
        
        
        
    } errorBlock:^(NSError *error) {
        
        
        NSLog(@"%@",error.localizedDescription);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
