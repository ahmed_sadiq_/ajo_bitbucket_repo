//
//  DataManager.h
//  LawNote
//
//  Created by Samreen Noor on 22/07/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface DataManager : NSObject
@property (strong, nonatomic) User *currentUser;
@property (strong, nonatomic) NSString *selectedCtaegory ;
@property (strong, nonatomic) id countryResponse;
@property (assign, nonatomic) NSInteger selectedComponent; // NSNotFound when no components are selected
 // NSNotFound when no components are selected
@property (strong, nonatomic) NSString *discoverPageToken;
@property (strong, nonatomic) NSMutableArray *favouritePlaces;
@property (strong, nonatomic) NSMutableArray *nearBYPlaces;

@property (strong, nonatomic) NSString *authToken;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) NSMutableArray *categoryArray;
@property (strong, nonatomic) NSMutableArray *catItemArray;
@property (strong, nonatomic) NSString *postObj;
@property ( nonatomic) BOOL updateSettings;
@property ( nonatomic) BOOL isUpdatedRecomnd;
@property ( nonatomic) BOOL isUpdatedDiscover;

@property ( nonatomic ) BOOL *isFromSignup;
@property ( nonatomic ) BOOL *isFromFacebook;

+ (DataManager *) sharedManager;

@end
