//
//  HomeDetailVC.h
//  Ajo
//
//  Created by Ahmed Sadiq on 15/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASStarRatingView.h"
#import "Places.h"
#import "AddNewPost.h"
#import "AJOLabel.h"
#import "PEARImageSlideViewController.h"

@interface HomeDetailVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *plusBox;
@property (weak, nonatomic) IBOutlet UILabel *lblReviewTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPostTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPhotoTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScroller;
@property (weak, nonatomic) IBOutlet UIPageControl *pager;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionTitleViewHeight;
@property (weak, nonatomic) IBOutlet UIView *desView;
@property (weak, nonatomic) IBOutlet UIView *aboUtView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *aboutTitleViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *placeImg;
@property (weak, nonatomic) IBOutlet UILabel *placeName;
@property (weak, nonatomic) IBOutlet UILabel *photoCount;
@property (weak, nonatomic) IBOutlet UILabel *postCount;
@property (weak, nonatomic) IBOutlet UILabel *reviewCount;
@property (weak, nonatomic) IBOutlet UIView *DeleteView;
@property (weak, nonatomic) IBOutlet UIView *deletSubView;
@property (nonatomic, assign) BOOL isFromDiscoverVc;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteSpecificPhoto;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *placesDetailViewHeight;

@property (weak, nonatomic) IBOutlet UIButton *btnDeletePost;
@property (strong, nonatomic) Places *place;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNewPost;
@property (weak, nonatomic) IBOutlet AJOLabel *detailViewTitle;
@property (strong, nonatomic) NSMutableArray *postDataSource;
@property (strong, nonatomic) NSMutableArray *filteredArray;
@property (strong, nonatomic) IBOutlet UITableView *mainTblView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *websiteViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *phoneViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *emailViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionHeightContrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressHeightConstrain;
@property (weak, nonatomic) IBOutlet UIView *placeDetilShowView;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblplacephone;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceWebsite;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceAddress;

@property (weak, nonatomic) IBOutlet UIView *showPhotoView;
@property (weak, nonatomic) IBOutlet UIScrollView *photoScrollView;
@property (weak, nonatomic) IBOutlet UIView *menuBgImgView;
@property (weak, nonatomic) IBOutlet UIView *menuInfoBgImgView;
@property (nonatomic, assign) BOOL isFromNewPost;
@property (weak, nonatomic) IBOutlet UILabel *lblEmptyPostTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnEmptyPost;

@property (weak, nonatomic) IBOutlet UIView *emptyPostView;
@property (strong, nonatomic) IBOutlet ASStarRatingView *staticStarRatingView;


- (IBAction)backPressed:(id)sender;

- (IBAction)tab1Pressed:(id)sender;
- (IBAction)tab2Pressed:(id)sender;
- (IBAction)tab3Pressed:(id)sender;
@end
