//
//  JSFBSdkService.h
//  Ajo
//
//  Created by Samreen Noor on 08/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSFbConstants.h"

typedef void (^loadKitSuccess)(id data);
typedef void (^loadKitFailure)(NSError *error);

@interface JSFBSdkService : NSObject


-(void) getUserInfoWithcompleted:(loadKitSuccess)completed
                          failed:(loadKitFailure)failed;

-(void) postMessageOnWall:(NSString *) message
                completed:(loadKitSuccess) completed
                   failed:(loadKitFailure) failed;

@end
