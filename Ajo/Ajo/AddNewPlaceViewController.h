//
//  AddNewPlaceViewController.h
//  Ajo
//
//  Created by Samreen Noor on 11/11/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKDropdownMenu.h"
@interface AddNewPlaceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *pickerView;
@property (weak, nonatomic) IBOutlet UITextField *tfName;

@property (weak, nonatomic) IBOutlet UITextField *tfCountry;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *countryPickerView;
@property (weak, nonatomic) IBOutlet UITextField *tfCity;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *cityPickerView;
@property (weak, nonatomic) IBOutlet UITextView *tfAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfType;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPlace;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *typePickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnChoosePhoto;
@property (weak, nonatomic) IBOutlet UITextField *tfWebsite;
@property (weak, nonatomic) IBOutlet UITableView *tblPlaces;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainScrollHeightConstrain;
@property (weak, nonatomic) IBOutlet UIScrollView *photoScroller;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *subCategoryDropDown;
@property (weak, nonatomic) IBOutlet UITextField *tfSubCategory;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoScrollHeight;
@property (weak, nonatomic) IBOutlet UITextField *tfPhone;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *categoryDropDown;
@property (weak, nonatomic) IBOutlet UITextField *tfSelectCateory;

@property (weak, nonatomic) IBOutlet UIView *cityBox;
@property (weak, nonatomic) IBOutlet UITextView *tfDescrption;
@property (weak, nonatomic) IBOutlet UIView *subCategoryBox;
@property (weak, nonatomic) IBOutlet UIView *countryBox;

@property (weak, nonatomic) IBOutlet UIView *categoryBox;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@end
