//
//  PlacesCollectionCell.m
//  Ajo
//
//  Created by Samreen on 31/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "PlacesCollectionCell.h"

@implementation PlacesCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.boxView.layer.cornerRadius = 5;
    self.boxView.layer.masksToBounds = YES;}

@end
