//
//  Pictures.h
//  Ajo
//
//  Created by Samreen Noor on 30/09/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "BaseEntity.h"

@interface Pictures : BaseEntity
@property (nonatomic, strong) NSString * date_time;
@property (nonatomic, strong) NSString * mainPicture_text;
@property (nonatomic, strong) NSString * place_id;
@property (nonatomic, strong) NSString * post_picture_id;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * user_name;
@property (nonatomic, strong) NSString * user_profile_pic;




@property (nonatomic, strong) NSString * picture;
@property (nonatomic, strong) NSString * picture_id;
@property (nonatomic, strong) NSArray * picturesArray;


- (id)initWithDictionary:(NSDictionary *) responseData;

+(NSArray *) mapPicturesFromArray:(NSArray *) arrlist;
-(NSArray *) mapArrayOfPics:(NSArray *)picsArray;

@end
