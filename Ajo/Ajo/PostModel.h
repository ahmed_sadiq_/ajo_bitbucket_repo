//
//  PostModel.h
//  Ajo
//
//  Created by Ahmed Sadiq on 16/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"
#import "Reviews.h"
#import "Pictures.h"
typedef enum
{
    review = 1,
    post,
    photo
} PostType;

@interface PostModel : BaseEntity
@property (strong, nonatomic) NSString *postHolderName;
@property (strong, nonatomic) NSString *postTime;
@property (strong, nonatomic) NSString *postDescription;

@property int postTyper;
@property (assign, nonatomic) PostType postType;
//////// ####### review #########

@property (nonatomic, strong) NSString * review_id;
@property  float  rating;
@property (nonatomic, strong) NSString * reviewtext;
@property (nonatomic, strong) NSString * reviewUser_id;
@property (nonatomic, strong) NSString * reviewUser_name;

@property (nonatomic, strong) NSString * reviewPlace_id;
@property (nonatomic, strong) NSString * reviewUser_profile_pic;
@property (nonatomic, strong) NSString * reviewdate_time;

///////////// ###### post #########
@property (nonatomic, strong) NSString * postdate_time;
@property (nonatomic, strong) NSString * postUser_name;

@property (nonatomic, strong) NSString * post_id;
@property (nonatomic, strong) NSString * post_text;
@property (nonatomic, strong) NSString * Postuser_id;
@property (nonatomic, strong) NSString * postPlace_id;
@property (nonatomic, strong) NSString * postUser_profile_pic;
///////////// ###### PICTURES ######
@property (nonatomic, strong) NSString * date_time;
@property (nonatomic, strong) NSString * mainPicture_text;
@property (nonatomic, strong) NSString * picturePlace_id;
@property (nonatomic, strong) NSString * post_picture_id;
@property (nonatomic, strong) NSString * pictureUser_id;
@property (nonatomic, strong) NSString * pictureUser_name;
@property (nonatomic, strong) NSString * pictureUser_profile_pic;




@property (nonatomic, strong) NSString * picture;
@property (nonatomic, strong) NSString * picture_id;
@property (nonatomic, strong) NSMutableArray * picturesArray;
- (id)initWithDictionaryReviews:(NSDictionary *) responseData;
- (id)initWithDictionaryPost:(NSDictionary *) responseData;
- (id)initWithDictionaryPics:(NSDictionary *) responseData;


@end
