//
//  ProfileVC.m
//  Ajo
//
//  Created by Ahmed Sadiq on 12/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import "ProfileVC.h"
#import "UIImageView+RoundImage.h"
#import "AccountService.h"
#import "DataManager.h"
#import "User.h"
#import "UIImageView+URL.h"
#import "UIImage+JS.h"
#import "LocationManager.h"
#import "MenuCell.h"
#import "MainMenu.h"
#import "PlacesSubMenu.h"
#import "Constant.h"
#import "CountryPicker.h"
#import "SVProgressHUD.h"
#import "LocationManager.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "UIAlertView+JS.h"
#import "DrawerVC.h"
#import "Constant.h"

@interface ProfileVC ()<CountryPickerDelegate>
{
    NSArray *dictRoot;
    NSString *userId;
    UITextField *currentField;
    CLLocationCoordinate2D coordinate;
    NSMutableArray *menArray;
    MainMenu *m;
    PlacesSubMenu *subMenu;
    NSMutableArray *countryArray;
    NSMutableArray *cityArray;
    NSString *distance;
    User *currentUser;
    NSString *selectedCountry;
    NSString *selectedCity;
    NSString *selectedproximity;
    id countryrespose;
    NSArray *proxiArray;
    BOOL isResponce;
    NSString *lat;
    NSString *logi;
    NSString *changeStr;


}
@end

@implementation ProfileVC

-(void)setUpDropDownData{
    dictRoot = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CountryList" ofType:@"plist"]];
    NSLog(@"%@",dictRoot);
    for (NSDictionary *D in dictRoot) {
        NSString *cNmae= [D valueForKey:@"country_name"];;
        [countryArray addObject:cNmae];
//        NSArray *cArray =[D valueForKey:@"cityArray"];;
//        for (NSString *city in cArray) {
//            [cityArray addObject:city];
//            
//        }
    }

}
-(void) resetCity{
    for (NSDictionary *D in dictRoot) {
        NSString *countryNmae= [D valueForKey:@"country_name"];;
        if ([countryNmae isEqualToString:selectedCountry])
        {
            NSArray *cArray =[D valueForKey:@"cityArray"];
            [cityArray removeAllObjects];
            for (NSString *city in cArray) {
                [cityArray addObject:city];
                
            }
            break;
        }
        
    }

}
-(void) setupAdressTextView{
    
    float centr =  self.tfAddress.frame.size.height/2;
    CGFloat topCorrect = ([self.tfAddress bounds].size.height - [self.tfAddress contentSize].height * [self.tfAddress zoomScale])/2.0;
    
    // self.tfAddress.textContainerInset = UIEdgeInsetsMake(0, 28, 5, 5);
    
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
   
    self.tfAddress.contentOffset = (CGPoint){ .x = 0, .y = - topCorrect};
    

}
-(void) setUpProfileView{
    User *user=[DataManager sharedManager].currentUser;
    _lblUserName.text = user.name;
    [_profilePic setImageWithStringURL:user.profilePicture];
    [_nameTxt setAutocapitalizationType:UITextAutocapitalizationTypeWords];

    _nameTxt.text = user.name;
    _emailTxt.text = user.email;
    _tfPhone.text = user.phone_number;
    _tfAddress.text = user.address;
    _tfProximity.text = [NSString stringWithFormat:@"%@ miles",user.proximity];
    selectedproximity = user.proximity;
    _tfLocation.text =  user.location;
    changeStr=_tfLocation.text;
    if ([user.location isEqualToString:@""]||user.location==nil) {
       _tfLocation.text =[[NSUserDefaults standardUserDefaults] objectForKey:@"location"];
        changeStr =_tfLocation.text;
    }
selectedCity = @"";
    selectedCountry = @"";
    if (![user.country isEqualToString:@""]) {
        _lblCountry.text = user.country;
        selectedCountry = user.country;
        [self resetCity];
        [_cityDropDown reloadAllComponents];


    }
    if (![user.city isEqualToString:@""]) {
        _lblCity.text = user.city;
        selectedCity = user.city;

        
    }
   // _lblCity.text = user.city;
    userId = user.userID;

    
    
    //########   country and city box view ########
    self.countryBox.layer.cornerRadius=5.0f;
    self.countryBox.layer.masksToBounds=YES;
    self.countryBox.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.countryBox.layer.borderWidth= 1.0f;
    self.cityBox.layer.cornerRadius=5.0f;
    self.cityBox.layer.masksToBounds=YES;
    self.cityBox.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.cityBox.layer.borderWidth= 1.0f;

    
    self.btnSave.layer.masksToBounds = NO;
    self.btnSave.layer.shadowOffset = CGSizeMake(-3, 1);
    self.btnSave.layer.shadowRadius = 3;
    self.btnSave.layer.shadowOpacity = 0.4;
    self.headerView.layer.shadowOffset = CGSizeMake(-2, 1);
    self.headerView.layer.shadowRadius = 3;
    self.headerView.layer.shadowOpacity = 0.5;

    [self setupAdressTextView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    countryArray =[[NSMutableArray alloc]init];
    cityArray =[[NSMutableArray alloc]init];
    currentUser  =[DataManager sharedManager].currentUser;
    isResponce = NO;
        [self getcity];

    // Do any additional setup after loading the view from its nib.
    [_profilePic roundImageCorner];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];

    [singleTap setNumberOfTapsRequired:1];
     [self._personalBoxView addGestureRecognizer:singleTap];
    [self._locationBoxView addGestureRecognizer:singleTap];
    proxiArray = [NSArray arrayWithObjects: @"5 miles",@"10 miles",@"20 miles",@"50 miles",@"75 miles",@"100 miles",nil];
    
   self.dropDownMneu.delegate = self;
    self.dropDownMneu.dataSource =self;
    self.cityDropDown.delegate = self;
    self.cityDropDown.dataSource = self;
    self.proxiMneu.delegate = self;
    self.proxiMneu.dataSource = self;
    //[self setUpDropDownData];
    [self setUpProfileView];

}

-(void)viewWillAppear:(BOOL)animated{
  
    _boxView2Constrain.constant = 0;

}
-(void)viewDidLayoutSubviews{
    self.automaticallyAdjustsScrollViewInsets = false;
    _scrollView.contentInset = UIEdgeInsetsZero;
    _scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 920)];
    if (IS_IPHONE_6Plus) {
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 920)];

    }
    
    if (IS_IPHONE_5) {
        _personalLabelLeftConstrain.constant = 110;
        _locationLabelLeftConstrain.constant = 100;
    }
    [self setupAdressTextView];

}


-(void) hideDropDown{

    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"hidePickerView"
     object:nil];
}
#pragma mark - Navigation

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Text VIEW Delegate Methods
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.tabBarController.tabBar setHidden:NO];

    _updatePhotoBox.hidden = YES;

    _contryPicker.hidden = YES;
    [self hideDropDown];


    [self animateTextField:nil up:YES];
    
 
}
-(void)textViewDidChange:(UITextView *)textView
{

    
}
-(void)textViewDidChangeSelection:(UITextView *)textView
{
    
    /*YOUR CODE HERE*/
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
  
        
    [self animateTextField:nil up:NO];

    /*YOUR CODE HERE*/
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if ([textView isEqual:_tfAddress]) {
            [self setupAdressTextView];
            
        }
        return NO;
    }
    
    return YES;
}

#pragma mark - Text Field Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if ([textField isEqual:_tfLocation]) {
//        changeStr=textField.text;
//    }
    
    _contryPicker.hidden = YES;


    currentField = textField;
    [self animateTextField:nil up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:nil up:NO];
  
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
   
    
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([_tfLocation isEqual:textField]) {
        if (![searchStr isEqualToString:@""]) {
            [self loadDataFromNetwork:_tfLocation.text];
          lat=@"";
            logi =@"";
        }
        else{
            
            
            // [m.menuArray removeAllObjects];
            //[_tblView reloadData];
            _tblView.hidden = YES;
            
        }
    }
    return YES;
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 145; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    _lblCountry.text = name;
    selectedCountry=name;
    selectedCity = @"";
    _lblCity.text = @"Select City";
    if (countryrespose!=nil) {
        cityArray =countryrespose[[NSString stringWithFormat:@"%@",name]];
        [_cityDropDown reloadAllComponents];

    }
  }

#pragma mark -  UIButton Action

- (IBAction)btnMenuClicked:(id)sender {
    [[DrawerVC getInstance] AddInView:self];
    [[DrawerVC getInstance] ShowInView];
    
    _updatePhotoBox.hidden = YES;
    [currentField resignFirstResponder];
    _contryPicker.hidden = YES;
    [self hideDropDown];

    
}

- (IBAction)btnShowPhotoUpdateView:(id)sender {
    [self.tabBarController.tabBar setHidden:YES];

    _updatePhotoBox.hidden = NO;
    [currentField resignFirstResponder];
    _contryPicker.hidden = YES;
    [self hideDropDown];


}


- (IBAction)btnSelectCountry:(id)sender {
    [self hideDropDown];
    _updatePhotoBox.hidden = YES;

    [currentField resignFirstResponder];
    [_tfAddress resignFirstResponder];

    [self.tabBarController.tabBar setHidden:YES];

    _contryPicker.hidden = NO;
}
- (IBAction)btnHideCountryPicker:(id)sender {
    if (!isResponce) {
        [SVProgressHUD show];

    }
    _contryPicker.hidden = YES;
    [self.tabBarController.tabBar setHidden:NO];
}

- (IBAction)cameraBtnPressed:(id)sender {
    [self.tabBarController.tabBar setHidden:NO];

    _updatePhotoBox.hidden = YES;

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }else{
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Camera Unavailable" message:@"Unable to find a camera on your device." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    

}

- (IBAction)galleryBtnPressed:(id)sender {
    [self.tabBarController.tabBar setHidden:NO];

    _updatePhotoBox.hidden = YES;

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];

}
-(BOOL)emailValidate:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
    
}
- (IBAction)savePressed:(id)sender {
  
   
//    if (lat!=nil && ![lat isEqualToString:@""]) {
//        [AccountService editUserProfileWithName:_nameTxt.text userID:currentUser.userID email:_emailTxt.text ProfielImg:_profilePic.image City:selectedCity Phone:_tfPhone.text Country:selectedCountry Address:_tfAddress.text Latitude:lat Logitude:logi PlanceName:_tfLocation.text andProximity:selectedproximity success:^(id data) {
//            [self  showAlertMessage:data[@"message"]];
//            [DataManager sharedManager].updateSettings = YES;
//            if ([_tfLocation.text isEqualToString:@""]) {
//                lat=@"";
//                logi= @"";
//            }
//            
//        } failure:^(NSError *error) {
//            [self showAlertMessage:error.localizedDescription];
//            
//        }];
//
//    }
//    else{
//        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Location your entered is not valid. Please pick valid location or your current phone location will be used." delegate:nil cancelButtonTitle:@"OK!" otherButtonTitles:@"Use Current Location", nil];
//        [alertView show];
//        
//        
//        [alertView showWithBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//            
//            if (buttonIndex == 1) {
//                [self getCurrentUserLocation];
//            }
//        }] ;
//    }
    if( [self emailValidate:_emailTxt.text]) {
        
    [AccountService editUserProfileWithName:_nameTxt.text userID:currentUser.userID email:_emailTxt.text ProfielImg:_profilePic.image City:selectedCity Phone:_tfPhone.text Country:selectedCountry Address:_tfAddress.text Latitude:lat Logitude:logi PlanceName:_tfLocation.text andProximity:selectedproximity success:^(id data) {
        [self  showAlertMessage:data[@"message"]];
        _lblUserName.text = _nameTxt.text;

      
    } failure:^(NSError *error) {
        [self showAlertMessage:error.localizedDescription];
        
    }];
    }
    else{
    
        [self  showAlertMessage:@"Invalid Email"];

    }
    
    
    
   

}
-(void) getCurrentUserLocation{
    [[LocationManager sharedManager] getUserCurrentLocationWithCompletionBlock:^(CLLocationCoordinate2D location) {
        
        coordinate = location;
        [LocationManager sharedManager].currentLat = location.latitude ;
        [LocationManager sharedManager].currentLong  = location.longitude ;
        lat=  [NSString stringWithFormat:@"%f", [LocationManager sharedManager].currentLat] ;
        logi= [NSString stringWithFormat:@"%f",[LocationManager sharedManager].currentLong ];
        
        [self savePressed:nil];
        
        
        
    } errorBlock:^(NSError *error) {
        
        
        NSLog(@"%@",error.localizedDescription);
    }];
    
}


#pragma mark -  camera delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = nil; // [info objectForKey:UIImagePickerControllerEditedImage];
    
    if (image == nil){
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        if (image == nil){
            NSLog(@"Image with some error.");
        }
    }
    
    
    
  UIImage *chosenImage = [image imageWithScaledToSize:CGSizeMake(204, 204)];
    

    
    _profilePic.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark -alert Methods

- (void) showAlertMessage:(NSString *) message{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"AJO!"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles: nil];
    [alert show];
}

-(void)dismissKeyboard {
    

    NSLog(@"screen touch");
    _tblView.hidden = YES;

    [currentField resignFirstResponder];
    [_tfAddress resignFirstResponder];
    _contryPicker.hidden = YES;

    self.view.frame = CGRectMake(0, 0,self.view.frame.size.width,self.view.frame.size.height);
    
}
- (IBAction)btnPersonalPressed:(id)sender {
    
    _boxView1Constrain.constant = 680;
    __personalBoxView.hidden = NO;
  
}


- (IBAction)btnLocationPressed:(id)sender {
    _boxView2Constrain.constant = 250;
    __locationBoxView.hidden = NO;
    }

-(void) getcity{
    
    
    NSString *str3=[[NSString alloc] initWithFormat:@"https://raw.githubusercontent.com/David-Haim/CountriesToCitiesJSON/master/countriesToCities.json"];//& usedin url string to  concatination
    
    NSURL * url  = [NSURL URLWithString:str3];
    
    NSURLRequest *resquest  = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:resquest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               isResponce = YES;
                               [SVProgressHUD dismiss];

                               if (![data isEqual:nil]) {
                                   
                                   countryrespose = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                   
                                   NSLog(@"%@",countryrespose[@"Pakistan"]);
                                   if (![selectedCountry isEqualToString:@""]) {
                                       cityArray =countryrespose[[NSString stringWithFormat:@"%@",_lblCountry.text]];
                                       [_cityDropDown reloadAllComponents];
                                   }
                                   
                               }
                               
                               
                           }];
    
    
}

////////   ################### /////////////////
#pragma mark - MKDropdownMenuDataSource

- (NSInteger)numberOfComponentsInDropdownMenu:(MKDropdownMenu *)dropdownMenu {
    return 1;
}

- (NSInteger)dropdownMenu:(MKDropdownMenu *)dropdownMenu numberOfRowsInComponent:(NSInteger)component {
    if ([dropdownMenu isEqual:_proxiMneu]) {
        return proxiArray.count;

    }
  else  if ([dropdownMenu isEqual:_dropDownMneu]) {
        return countryArray.count;

    }
    else{
    return cityArray.count;
    }
}
#pragma mark - MKDropdownMenuDelegate

- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu rowHeightForComponent:(NSInteger)component {
        return 0;
    
}

- (CGFloat)dropdownMenu:(MKDropdownMenu *)dropdownMenu widthForComponent:(NSInteger)component {
        return MAX(dropdownMenu.bounds.size.width/3, 125);
    }

- (BOOL)dropdownMenu:(MKDropdownMenu *)dropdownMenu shouldUseFullRowWidthForComponent:(NSInteger)component {
    _updatePhotoBox.hidden = YES;
    [self.tabBarController.tabBar setHidden:NO];

    [currentField resignFirstResponder];
    [_tfAddress resignFirstResponder];

    if ([dropdownMenu isEqual:_cityDropDown]) {

    if ([selectedCountry isEqualToString:@""]||cityArray.count==0) {
        [self showAlertMessage:@"please select country!"];
        
    }
    }
    return NO;
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForComponent:(NSInteger)component {
  
            return [[NSAttributedString alloc] initWithString:@""
                                                   attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightLight],
                                                                NSForegroundColorAttributeName: self.view.tintColor}];
    
}

- (NSAttributedString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu attributedTitleForSelectedComponent:(NSInteger)component {
            return [[NSAttributedString alloc] initWithString:@""
                                                   attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightRegular],
                                                                NSForegroundColorAttributeName: self.view.tintColor}];
   
            
            
    
    
    
    
}

- (NSString *)dropdownMenu:(MKDropdownMenu *)dropdownMenu titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if ([dropdownMenu isEqual:_proxiMneu]) {
        return proxiArray[row];

    }
    else if ([dropdownMenu isEqual:_dropDownMneu]) {
        return @"";

    }
    else{
    
        
        
        return cityArray[row];
    
    }
}
- (UIColor *)dropdownMenu:(MKDropdownMenu *)dropdownMenu backgroundColorForRow:(NSInteger)row forComponent:(NSInteger)component {
    //    if (component == DropdownComponentCity) {
    //        return [self colorForRow:row];
    //    }
    return nil;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if ([dropdownMenu isEqual:_proxiMneu]) {
        selectedproximity = [proxiArray[row]
                             stringByReplacingOccurrencesOfString:@"miles" withString:@""];;
        _tfProximity.text =proxiArray[row];     }
   else if ([dropdownMenu isEqual:_dropDownMneu]) {
//        _lblCountry.text = countryArray[row];
//       selectedCountry = countryArray[row];
//        [self resetCity];
//        [_cityDropDown reloadAllComponents];
    }
    else{
        _lblCity.text = cityArray[row];
selectedCity =cityArray[row];
    }
}

#pragma mark - LOcation GET Method

-(void) getLatLongWithPlanceID:(NSString *)placeId
{
    
    NSString *str3=[[NSString alloc] initWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=AIzaSyCAd8TOUYgbF2nBggLgkmJd3NT8hJPrals",placeId  ];//& usedin url string to  concatination
    
    NSURL * url  = [NSURL URLWithString:str3];
    NSURLRequest * resquest = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:resquest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               id respose = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                               
                               NSLog(@"%@",respose);
                               [self mappPlaceDetail:respose];
                               
                               
                               
                           }];
    
    
}


-(void) loadDataFromNetwork:(NSString *)city

{
    NSString *cityString = [city stringByReplacingOccurrencesOfString:@" " withString:@""];
    float lati =[LocationManager sharedManager].currentLat;
    float longi =[LocationManager sharedManager].currentLong;
    
    NSString *str3=[[NSString alloc] initWithFormat:@"https://maps.googleapis.com/maps/api/place/queryautocomplete/json?input=%@&location=%f,%f&radius=500&key=AIzaSyCAd8TOUYgbF2nBggLgkmJd3NT8hJPrals",cityString ,lati,longi  ];//& usedin url string to  concatination
    
    NSURL * url  = [NSURL URLWithString:str3];
    
    NSURLRequest *resquest  = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:resquest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (![data isEqual:nil]) {
                                   
                                   id respose = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                   
                                   NSLog(@"%@",respose);
                                   
                                   [self mappData:respose];
                                   
                               }
                               
                               
                           }];
    
}

//-(void) getCurrentUserLocation{
//    [[LocationManager sharedManager] getUserCurrentLocationWithCompletionBlock:^(CLLocationCoordinate2D location) {
//
//        coordinate = location;
//        [LocationManager sharedManager].currentLat = location.latitude ;
//         [LocationManager sharedManager].currentLong  = location.longitude ;
//
//    } errorBlock:^(NSError *error) {
//
//
//        NSLog(@"%@",error.localizedDescription);
//    }];
//
//}

#pragma mark -alert Methods



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 39;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return m.menuArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cell";
    MenuCell * cell = (MenuCell *)[_tblView dequeueReusableCellWithIdentifier:@"MenuCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MenuCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    MainMenu *menu=[m.menuArray objectAtIndex:indexPath.row];
    [cell.placeName setText:[NSString stringWithFormat:@"%@",menu.descipt]];
    
    
    
    //  [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenu *menu=[m.menuArray objectAtIndex:indexPath.row];
    _tfLocation.text = menu.descipt;
    changeStr=_tfLocation.text;
    _tblView.hidden = YES;
    //_btnsave.enabled = YES;
    
    [self getLatLongWithPlanceID:menu.placeId];
}

-(void) mappData:(NSDictionary *) value{
    
    
    [ menArray removeAllObjects];// remove previous data objects of table view..
    
    m =    [[MainMenu alloc]initWithDict:value];
    
    
    _tblView.hidden = NO;
    [_tblView reloadData];
}



-(void) mappPlaceDetail:(NSDictionary *) value{
    
    
    
    
    subMenu =    [[PlacesSubMenu alloc]initWithDict:value];
    lat= subMenu.lat;
    logi= subMenu.lng;

    [[NSUserDefaults standardUserDefaults] setObject:selectedproximity forKey:@"Proximity"];
    //_btnsave.enabled = YES;
    
    NSLog(@"%@",value);
    
}
-(void) saveLocalData{

    

}


- (IBAction)btnHideKeyBoard:(id)sender {
    if (isResponce) {
        _contryPicker.hidden = YES;
    }
    [self.tabBarController.tabBar setHidden:NO];


    [_tfAddress resignFirstResponder];
    [currentField resignFirstResponder];
    _tblView.hidden= YES;
   // _btnHidekeyBoard.hidden=YES;
    if ([_tfProximity.text rangeOfString:@"km"].location==NSNotFound && ![_tfProximity.text isEqualToString:@""])
    {
        distance = _tfProximity.text;
        
    }
    
    _tfProximity.text =[NSString stringWithFormat:@"%@ km",distance];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"hidePickerView"
     object:nil];
    _updatePhotoBox.hidden  = YES;

}



@end
